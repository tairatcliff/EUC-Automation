<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     cloneHorizonVMs.ps1
 Example:      cloneHorizonVMs.ps1 -vmConfigXML vmConfigXML.xml
========================================================================
#>
param(
    [ValidateScript({Test-Path $HorizonAgentFile -PathType 'Container'})]
    [string]$HorizonAgentFile,
    [ValidateScript({Test-Path $osOptimizationToolFile -PathType 'Container'})]
    [string]$osOptimizationToolFile,
    [string]$DestinationDirectory,
    [string]$VMName,
    [string]$GuestUser,
    [string]$GuestPass
)

If($HorizonAgent){  
    Write-Host "Copying Horizon Agent file to VM"
    Copy-VMGuestfile -LocalToGuest -source "$HorizonAgentFile" -destination $DestinationDirectory -Force:$true -vm $VMName -guestuser $hznServiceAccount -guestpassword $hznServiceAccountPassword  -ErrorAction SilentlyContinue
}
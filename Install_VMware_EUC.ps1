﻿<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     Install_VMware_EUC.ps1
 Example:      Install_VMware_EUC.ps1 -eucConfigJson eucConfigXML.json
 
 ## Primary script to execute all of the scripts
========================================================================
#>

param(
    [Parameter(Mandatory = $True)]
    [String]$eucConfigJson
)
If(!(Test-Path $eucConfigJson)){
    throw "Could not validate the path provided for the EUC Config JSON file!"
}

$global:eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$deploymentID = $eucConfig.globalConfig.configFileNames.deploymentID
If($deploymentID -eq ""){
    $deploymentID = Get-Random -Minimum 1000000 -Maximum 9999999
    $eucConfig.globalConfig.configFileNames.deploymentID = "$deploymentID"
}
$OriginalBackground = $Host.UI.RawUI.BackgroundColor
$NewBackground = "DarkGray"
$Host.UI.RawUI.BackgroundColor = $NewBackground
Clear-Host

# Add the application directory to the JSON file. 
$eucConfig.globalConfig.deploymentDirectories | Add-Member -MemberType NoteProperty -Name "appRootDirectory" -Value $PsScriptRoot -Force
$eucConfig.globalConfig.configFileNames.jsonFile = "eucConfig_$($deploymentID).json"
$jsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $jsonPath

Function Menu{
    Clear-Host
    Write-Host `n
    Write-Host "                                                 " -BackgroundColor Green -ForegroundColor DarkBlue
    Write-Host "          Welcome to the EUC Install App         " -BackgroundColor Green -ForegroundColor DarkBlue
    Write-Host "                                                 " -BackgroundColor Green -ForegroundColor DarkBlue
    Write-Host `n
    Write-Host "The following tasks can be exectued from this tool" -ForegroundColor Cyan
    Write-Host `n
    Write-Host "0: Validate the installation configurations  ***** Not available yet *****" -ForegroundColor Cyan `n
    Write-Host "1: Deploy NSX Edges and Routers for EUC Management" -ForegroundColor Cyan `n
    Write-Host "2: Deploy NSX Load Balancers" -ForegroundColor Cyan `n
    Write-Host "3: Deploy Windows VMs" -ForegroundColor Cyan `n
    Write-Host "4: Install the Connection Server software" -ForegroundColor Cyan `n
    Write-Host "5: Configure the desktop NSX environment" -ForegroundColor Cyan `n
    Write-Host "6: Build Horizon Desktop Pools" -ForegroundColor Cyan `n
    Write-Host "7: Deploy Unified Access Gateways (UAG)" -ForegroundColor Cyan `n
    Write-Host "8: Create AirWatch Config Files" -ForegroundColor Cyan `n
    Write-Host "9: Deploy AirWatch" -ForegroundColor Cyan `n
    Write-Host "10: Create Identity Manager Config Files" -ForegroundColor Cyan `n
    Write-Host "11: Deploy Identity Manager" -ForegroundColor Cyan `n
    Write-Host "12: Export JSON to file" -ForegroundColor Cyan `n
    Write-Host "Type 'x' to exit" -ForegroundColor Yellow
    Write-Host `n
    $Selection = (Read-Host "Select the tasks that you would like to execute").ToLower()
    return $Selection
}

Try{
    # Import PowerShell modules
    If(!(Get-Module VMware*)){
        Write-Host "`n`n********Importing vCenter and Horizon PowerShell modules********"
        Get-Module –ListAvailable VM* | Import-Module
    }

    If(!(Get-Module PowerNSX)){
        Write-Host "`n`n********Importing PowerNSX module********"
        $PowerNSXModule = Find-Module PowerNSX
        If($PowerNSXModule){
            Import-Module $PowerNSXModule.Name
        }
    }
} Catch {
    throw "Failed to import the VMware PowerShell Modules for vCenter, Horizon and PowerNSX. You will need to import and install these modules manually before continuing."
}

Try{
    Import-Module "$PsScriptRoot\Tools\PSModules\VMware.Hv.Helper\VMware.HV.Helper.psm1"
    Import-Module "$PsScriptRoot\Tools\PSModules\uagdeploy.psm1"

    If(!(Get-Module PowerNSX)){
        Write-Host "`n`n********Importing PowerNSX module********"
        Import-Module "$PsScriptRoot\Tools\powernsx-master\module\PowerNSX.psm1"
    }
} Catch {
    throw "Failed to import the required PowerShell Modules. Please ensure the 'VMware.HV.Helper.psm1', 'PowerNSX.psm1' and 'uagdeploy.psm1' modules are located in the $($PsScriptRoot)\Tools\PSModules\ directory."
}

#PowerCLI 6 is required due to OvfConfiguration commands.
[int]$PowerCliMajorVersion = (Get-PowerCliVersion).major
if ( -not ($PowerCliMajorVersion -ge 6 ) ) { throw "PowerCLI version 6 or above is required" }

# Configure settings to allow connections to multiple vCenter Servers
#Set-PowerCLIConfiguration -DefaultVIServerMode multiple -Confirm:$false

# Set the PowerShell preference variables
#$ErrorActionPreference = "SilentlyContinue"
$WarningPreference = "SilentlyContinue"
$ConfirmPreference = "None"
$LogCommandHealthEvent = $true


# Remove any existing jobs that were not properly removed from previous executions
Remove-Job * | Out-Null

# Disconnect from any existing vSphere or Horizon connections that were not properly disconnected from previous exections
If($global:defaultHVServers.Name){
	disconnect-hvserver * -Confirm:$false
}
If($global:defaultVIServers.Name){
	disconnect-hvserver * -Confirm:$false
}


$Quit = $false

While(! $Quit){
    $Selection = Menu

    If($Selection -eq "0"){
        Clear-Host
        Write-Host -ForegroundColor White  "Validating the Installation Configurations" `n
        & '.\Code\00 - Validate Environment\eucDeploymentValidate-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "1"){
        Clear-Host
        Write-Host -ForegroundColor White  "Configure the management NSX environment" `n
        & '.\Code\01 - nsxDeployNetworking\nsxDeployLdr-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "2"){
        Clear-Host
        Write-Host -ForegroundColor White  "Deploy NSX Load Balancers" `n
        & '.\Code\01 - nsxDeployNetworking\nsxDeployLB-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "3"){
        Clear-Host
        Write-Host -ForegroundColor White  "Clone the Horizon Connection Server VMs" `n
        & '.\Code\02 - cloneVMs\cloneVMs-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "4"){
        Clear-Host
        Write-Host -ForegroundColor White  "Install the Connection Server software" `n
        & '.\Code\03 - installConnectionServers\installConnectionServers-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
        If($Continue -eq "y"){$Selection = Menu}
    }
    If($Selection -eq "5"){
        Clear-Host
        Write-Host -ForegroundColor White  "Configure the desktop NSX environment" `n
        & '.\Code\04 - buildNSXDesktopNetworks\buildNSXDesktopNetworks-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "6"){
        Clear-Host
        Write-Host -ForegroundColor White  "Build Horizon Desktop Pools" `n
        & '.\Code\05 - buildDesktopPools\buildDesktopPools-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "7"){
        Clear-Host
        Write-Host -ForegroundColor White  "Deploy UAG" `n
        & '.\Code\10 - Deploy UAG\installUAG-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "8"){
        Clear-Host
        Write-Host -ForegroundColor White  "Create AirWatch Config Files" `n
        & '.\Code\20 - Deploy AirWatch\createAirWatchConfigFiles.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "9"){
        Clear-Host
        Write-Host -ForegroundColor White  "Deploy AirWatch" `n
        & '.\Code\20 - Deploy AirWatch\installAirWatch-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "10"){
        Clear-Host
        Write-Host -ForegroundColor White  "Create Identity Manager Config Files" `n
        & '.\Code\30 - Deploy vIDM\createVidmConfigFiles.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "11"){
        Clear-Host
        Write-Host -ForegroundColor White  "Deploy Identity Manager" `n
        & '.\Code\30 - Deploy vIDM\deployVidm-1.0.0.ps1'
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "12"){
        Clear-Host
        Write-Host -ForegroundColor White  "Export JSON to file" `n
        $FilePath = Read-Host "Enter the path to save the JSON File: "
        Set-Content -Path $FilePath -Value $global:eucConfig
        $Continue = Read-Host "Return to the menu? [y/n]"
        If($Continue -eq "n"){$Quit = $true}
    }
    If($Selection -eq "x"){
        $Quit = $true
    }
}

# Disconnect from any existing vSphere or Horizon connections that were not properly disconnected from previous exections
If($global:defaultHVServers.Name){
	disconnect-hvserver * -Confirm:$false
}
If($global:defaultVIServers.Name){
	disconnect-hvserver * -Confirm:$false
}

$Host.UI.RawUI.BackgroundColor = $OriginalBackground
Clear-Host


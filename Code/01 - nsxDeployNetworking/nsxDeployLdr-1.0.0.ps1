<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     nsxDeployNetwork.ps1
 Example:      nsxDeployNetwork.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    Write-Host "Refreshing JSON config"
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}

$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.horizonCertificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$connectionServerConfig = $horizonConfig.connectionServerConfig
$nsxEdgeConfig = $nsxConfig.edge
$MgmtvCenterServer = $mgmtvCenterConfig.vCenter

#vSphereDetails for NSX
$mgmtvCenterUserName = $serviceAccounts.horizonServiceAccount.username
$mgmtvCenterPassword = $serviceAccounts.horizonServiceAccount.password
$mgmtClusterName = $mgmtvCenterConfig.cluster
$mgmtVdsName = $mgmtvCenterConfig.distributedSwitch
$mgmtEdgeClusterName = $mgmtvCenterConfig.edgeCluster
$mgmtEdgeDatastoreName = $mgmtvCenterConfig.edgeDatastore
$mgmtNsxUplinkPortGroup01Name = $nsxEdgeConfig.nsxUplinkPortGroup01Name
$mgmtNsxUplinkPortGroup02Name = $nsxEdgeConfig.nsxUplinkPortGroup02Name
$mgmtNsxFolderName = $vmFolders.nsx
$mgmtDatacenterName = $mgmtvCenterConfig.datacenter

# Logical Topology environment
$tor01UplinkProtocolAddress = $nsxEdgeConfig.tor01UplinkProtocolAddress
$tor02UplinkProtocolAddress = $nsxEdgeConfig.tor02UplinkProtocolAddress
$Edge01Uplink01PrimaryAddress = $nsxEdgeConfig.edge01Uplink01PrimaryAddress
$Edge01Uplink02PrimaryAddress = $nsxEdgeConfig.edge01Uplink02PrimaryAddress
$Edge02Uplink01PrimaryAddress = $nsxEdgeConfig.edge02Uplink01PrimaryAddress
$Edge02Uplink02PrimaryAddress = $nsxEdgeConfig.edge02Uplink02PrimaryAddress
$bgpPassword = $nsxEdgeConfig.bgpPassword
$uplink01ASN = $nsxEdgeConfig.uplink01ASN
$uplink02ASN = $nsxEdgeConfig.uplink02ASN
$LocalASN = $nsxEdgeConfig.localASN
$keepAliveTimer = $nsxEdgeConfig.keepAliveTimer
$holdDownTimer = $nsxEdgeConfig.holdDownTimer
$AppliancePassword = $nsxEdgeConfig.edgePassword
$mgmtTransitLsName = $nsxConfig.logicalSwitches.eucMgmtTransit
$mgmtEdgeHAPortGroupName = $nsxEdgeConfig.edgeHAPortGroupName
$eucMgmtNetwork = $nsxConfig.logicalSwitches.eucMgmtNetwork
$mgmtEdge01Name = $nsxEdgeConfig.edge01Name
$mgmtEdge02Name = $nsxEdgeConfig.edge02Name
$mgmtLdrName = $nsxEdgeConfig.ldrName
$mgmtEUCTransportZoneName = $nsxEdgeConfig.EUCTransportZoneName

#Logical Networking Topology
$mgmtEdge01InternalPrimaryAddress = $nsxEdgeConfig.edge01InternalPrimaryAddress
$mgmtEdge02InternalPrimaryAddress = $nsxEdgeConfig.edge02InternalPrimaryAddress
$mgmtLdrUplinkPrimaryAddress = $nsxEdgeConfig.ldrUplinkPrimaryAddress
$mgmtLdrUplinkProtocolAddress = $nsxEdgeConfig.ldrUplinkProtocolAddress
$mgmtLdrEUCMGMTPrimaryAddress = $nsxEdgeConfig.ldrEUCMGMTPrimaryAddress
$mgmtDefaultSubnetBits = $nsxEdgeConfig.defaultSubnetBits

If($error){throw "Failed to validate the required configuration settings"}


#Get Connection required.
try {
    Connect-NsxServer -vCenterServer $MgmtvCenterServer -Username $MgmtvCenterUserName -Password $MgmtvCenterPassword #| out-null
} catch {
    Write-Error "Failed connecting.  Check connection details and try again.  $_"
}

    #Check that the vCenter environment looks correct for deployment.
    try {
        $MgmtCluster = Get-Cluster $MgmtClusterName -errorAction Stop
        $mgmtEdgeCluster = get-cluster $mgmtEdgeClusterName -errorAction Stop
        $mgmtEdgeDatastore = get-datastore $mgmtEdgeDatastoreName -errorAction Stop
    }
    catch {
        Write-Error "Failed validating vSphere Environment. $_"
    }

    try {
        # Validate that existing configuration doesn't already exist.
		if ( Get-NsxLogicalSwitch $eucMgmtNetwork ) {
            Write-Error "Logical Switch already exists.  Please remove and try again."
        }
        if ( Get-NsxLogicalSwitch $mgmtTransitLsName ) {
            Write-Error "Logical Switch already exists.  Please remove and try again."
        }
        if ( (get-nsxservice "tcp-80") -or (get-nsxservice "tcp-3306" ) ) {
            Write-Error "Custom services already exist.  Please remove and try again."
        }
        if ( get-nsxedge $mgmtEdge01Name ) {
            Write-Error "Edge already exists.  Please remove and try again."
        }
        if ( get-nsxlogicalrouter $mgmtLdrName ) {
            Write-Error "Logical Router already exists.  Please remove and try again."
        }
    }
    catch {
        Write-Error "Failed validating environment for Horizon deployment.  $_"
    }


if($nsxEdgeConfig.deployNsxEdges){
    #Prepare for NSX Install/Configuration
    #Create Uplink distributed port groups in vCenter
    if(!(get-vdportgroup $mgmtNsxUplinkPortGroup01Name -errorAction Ignore)){
        write-host -foregroundcolor Green "Creating NSX Uplink Port Group 1. `n"
        Get-VDSwitch -Name $MgmtVdsName | New-VDPortgroup -Name $mgmtNsxUplinkPortGroup01Name -NumPorts 10 -VLanId 145 -PortBinding Static #| out-null
        $EdgeUplink01Network = Get-VDPortgroup $mgmtNsxUplinkPortGroup01Name
        Get-VDUplinkTeamingPolicy -VDPortgroup $mgmtNsxUplinkPortGroup01Name | Set-VDUplinkTeamingPolicy -LoadBalancingPolicy "LoadBalanceLoadBased" -ActiveUplinkPort "dvUplink1" -StandbyUplinkPort "dvUplink2" #| out-null
    }
    if(!(get-vdportgroup $mgmtNsxUplinkPortGroup02Name -errorAction Ignore)){
        write-host -foregroundcolor Green "Creating NSX Uplink Port Group 2. `n"
        Get-VDSwitch -Name $MgmtVdsName | New-VDPortgroup -Name $mgmtNsxUplinkPortGroup02Name -NumPorts 10 -VLanId 145 -PortBinding Static #| out-null
        $EdgeUplink02Network = Get-VDPortgroup $mgmtNsxUplinkPortGroup02Name
        Get-VDUplinkTeamingPolicy -VDPortgroup $mgmtNsxUplinkPortGroup02Name | Set-VDUplinkTeamingPolicy -LoadBalancingPolicy "LoadBalanceLoadBased" -ActiveUplinkPort "dvUplink2" -StandbyUplinkPort "dvUplink1" #| out-null
    }

    if(get-Folder -Name $mgmtNsxFolderName -ErrorAction Ignore){
        Write-Host "Found an existing $mgmtNsxFolderName VM Folder in vCenter. This is where the Connection Servers will be deployed." -ForegroundColor Yellow `n
    } Else {
        Write-Host "The $mgmtNsxFolderName VM folder does not exist, creating a new folder" -ForegroundColor Yellow `n
        (Get-View (Get-View -viewtype datacenter -filter @{"name"="$mgmtDatacenterName"}).vmfolder).CreateFolder("$mgmtNsxFolderName") #| out-null
    }

    try {
        #Configure TZ and add clusters.
        If(!(Get-NsxTransportZone -Name $mgmtEUCTransportZoneName)){
            Write-Host "Creating NSX Transport Zone $mgmtEUCTransportZoneName. `n" -ForegroundColor Green
            New-NsxTransportZone -Name $mgmtEUCTransportZoneName -Cluster $MgmtCluster -ControlPlaneMode "HYBRID_MODE" #| out-null
        }
    }
    catch {
        Write-Error  "Failed configuring Transport Zone.  $_"
    }

    write-host -foregroundcolor Green "`nNSX Infrastructure Config Complete`n"

    ## Topology Deployment
    write-host -foregroundcolor Green "NSX Horizon deployment beginning.`n"
    #Logical Switches

    write-host -foregroundcolor "Green" "Creating Logical Switches..."

    ## Creates logical switches
    $TransitLs = Get-NsxTransportZone $mgmtEUCTransportZoneName | New-NsxLogicalSwitch $mgmtTransitLsName
    $HorizonLs = Get-NsxTransportZone $mgmtEUCTransportZoneName | New-NsxLogicalSwitch $eucMgmtNetwork
    
    # Check if the user defined an Edge HA Port group that already exists .e.g. The Management DVS Port Group. If not, create a new Logical Switch
	if(($EdgeHAPortGroup = Get-VDPortgroup -VDSwitch $MgmtVdsName $mgmtEdgeHAPortGroupName -ErrorAction Ignore) -or ($EdgeHAPortGroup = Get-NsxLogicalSwitch $mgmtEdgeHAPortGroupName -ErrorAction Ignore)){
		Write-Host -foregroundcolor "Green" "Found an existing port group or Logical Switch called $mgmtEdgeHAPortGroupName. This will be used for the Edge HA Management."
    } Else {
		$EdgeHAPortGroup = Get-NsxTransportZone $mgmtEUCTransportZoneName | New-NsxLogicalSwitch $mgmtEdgeHAPortGroupName
	}
    
    # Provision and Configure DLR
    # DLR Appliance has the uplink router interface created first.
    write-host -foregroundcolor "Green" "Creating DLR"
    $LdrvNic0 = New-NsxLogicalRouterInterfaceSpec -type Uplink -Name $mgmtTransitLsName -ConnectedTo $TransitLs -PrimaryAddress $mgmtLdrUplinkPrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits

    # The DLR is created with the first vnic defined, and the datastore and cluster on which the Control VM will be deployed.
    $Ldr = New-NsxLogicalRouter -name $mgmtLdrName -ManagementPortGroup $EdgeHAPortGroup -interface $LdrvNic0 -cluster $mgmtEdgeCluster -datastore $mgmtEdgeDatastore -EnableHA -HADatastore $mgmtEdgeDatastore

    ## Adding DLR interfaces after the DLR has been deployed. This can be done any time if new interfaces are required.
    write-host -foregroundcolor Green "Adding DLR interfaces after the DLR has been deployed"
    $Ldr | New-NsxLogicalRouterInterface -Type Internal -name $eucMgmtNetwork -ConnectedTo $HorizonLs -PrimaryAddress $mgmtLdrEUCMGMTPrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits #| out-null

    # Disable LDR Firewall
    $Ldr.features.firewall.enabled = "false"
    $Ldr | Set-NsxLogicalRouter -confirm:$false

    ##Configure DLR Routing
    Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -confirm:$false -EnableEcmp -RouterId $mgmtLdrUplinkPrimaryAddress -ProtocolAddress $mgmtLdrUplinkProtocolAddress -LocalAS $LocalASN -ForwardingAddress $mgmtLdrUplinkPrimaryAddress -EnableBgp -EnableLogging #| out-null
    If($bgpPassword){    
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtEdge01InternalPrimaryAddress -ForwardingAddress $mgmtLdrUplinkPrimaryAddress -ProtocolAddress $mgmtLdrUplinkProtocolAddress -HoldDownTimer $holdDownTimer -KeepAliveTimer $keepAliveTimer -Confirm:$false -Password $bgpPassword #| out-null
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtEdge02InternalPrimaryAddress -ForwardingAddress $mgmtLdrUplinkPrimaryAddress -ProtocolAddress $mgmtLdrUplinkProtocolAddress -HoldDownTimer $holdDownTimer -KeepAliveTimer $keepAliveTimer -Confirm:$false -Password $bgpPassword #| out-null
    } Else {
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtEdge01InternalPrimaryAddress -ForwardingAddress $mgmtLdrUplinkPrimaryAddress -ProtocolAddress $mgmtLdrUplinkProtocolAddress -HoldDownTimer $holdDownTimer -KeepAliveTimer $keepAliveTimer -Confirm:$false #| out-null
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtEdge02InternalPrimaryAddress -ForwardingAddress $mgmtLdrUplinkPrimaryAddress -ProtocolAddress $mgmtLdrUplinkProtocolAddress -HoldDownTimer $holdDownTimer -KeepAliveTimer $keepAliveTimer -Confirm:$false #| out-null
    }
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -EnableBgpRouteRedistribution -Confirm:$false #| out-null
        Get-NsxLogicalRouter $mgmtLdrName | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterRedistributionRule -Learner bgp -FromConnected -Action permit -Confirm:$false #| out-null
    # Provision and Configure Edges

    # EDGE01 -ECMP
    ## Defining the uplink and internal interfaces to be used when deploying the edge. Note there are two IP addreses on these interfaces. $EdgeInternalSecondaryAddress and $EdgeUplink01SecondaryAddress are the VIPs
    $edgevnic0 = New-NsxEdgeinterfacespec -index 0 -Name "Uplink01" -type Uplink -ConnectedTo $EdgeUplink01Network -PrimaryAddress $Edge01Uplink01PrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
	$edgevnic1 = New-NsxEdgeinterfacespec -index 1 -Name "Uplink02" -type Uplink -ConnectedTo $EdgeUplink02Network -PrimaryAddress $Edge01Uplink02PrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
    $edgevnic2 = New-NsxEdgeinterfacespec -index 2 -Name $mgmtTransitLsName -type Internal -ConnectedTo $TransitLs -PrimaryAddress $mgmtEdge01InternalPrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
    ## Deploy appliance with the defined uplinks
    write-host -foregroundcolor "Green" "Creating $mgmtEdge01Name"
    $Edge1 = New-NsxEdge -name $mgmtEdge01Name -cluster $mgmtEdgeCluster -datastore $mgmtEdgeDatastore -Interface $edgevnic0, $edgevnic1, $edgevnic2 -Password $AppliancePassword -FwDefaultPolicyAllow -FwEnabled:$false
    
    # EDGE02 -ECMP
    ## Defining the uplink and internal interfaces to be used when deploying the edge. Note there are two IP addreses on these interfaces. $EdgeInternalSecondaryAddress and $EdgeUplink01SecondaryAddress are the VIPs
    $edgevnic0 = New-NsxEdgeinterfacespec -index 0 -Name "Uplink01" -type Uplink -ConnectedTo $EdgeUplink01Network -PrimaryAddress $Edge02Uplink01PrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
	$edgevnic1 = New-NsxEdgeinterfacespec -index 1 -Name "Uplink02" -type Uplink -ConnectedTo $EdgeUplink02Network -PrimaryAddress $Edge02Uplink02PrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
    $edgevnic2 = New-NsxEdgeinterfacespec -index 2 -Name $mgmtTransitLsName -type Internal -ConnectedTo $TransitLs -PrimaryAddress $mgmtEdge02InternalPrimaryAddress -SubnetPrefixLength $mgmtDefaultSubnetBits
    ## Deploy appliance with the defined uplinks
    write-host -foregroundcolor "Green" "Creating $mgmtEdge02Name"
    $Edge2 = New-NsxEdge -name $mgmtEdge02Name -cluster $mgmtEdgeCluster -datastore $mgmtEdgeDatastore -Interface $edgevnic0, $edgevnic1, $edgevnic2 -Password $AppliancePassword -FwDefaultPolicyAllow -FwEnabled:$false

    
    ######################################
    ##Configure Edge Routing
    ## Edge 01
    Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -confirm:$false -EnableEcmp -RouterId $Edge01Uplink01PrimaryAddress -LocalAS $LocalASN -EnableBgp -EnableLogging #| out-null
    If($bgpPassword){
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtLdrUplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink01ASN -IpAddress $tor01UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink02ASN -IpAddress $tor02UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
    } else {
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtLdrUplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink01ASN -IpAddress $tor01UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
        Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink02ASN -IpAddress $tor02UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
    }
    Get-NSXEdge $mgmtEdge01Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableBgpRouteRedistribution -Confirm:$false #| out-null
    Get-NsxEdge $mgmtEdge01Name | Get-NsxEdgeRouting | New-NsxEdgeRedistributionRule -Learner bgp -FromConnected -FromStatic -Action permit -Confirm:$false #| out-null
    #Edge 02
    Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -confirm:$false -EnableEcmp -RouterId $Edge01Uplink02PrimaryAddress -LocalAS $LocalASN -EnableBgp -EnableLogging #| out-null
    If($bgpPassword){
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtLdrUplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink01ASN -IpAddress $tor01UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink02ASN -IpAddress $tor02UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false -Password $bgpPassword #| out-null
    } Else {
        
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $LocalASN -IpAddress $mgmtLdrUplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink01ASN -IpAddress $tor01UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
        Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeBgpNeighbour -RemoteAS $uplink02ASN -IpAddress $tor02UplinkProtocolAddress -KeepAliveTimer $keepAliveTimer -HoldDownTimer $holdDownTimer -Confirm:$false #| out-null
    }
    Get-NSXEdge $mgmtEdge02Name | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableBgpRouteRedistribution -Confirm:$false #| out-null
    Get-NsxEdge $mgmtEdge02Name | Get-NsxEdgeRouting | New-NsxEdgeRedistributionRule -Learner bgp -FromConnected -FromStatic -Action permit -Confirm:$false #| out-null

    # Move the NSX VMs to the VM Folder
    Move-VM -VM "$mgmtEdge01Name*" -Destination $mgmtNsxFolderName -ErrorAction SilentlyContinue
    Move-VM -VM "$mgmtEdge02Name*" -Destination $mgmtNsxFolderName -ErrorAction SilentlyContinue
    Move-VM -VM "$mgmtLdrName*" -Destination $mgmtNsxFolderName -ErrorAction SilentlyContinue
}

Write-Host "`n The deployment and configuraiton of NSX is complete `n" -ForegroundColor Green

$vSpherePortGroup = Get-VDPortgroup | Where-Object{$_.Name -like "*$($HorizonLs.vdnId)-$($HorizonLs.Name)"} | Select-Object Name
Write-Host "The Name of the Horizon Management Network is: $($vSpherePortGroup.Name)" 

$networks.mgmtNetwork.name = $vSpherePortGroup.Name
$networks.mgmtNetwork.gateway = $mgmtLdrEUCMGMTPrimaryAddress
$eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJsonPath

Disconnect-NsxServer * -Confirm:$false
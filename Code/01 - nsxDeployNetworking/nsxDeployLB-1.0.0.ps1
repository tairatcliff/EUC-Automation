<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     nsxDeployNetwork.ps1
 Example:      nsxDeployNetwork.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    Write-Host "Refreshing JSON config"
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}

$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.horizonCertificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$connectionServerConfig = $horizonConfig.connectionServerConfig
$nsxEdgeConfig = $nsxConfig.edge
$MgmtvCenterServer = $mgmtvCenterConfig.vCenter
$mgmtvCenterUserName = $serviceAccounts.horizonServiceAccount.username
$mgmtvCenterPassword = $serviceAccounts.horizonServiceAccount.password
$dmzvCenterServer = $dmzvCenterConfig.vCenter
$dmzvCenterUserName = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password

$nsxFolderName = $vmFolders.nsx
$AppliancePassword = $nsxEdgeConfig.edgePassword
$defaultSubnetBits = $nsxEdgeConfig.defaultSubnetBits
$mgmtEdgeClusterName = $mgmtvCenterConfig.edgeCluster
$mgmtEdgeDatastoreName = $mgmtvCenterConfig.edgeDatastore
$mgmtEucTransportZoneName = $nsxEdgeConfig.EUCTransportZoneName
$mgmtEucLogicalSwitchName = $nsxConfig.logicalSwitches.eucMgmtNetwork
$mgmtLdrEUCMGMTPrimaryAddress = $networks.mgmtNetwork.gateway 
$dmzEdgeClusterName = $dmzvCenterConfig.cluster
$dmzEdgeDatastoreName = $mgmtvCenterConfig.datastore
$dmzEucTransportZoneName = $nsxEdgeConfig.eucDmzTransportZoneName
$dmzEucLogicalSwitchName = $nsxConfig.logicalSwitches.eucDmzNetwork
$dmzLdrEUCMGMTPrimaryAddress = $networks.dmzInternet.gateway

# Check which LoadBalancers are being deployed
$deployCsLb = $nsxConfig.loadBalancers.Horizon.ConnectionServers.deployLoadBalancer
$deployDmzCsUagLb = $nsxConfig.loadBalancers.Horizon.UAG.deployLoadBalancer
$deployAwCnLb = $nsxConfig.loadBalancers.AirWatch.CN.deployLoadBalancer
$deployDmzAwDsLb = $nsxConfig.loadBalancers.AirWatch.DS.deployLoadBalancer
$deployDmzIdmLb = $nsxConfig.loadBalancers.vIDM.vIDM.deployLoadBalancer
$deployDmzIdmUagLb = $nsxConfig.loadBalancers.vIDM.UAG.deployLoadBalancer

# VM Arrays
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$horizonUagServers = $horizonConfig.connectionServerConfig.uagServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchCnUagServers = $airwatchConfig.airwatchServers.CN.uagServers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers
$airwatchDsUagServers = $airwatchConfig.airwatchServers.DS.uagServers
$vidmServers = $vidmConfig.vidmServers.servers
$vidmUagServers = $vidmConfig.vidmServers.uagServers

# Horizon CS LoadBalancer Config
$horizonCsLbConfig = $nsxConfig.loadBalancers.Horizon.ConnectionServers
$horizonLbEdgeName = $horizonCsLbConfig.lbEdgeName
$horizonLbPrimaryIPAddress = $horizonCsLbConfig.LbPrimaryIPAddress
$horizonVipIp = $horizonCsLbConfig.vipIp
$horizonLbAlgorithm = $horizonCsLbConfig.lbAlgorithm
$horizonLbPoolName = $horizonCsLbConfig.lbPoolName
$horizonVipName = $horizonCsLbConfig.vipName
$horizonAppProfileName = $horizonCsLbConfig.appProfileName
$horizonPersistenceMethod = $horizonCsLbConfig.PersistenceMethod
$horizonPersistenceExpires = $horizonCsLbConfig.PersistenceExpires
$horizonVipProtocol = $horizonCsLbConfig.vipProtocol
$horizonVipPort = $horizonCsLbConfig.vipPort
$horizonServerPort = $horizonCsLbConfig.serverPort
$horizonLbMonitorName = $horizonCsLbConfig.LbMonitorName
$horizonLbMonitorUrl = $horizonCsLbConfig.LbMonitorUrl

# Horizon UAG LoadBalancer Config
$horizonUagCsLbConfig = $nsxConfig.loadBalancers.Horizon.UAG
$horizonUagLbEdgeName = $horizonUagCsLbConfig.lbEdgeName
$horizonUagLbPrimaryIPAddress = $horizonUagCsLbConfig.LbPrimaryIPAddress
$horizonUagVipIp = $horizonUagCsLbConfig.vipIp
$horizonUagLbAlgorithm = $horizonUagCsLbConfig.lbAlgorithm
$horizonUagLbPoolName = $horizonUagCsLbConfig.lbPoolName
$horizonUagVipName = $horizonUagCsLbConfig.vipName
$horizonUagAppProfileName = $horizonUagCsLbConfig.appProfileName
$horizonUagPersistenceMethod = $horizonUagLbConfig.PersistenceMethod
$horizonUagPersistenceExpires = $horizonUagLbConfig.PersistenceExpires
$horizonUagVipProtocol = $horizonUagCsLbConfig.vipProtocol
$horizonUagVipPort = $horizonUagCsLbConfig.vipPort
$horizonUagServerPort = $horizonUagCsLbConfig.serverPort
$horizonUagLbMonitorName = $horizonUagCsLbConfig.LbMonitorName
$horizonUagLbMonitorUrl = $horizonUagCsLbConfig.LbMonitorUrl

# AirWatch CN LoadBalancer Config
$AirWatchCnLbConfig = $nsxConfig.loadBalancers.AirWatch.CN
$AirWatchCnLbEdgeName = $AirwatchCnLbConfig.lbEdgeName
$AirWatchCnLbPrimaryIPAddress = $AirwatchCnLbConfig.LbPrimaryIPAddress
$AirWatchCnVipIp = $AirwatchCnLbConfig.vipIp
$AirWatchCnLbAlgorithm = $AirwatchCnLbConfig.lbAlgorithm
$AirWatchCnLbPoolName = $AirwatchCnLbConfig.lbPoolName
$AirWatchCnVipName = $AirwatchCnLbConfig.vipName
$AirWatchCnAppProfileName = $AirwatchCnLbConfig.appProfileName
$AirWatchCnPersistenceMethod = $AirwatchCnLbConfig.PersistenceMethod
$AirWatchCnPersistenceExpires = $AirwatchCnLbConfig.PersistenceExpires
$AirWatchCnVipProtocol = $AirwatchCnLbConfig.vipProtocol
$AirWatchCnVipPort = $AirwatchCnLbConfig.vipPort
$AirWatchCnServerPort = $AirwatchCnLbConfig.serverPort
$AirWatchCnLbMonitorName = $AirwatchCnLbConfig.LbMonitorName
$AirWatchCnLbMonitorUrl = $AirwatchCnLbConfig.LbMonitorUrl

# AirWatch DS LoadBalancer Config
$AirWatchDsLbConfig = $nsxConfig.loadBalancers.AirWatch.DS
$AirWatchDsLbEdgeName = $AirWatchDsLbConfig.lbEdgeName
$AirWatchDsLbPrimaryIPAddress = $AirWatchDsLbConfig.LbPrimaryIPAddress
$AirWatchDsVipIp = $AirWatchDsLbConfig.vipIp
$AirWatchDsLbAlgorithm = $AirWatchDsLbConfig.lbAlgorithm
$AirWatchDsLbPoolName = $AirWatchDsLbConfig.lbPoolName
$AirWatchDsVipName = $AirWatchDsLbConfig.vipName
$AirWatchDsAppProfileName = $AirWatchDsLbConfig.appProfileName
$AirWatchDsPersistenceMethod = $AirWatchDsLbConfig.PersistenceMethod
$AirWatchDsPersistenceExpires = $AirWatchDsLbConfig.PersistenceExpires
$AirWatchDsVipProtocol = $AirWatchDsLbConfig.vipProtocol
$AirWatchDsVipPort = $AirWatchDsLbConfig.vipPort
$AirWatchDsServerPort = $AirWatchDsLbConfig.serverPort
$AirWatchDsLbMonitorName = $AirWatchDsLbConfig.LbMonitorName
$AirWatchDsLbMonitorUrl = $AirWatchDsLbConfig.LbMonitorUrl

# vIDM LoadBalancer Config
$vidmLbConfig = $nsxConfig.loadBalancers.vIDM.vIDM
$vidmLbEdgeName = $vidmLbConfig.lbEdgeName
$vidmLbPrimaryIPAddress = $vidmLbConfig.LbPrimaryIPAddress
$vidmVipIp = $vidmLbConfig.vipIp
$vidmLbAlgorithm = $vidmLbConfig.lbAlgorithm
$vidmLbPoolName = $vidmLbConfig.lbPoolName
$vidmVipName = $vidmLbConfig.vipName
$vidmAppProfileName = $vidmLbConfig.appProfileName
$vidmPersistenceMethod = $vidmLbConfig.PersistenceMethod
$vidmPersistenceExpires = $vidmLbConfig.PersistenceExpires
$vidmVipProtocol = $vidmLbConfig.vipProtocol
$vidmVipPort = $vidmLbConfig.vipPort
$vidmServerPort = $vidmLbConfig.serverPort
$vidmLbMonitorName = $vidmLbConfig.LbMonitorName
$vidmLbMonitorUrl = $vidmLbConfig.LbMonitorUrl

# vIDM UAG LoadBalancer Config
$vidmUagLbConfig = $nsxConfig.loadBalancers.vIDM.UAG
$vidmUagLbEdgeName = $vidmUagLbConfig.lbEdgeName
$vidmUagLbPrimaryIPAddress = $vidmUagLbConfig.LbPrimaryIPAddress
$vidmUagVipIp = $vidmUagLbConfig.vipIp
$vidmUagLbAlgorithm = $vidmUagLbConfig.lbAlgorithm
$vidmUagLbPoolName = $vidmUagLbConfig.lbPoolName
$vidmUagVipName = $vidmUagLbConfig.vipName
$vidmUagAppProfileName = $vidmUagLbConfig.appProfileName
$vidmUagPersistenceMethod = $vidmUagLbConfig.PersistenceMethod
$vidmUagPersistenceExpires = $vidmUagLbConfig.PersistenceExpires
$vidmUagVipProtocol = $vidmUagLbConfig.vipProtocol
$vidmUagVipPort = $vidmUagLbConfig.vipPort
$vidmUagServerPort = $vidmUagLbConfig.serverPort
$vidmUagLbMonitorName = $vidmUagLbConfig.LbMonitorName
$vidmUagLbMonitorUrl = $vidmUagLbConfig.LbMonitorUrl

If($error){throw "Failed to validate the required configuration settings"}


function deployLoadBalancer{
    param(
        [Array]$vmArray,
        $LbEdgeName,
        $TransportZoneName,
        $LogicalSwitchName,
        $LbPrimaryIPAddress,
        $vipIp,
        $EdgeClusterName,
        $EdgeDatastoreName,
        $EdgeDefaultGW,
        $LbMonitorName,
        $LbMonitorUrl,
        $LbPoolName,
        $LbAlgorithm,
        $serverPort,
        $AppProfileName,
        $PersistenceMethod,
        $PersistenceExpires,
        $vipProtocol,
        $vipName,
        $vipPort
    )

    $EdgeCluster = get-cluster $EdgeClusterName -errorAction Stop
    $EdgeDatastore = get-datastore $EdgeDatastoreName -errorAction Stop
    $LogicalSwitch = Get-NsxTransportZone $TransportZoneName | Get-NsxLogicalSwitch $LogicalSwitchName

    # Deploy a 1-arm load balancer
    write-host -foregroundcolor "Green" "Deploying new Load Balancer $LbEdgeName"
    
    # Defining the uplink and internal interfaces to be used when deploying the edge.
    $edgevnic0 = New-NsxEdgeinterfacespec -index 0 -Name "OneArmLb" -type Uplink -ConnectedTo $LogicalSwitch -PrimaryAddress $LbPrimaryIPAddress -SecondaryAddresses $vipIp -SubnetPrefixLength $defaultSubnetBits
    
    # Deploy appliance with the defined uplinks
    $LbEdge = New-NsxEdge -name $LbEdgeName -cluster $EdgeCluster -datastore $EdgeDatastore -Interface $edgevnic0 -Password $AppliancePassword -FwDefaultPolicyAllow
    
    # Configure Edge DGW
    Get-NSXEdge $LbEdgeName | Get-NsxEdgeRouting | Set-NsxEdgeRouting -DefaultGatewayAddress $EdgeDefaultGW -confirm:$false #| out-null

    # Enable NSX Load Balancer on the Edge
    Get-NsxEdge $LbEdgeName | Get-NsxLoadBalancer | Set-NsxLoadBalancer -Enabled #| out-null

    # Configure the default monitor and set it to be used
    $LbEdge = Get-NsxEdge $LbEdgeName
    ($LbEdge.features.loadBalancer.monitor | Where {$_.name -eq "$LBMonitorName"}).url = "$LbMonitorUrl"
    $LbEdge | Set-NsxEdge -confirm:$false

    $monitor = $LbEdge | Get-NsxLoadBalancer | Get-NsxLoadBalancerMonitor -Name $LBMonitorName
    
    # Define pool members.
    write-host -foregroundcolor Green "Creating Load Balancer Pool"

    # Create the Load Blancer pool
    $LbPool =  Get-NsxEdge $LbEdgeName | Get-NsxLoadBalancer | New-NsxLoadBalancerPool -name $LbPoolName -Description "Server Pool" -Transparent:$false -Algorithm $LbAlgorithm -Monitor $Monitor

    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}

        $server = $vmArray[$i]
        Write-Host "Adding $($server.Name) to the Load Balancer Pool" -ForegroundColor Blue
        Get-NsxEdge $LbEdgeName | Get-NsxLoadBalancer | Get-NsxLoadBalancerPool $LbPoolName | Add-NsxLoadBalancerPoolMember -Name $server.Name -IpAddress $server.IP -Port $serverPort #| out-null
    }

    # Create App Profiles.
    write-host "Creating Application Profiles" -foregroundcolor Green
    $AppProfile = Get-NsxEdge $LbEdgeName | Get-NsxLoadBalancer | New-NsxLoadBalancerApplicationProfile -Name $AppProfileName -Type $vipProtocol -SslPassthrough -PersistenceMethod $PersistenceMethod -PersistenceExpiry $PersistenceExpires

    # Create the VIPs.
    write-host "Creating VIPs" -foregroundcolor Green
    Get-NsxEdge $LbEdgeName | Get-NsxLoadBalancer | Add-NsxLoadBalancerVip -name $vipName -Description $vipName -ipaddress $vipIp -Protocol $vipProtocol -Port $vipPort -ApplicationProfile $AppProfile -DefaultPool $LbPool -AccelerationEnabled #| out-null

    # Move the NSX LB VMs to the VM Folder
    Move-VM -VM "$LbEdgeName*" -Destination $nsxFolderName -ErrorAction SilentlyContinue
}


#############################################################################################################################
# Script Starts Here
#############################################################################################################################

# Connect to the Management vCenter
try {
    Connect-NsxServer -vCenterServer $MgmtvCenterServer -Username $MgmtvCenterUserName -Password $MgmtvCenterPassword #| out-null
} catch {
    Write-Error "Failed connecting.  Check connection details and try again.  $_"
}
if($global:defaultVIServers.Name -contains $MgmtvCenterServer){
    Write-Host "Successfully connected to $MgmtvCenterServer" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

If($deployCsLb){
    If(Get-NsxEdge -Name $horizonLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$horizonLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $connectionServers -LbEdgeName $horizonLbEdgeName -LogicalSwitchName $mgmtEucLogicalSwitchName -TransportZoneName $mgmtEucTransportZoneName -LbPrimaryIPAddress $horizonLbPrimaryIPAddress -vipIp $horizonVipIp -EdgeClusterName $mgmtEdgeClusterName -EdgeDatastoreName $mgmtEdgeDatastoreName -EdgeDefaultGW $mgmtLdrEUCMGMTPrimaryAddress -LBMonitorName $horizonLBMonitorName -LbMonitorUrl $horizonLBMonitorURL -LbPoolName $horizonLbPoolName -LbAlgorithm $horizonLbAlgorithm -serverPort $horizonServerPort -AppProfileName $horizonAppProfileName -vipProtocol $horizonVipProtocol -PersistenceMethod $horizonPersistenceMethod -PersistenceExpires $horizonPersistenceExpires -vipName $horizonVipName -vipPort $horizonVipPort
    }
}

If($deployAwCnLb){
    If(Get-NsxEdge -Name $airwatchCnLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$airwatchCnLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $airwatchCnServers -LbEdgeName $airwatchCnLbEdgeName -LogicalSwitchName $mgmtEucLogicalSwitchName -TransportZoneName $mgmtEucTransportZoneName -LbPrimaryIPAddress $airwatchCnLbPrimaryIPAddress -vipIp $airwatchCnVipIp -EdgeClusterName $mgmtEdgeClusterName -EdgeDatastoreName $mgmtEdgeDatastoreName -EdgeDefaultGW $mgmtLdrEUCMGMTPrimaryAddress -LBMonitorName $airwatchCnLBMonitorName -LbMonitorUrl $airwatchCnLBMonitorURL -LbPoolName $airwatchCnLbPoolName -LbAlgorithm $airwatchCnLbAlgorithm -serverPort $airwatchCnServerPort -AppProfileName $airwatchCnAppProfileName -PersistenceMethod $airwatchCnPersistenceMethod -PersistenceExpires $airwatchCnPersistenceExpires -vipProtocol $airwatchCnVipProtocol -vipName $airwatchCnVipName -vipPort $airwatchCnVipPort
    }
}

Disconnect-NsxServer * -Confirm:$false


# Connect to DMZ vCenter Servers
try {
    Connect-NsxServer -vCenterServer $dmzvCenterServer -Username $dmzvCenterUserName -Password $dmzvCenterPassword #| out-null
} catch {
    Write-Error "Failed connecting.  Check connection details and try again.  $_"
}
if($global:defaultVIServers.Name -contains $dmzvCenterServer){
    Write-Host "Successfully connected to $dmzvCenterServer" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

If($deployDmzAwDsLb){
    If(Get-NsxEdge -Name $airwatchDsLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$airwatchDsLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $airwatchDsServers -LbEdgeName $airwatchDsLbEdgeName -LogicalSwitchName $dmzEucLogicalSwitchName -TransportZoneName $dmzEucTransportZoneName -LbPrimaryIPAddress $airwatchDsLbPrimaryIPAddress -vipIp $airwatchDsVipIp -EdgeClusterName $dmzEdgeClusterName -EdgeDatastoreName $dmzEdgeDatastoreName -EdgeDefaultGW $dmzLdrEUCMGMTPrimaryAddress -LBMonitorName $airwatchDsLBMonitorName -LbMonitorUrl $airwatchDsLBMonitorURL -LbPoolName $airwatchDsLbPoolName -LbAlgorithm $airwatchDsLbAlgorithm -serverPort $airwatchDsServerPort -AppProfileName $airwatchDsAppProfileName -PersistenceMethod $airwatchDsPersistenceMethod -PersistenceExpires $airwatchDsPersistenceExpires -vipProtocol $airwatchDsVipProtocol -vipName $airwatchDsVipName -vipPort $airwatchDsVipPort
    }
}
If($deployDmzIdmLb){
    If(Get-NsxEdge -Name $vidmLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$vidmLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $vidmServers -LbEdgeName $vidmLbEdgeName -LogicalSwitchName $dmzEucLogicalSwitchName -TransportZoneName $dmzEucTransportZoneName -LbPrimaryIPAddress $vidmLbPrimaryIPAddress -vipIp $vidmVipIp -EdgeClusterName $dmzEdgeClusterName -EdgeDatastoreName $dmzEdgeDatastoreName -EdgeDefaultGW $dmzLdrEUCMGMTPrimaryAddress -LBMonitorName $vidmLBMonitorName -LbMonitorUrl $vidmLBMonitorURL -LbPoolName $vidmLbPoolName -LbAlgorithm $vidmLbAlgorithm -serverPort $vidmServerPort -AppProfileName $vidmAppProfileName -PersistenceMethod $vidmPersistenceMethod -PersistenceExpires $vidmPersistenceExpires -vipProtocol $vidmVipProtocol -vipName $vidmVipName -vipPort $vidmVipPort
    }
}
If($deployDmzIdmUagLb){
    If(Get-NsxEdge -Name $vidmUagLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$vidmUagLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $vidmUagServers -LbEdgeName $vidmUagLbEdgeName -LogicalSwitchName $dmzEucLogicalSwitchName -TransportZoneName $dmzEucTransportZoneName -LbPrimaryIPAddress $vidmUagLbPrimaryIPAddress -vipIp $vidmUagVipIp -EdgeClusterName $dmzEdgeClusterName -EdgeDatastoreName $dmzEdgeDatastoreName -EdgeDefaultGW $dmzLdrEUCMGMTPrimaryAddress -LBMonitorName $vidmUagLBMonitorName -LbMonitorUrl $vidmUagLBMonitorURL -LbPoolName $vidmUagLbPoolName -LbAlgorithm $vidmUagLbAlgorithm -serverPort $vidmUagServerPort -AppProfileName $vidmUagAppProfileName -PersistenceMethod $vidmUagPersistenceMethod -PersistenceExpires $vidmPersistenceExpires -vipProtocol $vidmUagVipProtocol -vipName $vidmUagVipName -vipPort $vidmUagVipPort
    }
}
If($deployDmzCsUagLb){
    If(Get-NsxEdge -Name $horizonUagLbEdgeName -errorAction SilentlyContinue){
        Write-Error "$horizonUagLbEdgeName already exists!!!"
    } Else {
        deployLoadBalancer -vmArray $horizonUagServers -LbEdgeName $horizonUagLbEdgeName -LogicalSwitchName $dmzEucLogicalSwitchName -TransportZoneName $dmzEucTransportZoneName -LbPrimaryIPAddress $horizonUagLbPrimaryIPAddress -vipIp $horizonUagVipIp -EdgeClusterName $dmzEdgeClusterName -EdgeDatastoreName $dmzEdgeDatastoreName -EdgeDefaultGW $dmzLdrEUCMGMTPrimaryAddress -LBMonitorName $horizonUagLBMonitorName -LbMonitorUrl $horizonUagLBMonitorURL -LbPoolName $horizonUagLbPoolName -LbAlgorithm $horizonUagLbAlgorithm -serverPort $horizonUagServerPort -AppProfileName $horizonUagAppProfileName -PersistenceMethod $horizonUagPersistenceMethod -PersistenceExpires $horizonUagPersistenceExpires -vipProtocol $horizonUagVipProtocol -vipName $horizonUagVipName -vipPort $horizonUagVipPort
    }
}

Write-Host "`n The deployment and configuraiton of NSX Load Balancers is complete `n" -ForegroundColor Green

Disconnect-NsxServer * -Confirm:$false
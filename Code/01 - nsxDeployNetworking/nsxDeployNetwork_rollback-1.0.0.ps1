<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     nsxDeployNetwork.ps1
 Example:      nsxDeployNetwork.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>
<#
param( 
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
    )

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
#>

If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    Write-Host "Refreshing JSON config"
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}

$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.horizonCertificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$connectionServerConfig = $horizonConfig.connectionServerConfig
$blockvCenterConfig = $horizonConfig.blockvCenters
$horizonCertificateConfig = $horizonConfig.certificateConfig
$nsxEdgeConfig = $nsxConfig.edge

# NSX Infrastructure Configuration.
$NsxManagerServer = $nsxConfig.nsxManagerServer
$NsxManagerPassword = $nsxConfig.nsxManagerPassword
$MgmtvCenterServer = $mgmtvCenterConfig.vCenter

#vSphereDetails for NSX
$mgmtvCenterUserName = $serviceAccounts.horizonServiceAccount.username
$mgmtvCenterPassword = $serviceAccounts.horizonServiceAccount.password
$mgmtClusterName = $mgmtvCenterConfig.cluster
$mgmtVdsName = $mgmtvCenterConfig.distributedSwitch
$mgmtEdgeClusterName = $mgmtvCenterConfig.edgeCluster
$mgmtEdgeDatastoreName = $mgmtvCenterConfig.edgeDatastore
$mgmtNsxUplinkPortGroup01Name = $nsxEdgeConfig.nsxUplinkPortGroup01Name
$mgmtNsxUplinkPortGroup02Name = $nsxEdgeConfig.nsxUplinkPortGroup02Name
$mgmtNsxFolderName = $vmFolders.nsx
$mgmtDatacenterName = $mgmtvCenterConfig.datacenter

# Logical Topology environment
$tor01UplinkProtocolAddress = $nsxConfig.tor01UplinkProtocolAddress
$tor02UplinkProtocolAddress = $nsxConfig.tor02UplinkProtocolAddress
$Edge01Uplink01PrimaryAddress = $nsxEdgeConfig.edge01Uplink01PrimaryAddress
$Edge01Uplink02PrimaryAddress = $nsxEdgeConfig.edge01Uplink02PrimaryAddress
$Edge02Uplink01PrimaryAddress = $nsxEdgeConfig.edge02Uplink01PrimaryAddress
$Edge02Uplink02PrimaryAddress = $nsxEdgeConfig.edge02Uplink02PrimaryAddress
$bgpPassword = $nsxEdgeConfig.bgpPassword
$uplink01ASN = $nsxEdgeConfig.uplink01ASN
$uplink02ASN = $nsxEdgeConfig.uplink02ASN
$LocalASN = $nsxEdgeConfig.localASN
$keepAliveTimer = $nsxEdgeConfig.keepAliveTimer
$holdDownTimer = $nsxEdgeConfig.holdDownTimer
$AppliancePassword = $nsxEdgeConfig.edgePassword
$mgmtTransitLsName = $nsxConfig.logicalSwitches.eucMgmtTransit
$mgmtEdgeHAPortGroupName = $nsxEdgeConfig.edgeHAPortGroupName
$eucMgmtNetwork = $nsxConfig.logicalSwitches.eucMgmtNetwork
$mgmtEdge01Name = $nsxEdgeConfig.edge01Name
$mgmtEdge02Name = $nsxEdgeConfig.edge02Name
$mgmtLdrName = $nsxEdgeConfig.ldrName
$mgmtEUCTransportZoneName = $nsxEdgeConfig.EUCTransportZoneName

#Logical Networking Topology
$mgmtEdge01InternalPrimaryAddress = $nsxEdgeConfig.edge01InternalPrimaryAddress
$mgmtEdge02InternalPrimaryAddress = $nsxEdgeConfig.edge02InternalPrimaryAddress
$mgmtLdrUplinkPrimaryAddress = $nsxEdgeConfig.ldrUplinkPrimaryAddress
$mgmtLdrUplinkProtocolAddress = $nsxEdgeConfig.ldrUplinkProtocolAddress
$mgmtLdrEUCMGMTPrimaryAddress = $nsxEdgeConfig.ldrEUCMGMTPrimaryAddress
$mgmtDefaultSubnetBits = $nsxEdgeConfig.defaultSubnetBits
$mgmtDlrHaDatastoreName = $nsxEdgeConfig.dlrHaDatastoreName

#Connection Server VMs
$csServers = $connectionServerConfig.ConnectionServers

# Horizon CS LoadBalancer Config
$horizonCsLbConfig = $nsxConfig.loadBalancers.Horizon.ConnectionServers
$horizonLbEdgeName = $horizonCsLbConfig.lbEdgeName
$horizonLbPrimaryIPAddress = $horizonCsLbConfig.lbPrimaryIPAddress
$horizonVipIp = $horizonCsLbConfig.vipIp
$horizonLbAlgorithm = $horizonCsLbConfig.lbAlgorithm
$horizonLbPoolName = $horizonCsLbConfig.lbPoolName
$horizonVipName = $horizonCsLbConfig.vipName
$horizonAppProfileName = $horizonCsLbConfig.appProfileName
$horizonVipProtocol = $horizonCsLbConfig.vipProtocol
$horizonHttpsPort = $horizonCsLbConfig.httpsPort
$horizonLBMonitorName = $horizonCsLbConfig.lBMonitorName

##IPset
$horizonVIP_IpSet_Name = $nsxConfig.horizonVIP_IpSet_Name
$horizonInternalESG_IpSet_Name = $nsxConfig.horizonInternalESG_IpSet_Name

If($error){throw "Failed to validate the required configuration settings"}


#Get Connection required.
try {
    Connect-NsxServer -vCenterServer $MgmtvCenterServer -Username $MgmtvCenterUserName -Password $MgmtvCenterPassword | out-null
} catch {
    Throw "Failed connecting.  Check connection details and try again.  $_"
}

Get-NsxEdge | Where-Object{($_.Name -eq $mgmtEdge01Name) -or ($_.Name -eq $mgmtEdge02Name) -or ($_.Name -eq $horizonLbEdgeName)} | Remove-NsxEdge -Confirm:$False
Get-NsxLogicalRouter | Where-Object{$_.name -eq $mgmtLdrName} | Remove-NsxLogicalRouter -Confirm:$False
Start-Sleep 2
Get-NsxLogicalSwitch | Where-Object{($_.name -eq $HorizonLsName) -or ($_.name -eq $mgmtTransitLsName) -or ($_.Name -eq $eucMgmtNetwork)} | Remove-NsxLogicalSwitch -Confirm:$False

If(Get-NsxLogicalSwitch $mgmtEdgeHAPortGroupName){
    Get-NsxLogicalSwitch $mgmtEdgeHAPortGroupName | Remove-NsxLogicalSwitch -Confirm:$False
}
Get-VDPortgroup | Where-Object{($_.Name -eq $mgmtNsxUplinkPortGroup01Name) -or ($_.Name -eq $mgmtNsxUplinkPortGroup02Name)} | Remove-VDPortGroup -Confirm:$False

If(Get-Folder $mgmtNsxFolderName -ErrorAction SilentlyContinue){
    Remove-Folder $mgmtNsxFolderName -DeletePermanently -Confirm:$false
}
Write-Host "Rolled back NSX configuraiton `n" -ForegroundColor Green

Disconnect-NsxServer * -Confirm:$false
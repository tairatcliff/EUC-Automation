<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     buildDesktopPools.ps1
 Example:      buildDesktopPools.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>
If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}
$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$appRootDirectory = $deploymentDirectories.appRootDirectory
$deploymentSourceDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$certificateDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.certificateDirectory
$toolsDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.toolsDirectory
$codeDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.codeDirectory
$configFilesDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.configFilesDirectory
$destinationConfigFilesDirectory = Join-Path -Path $deploymentDestinationDirectory -ChildPath $deploymentDirectories.configFilesDirectory

# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
# Service Accounts
$airwatchServiceAccountName = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$horizonServiceAccountName = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$localDomainAdminUser = $serviceAccounts.localDomainAdmin.username
$localDomainAdminPassword = $serviceAccounts.localDomainAdmin.password
$domainJoinUser = $serviceAccounts.domainJoin.username
$domainJoinPassword = $serviceAccounts.domainJoin.password

# Import arrays of VMs to deploy
$installCn = $airwatchConfig.airwatchServers.CN.installCn
$installDs = $airwatchConfig.airwatchServers.DS.installDs
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$blockvCenters = $horizonConfig.blockvCenters
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers

$horizonInstallBinaryName = $binaries.horizonBinary
$horizonInstallBinary = Get-Item -Path (Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinaryName)
$hznRecoveryPassword = $horizonConfig.connectionServerConfig.horizonRecoveryPassword
$hznRecoveryPasswordHint = $horizonConfig.connectionServerConfig.horizonRecoveryPasswordHint
$horizonDestinationBinary = Join-Path -Path $deploymentDestinationDirectory -ChildPath $horizonInstallBinaryName
$domainName = $domainConfig.domainName
$netbios = $domainConfig.netbios
$dnsServer = $domainConfig.dnsServer
$horizonConnectionServerURL = $horizonConfig.connectionServerConfig.horizonConnectionServerURL
$horizonLicenseKey = $licenseKeys.horizon


If($error){throw "Failed to validate the required configuration settings"}

function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}

If($global:defaultHVServers.Name -notcontains $horizonConnectionServerURL){
	connect-hvserver -server $horizonConnectionServerURL -user $horizonServiceAccountName -password $horizonServiceAccountPassword -domain $domainName -WarningAction SilentlyContinue #| Out-Null
}
If($global:defaultHVServers.Name -contains $horizonConnectionServerURL){
	Write-Host "`nSuccessfully connected to $horizonConnectionServerURL" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to Horizon Connection Server"
}

# Connect to each block vCenter and create desktop pools
for($i = 0; $i -lt $blockvCenters.count; $i++){
    $blockvCenterName = $blockvCenters[$i].Name
    $blockvCenterConfig = $blockvCenters[$i].blockConfig

    $datacenterName = $blockvCenterConfig.blockDatacenterName
    $ignoreSSL = $blockvCenterConfig.ignoreSSLValidation

    # Connect to the block vCenter 
    if($global:defaultVIServers.Name -notcontains $blockvCenterName){
        Connect-VIServer -Server $blockvCenterName -User $horizonServiceAccountName -Password $horizonServiceAccountPassword -Force #| Out-Null
    }
    if($global:defaultVIServers.Name -contains $blockvCenterName){
        Write-Host "Successfully connected to $blockvCenterName" -ForegroundColor Green `n
    } Else {
        throw "Unable to connect to vCenter Server"
    }

    # Create each pool within the block vCenter
    for($j = 0; $j -lt $blockvCenterConfig.poolConfig.count; $j++){
        $desktopPool = $blockvCenterConfig.poolConfig[$j]

        If($desktopPool.DeployPool){
            
            $poolType = $desktopPool.poolType.toupper()
            
            # Create a folder for the Desktop Pool VMs
            $folderName = $desktopPool.VmFolder

            if(get-Folder -Name $folderName -ErrorAction Ignore){
                Write-Host "Found an existing $foldername VM Folder in vCenter. This is where the desktops will be deployed." -ForegroundColor Yellow `n
            } Else {
                Write-Host "The $foldername VM folder does not exist, creating a new folder" -BackgroundColor Yellow -ForegroundColor Black `n
                (Get-View (Get-View -viewtype datacenter -filter @{"name"="$datacenterName"}).vmfolder).CreateFolder("$folderName") #| Out-Null
            }
            
            # Change the network on the master VM template so that the pool provisions to the correct Network           
            $parentVM = $desktopPool.ParentVM
            $networkAdapter = Get-VM $parentVM | Get-NetworkAdapter -Name "Network adapter 1"
            $networkPortGroup =  Get-VDPortgroup -Name $desktopPool.networkPortGroup
            Set-NetworkAdapter -NetworkAdapter $networkAdapter -Portgroup $networkPortGroup -Confirm:$false ##| Out-Null

            switch ($poolType){
                "INSTANTCLONE"{ 
                    Write-Host "Creating new $poolType pool named $($desktopPool.PoolName)"
                    New-HVPool -InstantClone -PoolName $desktopPool.PoolName -PoolDisplayName $desktopPool.PoolDisplayName -Description $desktopPool.Description -UserAssignment $desktopPool.UserAssignment -ParentVM $desktopPool.ParentVM -SnapshotVM $desktopPool.SnapshotVM -VmFolder $desktopPool.VmFolder -HostOrCluster $desktopPool.HostOrCluster -ResourcePool $desktopPool.ResourcePool -NamingMethod $desktopPool.NamingMethod -Datastores $desktopPool.Datastores -NamingPattern  $desktopPool.NamingPattern -NetBiosName $desktopPool.NetBiosName -DomainAdmin $desktopPool.DomainAdmin -vCenter $blockvCenterName -MinimumCount $desktopPool.MinimumCount -MaximumCount $desktopPool.MaximumCount #| Out-Null
                }
                "FULLCLONE"{ 
                    Write-Host "Creating new $poolType pool named $($desktopPool.PoolName)"
                    New-HVPool -FullClone -PoolName $desktopPool.PoolName -PoolDisplayName $desktopPool.PoolDisplayName  -Description $desktopPool.Description -UserAssignment $desktopPool.UserAssignment -VmFolder $desktopPool.VmFolder -HostOrCluster $desktopPool.HostOrCluster -ResourcePool $desktopPool.ResourcePool -NamingMethod $desktopPool.NamingMethod -Datastores $desktopPool.Datastores -NamingPattern  $desktopPool.NamingPattern -NetBiosName $desktopPool.NetBiosName -vCenter $blockvCenterName -Template $desktopPool.Template -SysPrepName $desktopPool.SysPrepName -CustType $desktopPool.CustType #| Out-Null
                }
                "RDS"{
                    #Write-Host "Creating new $poolType pool named $($desktopPool.PoolName)"
                    #New-HVPool - -PoolName $desktopPool.PoolName  -PoolDisplayName $desktopPool.PoolDisplayName  -Description $desktopPool.Description -UserAssignment $desktopPool.UserAssignment -VmFolder $desktopPool.VmFolder -HostOrCluster $desktopPool.HostOrCluster -ResourcePool $desktopPool.ResourcePool -NamingMethod $desktopPool.NamingMethod -Datastores $desktopPool.Datastores -NamingPattern  $desktopPool.NamingPattern -NetBiosName $desktopPool.NetBiosName -vCenter $blockvCenterName  -Template $desktopPool.Template -SysPrepName $desktopPool.SysPrepName -CustType $desktopPool.CustType #| Out-Null
                }			    
                default {
                    Write-Error "$poolType pool $($desktopPool.PoolName) failed to create. Only Types 'INSTANTCLONE', 'FULLCLONE' or 'RDS' are accepted."
                    Continue
                }
            }
            
            If ((Get-HVPool).Base.Name -eq $desktopPool.PoolName){
                Write-Host "Desktop Pool created successfully!" `n -ForegroundColor Green
            }
            #need to let the pool finish being created before doing the entitlement
            Start-Sleep -Seconds 60
            #if it exists, do the entitlement
            foreach ($entitlement in $desktopPool.entitlement){
                if (![string]::IsNullOrEmpty($entitlement.group)){
                    foreach ($group in $entitlement.group){
                        New-HVEntitlement -User $group -ResourceName $desktopPool.PoolName -ResourceType Desktop -Type Group
                    }	
                }
                
                if (![string]::IsNullOrEmpty($entitlement.user)){
                    foreach ($user in $entitlement.user){
                        New-HVEntitlement -User $user -ResourceName $desktopPool.PoolName -ResourceType Desktop -Type User
                    }
                }
            }
        }
    }
    
    # Disconnect from the block vCenter in preperation for the next block
    if($global:DefaultVIServer){
        Disconnect-VIServer * -Confirm:$false #| Out-Null
    }
}
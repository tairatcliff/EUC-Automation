<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     cloneVMs.ps1
 Example:      cloneVMs.ps1 -eucConfigJson eucConfig.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$error.Clear()


# Global variables from JSON
# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig


# Script specific variables from JSON
$deploymentSourceDirectory = Get-Item -path $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$certificateDirectory = $deploymentDirectories.certificateDirectory
# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
# Service Accounts
$airwatchServiceAccountName = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$horizonServiceAccountName = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$localDomainAdminUser = $serviceAccounts.localDomainAdmin.username
$localDomainAdminPassword = $serviceAccounts.localDomainAdmin.password
$domainJoinUser = $serviceAccounts.domainJoin.username
$domainJoinPassword = $serviceAccounts.domainJoin.password
# Import arrays of VMs to deploy
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers
# CA SSL Certificate config
$pfxFileName = $globalConfig.sslCertificates.airwatch.pfxFileName
$pfxPassword = $globalConfig.sslCertificates.airwatch.pfxPassword
$pfxSourceFilePath = Join-Path -Path $certificateDirectory -ChildPath $pfxFileName
$pfxDestinationFilePath = Join-Path -Path $deploymentDestinationDirectory -ChildPath $pfxFileName
# AirWatch Cert Config
$airwatchCnCommonName = $airwatchConfig.certificateConfig.cnCommonName
$airwatchDsCommonName = $airwatchConfig.certificateConfig.dsCommonName
# IIS Config
$iisSite = $airwatchConfig.iis.iisSiteName
# Windows Source files for installing Windows Features like IIS and .NET
$windowsSourceSxsZip = $binaries.windowsSourceZip
$windowsSourceSxsZipPath = Join-Path -Path $deploymentSourceDirectory -ChildPath $windowsSourceSxsZip
$windowsDestinationSxsZipPath = Join-Path -Path $deploymentDestinationDirectory -ChildPath $windowsSourceSxsZip
$windowsDestinationSxsFolderPath = Join-Path -Path $deploymentDestinationDirectory -ChildPath "\sxs\"
$dotNetBinary = $binaries.dotNet
$dotNetSourcePath = Join-Path -Path $deploymentSourceDirectory -ChildPath $dotNetBinary
$dotNetDesitionationPath = Join-Path -Path $deploymentDestinationDirectory -ChildPath $dotNetBinary
# AirWatch Install Zip
$airwatchInstallZipName = $binaries.airwatchZip
$airwatchInstallSourceZipPath = Join-Path -Path $deploymentSourceDirectory -ChildPath $airwatchInstallZipName
$airwatchInstallDestinationZipPath = Join-Path -Path $deploymentDestinationDirectory -ChildPath $airwatchInstallZipName

If($error){throw "Failed to validate the required configuration settings"}


function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}


function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
        $checkVM = $false
        Write-Host "Unable to find $vmName in vCenter!" `n -ForegroundColor Red
		return $checkVM
	}
	
	# Check if VM Tools is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -ne "GUESTTOOLSRUNNING"){
        $checkVM = $false	
        Write-Host "VMTools is not running on $vmName!" `n -ForegroundColor Red
		return $checkVM
	} 
    
    # Check if network is accessible
    if(Test-Connection -ComputerName $vmName -Count 1 -Quiet -ErrorAction SilentlyContinue){
    	$checkVM = $true	
		return $checkVM
	} else {
        $checkVM = $false
        Write-Host "Unable to reach $vmName over the network!" `n -ForegroundColor Red
		return $checkVM
    }
}


function checkVmTools{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName
	
	# Check if VM Tools is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -ne "GUESTTOOLSRUNNING"){
        $checkVM = $false	
        Write-Host "VMTools is not running on $vmName!" `n -ForegroundColor Red
		return $checkVM
    } 
    $checkVM = $true	
	return $checkVM
}


function waitForVMs{
    param(
        [Array]$vmArray
    )
    
    # Build an Array of VM Name strings
    $checkVMs = @()

    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
       If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
       
       $checkVMs += $vmArray[$i].Name
    }
    
    # Wait for up to 10 minutes for the VMs to respond before continuing
    for($i = 0; $i -lt 10; $i++){
        foreach($VM in $checkVMs){
            If(checkVM -vmName $VM){
                # If the VM is responding them remove it from the array so it doesn't get checked again.
                $checkVMs = $checkVMs | Where-Object {$_ -ne $VM}
            }
        }
        If(! $checkVMs){Break}
        Write-Host "$($checkVMs -join ", ") are not responding. Wait 1 minute and try again...." `n -ForegroundColor Yellow
        Start-Sleep 60
    }
    If(! $checkVMs){
        return $true
    } Else {
        return $false
    }
}


function rollbackToSnapshot{
    param(
        [Array]$vmArray,
        [String]$snapshotName = "Installing AirWatch"
    )

    for($i = 0; $i -lt $vmArray.count; $i++){ 
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        
        $vmName = $vmArray[$i].Name
    
        # Revert all VMs in array to snapshot
        Write-Host "Rolling back $vmName to the snapshot $snapshotName" `n -ForegroundColor White -backgroundColor Red
        $VmSnapshot = get-snapshot -VM $vmName -Name $snapshotName
        Set-VM -VM $vmName -Snapshot $snapshotName -Confirm:$false | Out-Null
    
        # Power the VM back on
        Start-VM $vmName | Out-Null
    }
    
    for($i = 0; $i -lt $vmArray.count; $i++){ 
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        $vmName = $vmArray[$i].Name
        waitForVMs -vmName $vmName | Out-Null
    }
}

#####################################################################################
######################           SCRIPT STARTS HERE            ######################
#####################################################################################

# Install the AirWatch CN Servers in the internal environment
# Connect to Management vCenter Servers
if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
	Connect-VIServer -Server $mgmtvCenterName -User $mgmtvCenterAccount -Password $mgmtvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterName){
	Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
} Else {
	Write-Error "Unable to connect to Management vCenter Server"
}

rollbackToSnapshot -vmArray $airwatchCnServers -snapshotName "Installing AirWatch"

Disconnect-VIServer * -Force -Confirm:$false


# Install the AirWatch DS Servers in the DMZ environment
# Connect to DMZ vCenter Servers
if($global:defaultVIServers.Name -notcontains $dmzvCenterName){
	Connect-VIServer -Server $dmzvCenterName -User $dmzvCenterAccount -Password $dmzvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $dmzvCenterName){
	Write-Host "Successfully connected to $dmzvCenterName" -ForegroundColor Green `n
} Else {
	Write-Error "Unable to connect to Management vCenter Server"
}

rollbackToSnapshot -vmArray $airwatchDsServers -snapshotName "Installing AirWatch"

Write-Host "Script Completed" -ForegroundColor Green `n


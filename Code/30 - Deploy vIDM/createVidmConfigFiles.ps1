<#
========================================================================
 Created on =   05/25/2018
 Created by =   Tai Ratcliff
 Organization = VMware	 
 Filename =     createConfigFiles.ps1
 Example =      createConfigFiles.ps1 -eucConfigJson eucConfig.json

 Internal Confluence page that describes all of hte possible settings can be found here:
 https://confluence-euc.eng.vmware.com/pages/viewpage.action?spaceKey=RM&title=AirWatch+Headless+Installers#AirWatchHeadlessInstallers-ConfigurationXML
========================================================================
#>

If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}

$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig
$vidmConfig = $eucConfig.vidmConfig

# Script specific variables from JSON
$appRootDirectory = $deploymentDirectories.appRootDirectory
$deploymentSourceDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$certificateDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.certificateDirectory
$toolsDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.toolsDirectory
$codeDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.codeDirectory
$configFilesDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.configFilesDirectory
# AirWatch XML Config Files
$xmlConfig = $vidmConfig.xmlConfig
$vidmConfigXmlPath = Join-Path -Path $configFilesDirectory -ChildPath $configFileNames.vidmConfig

If($error){throw "Failed to validate the required configuration settings"}


function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}


function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
		$checkVM = $false
		return $checkVM
	}
	
	# Check if VM Toosl is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -eq "GUESTTOOLSRUNNING"){
		$checkVM = $true	
		return $checkVM
	} else {
		$checkVM = $false
		return $checkVM
	}
}



#####################################################################################
######################           SCRIPT STARTS HERE            ######################
#####################################################################################

# General Config
$ADDLOCAL = $xmlConfig.ADDLOCAL
$IS_SQLSERVER_SERVER =  $xmlConfig.IS_SQLSERVER_SERVER
$IS_SQLSERVER_AUTHENTICATION = $xmlConfig.IS_SQLSERVER_AUTHENTICATION
$IS_SQLSERVER_USERNAME = $xmlConfig.IS_SQLSERVER_USERNAME
$IS_SQLSERVER_DATABASE = $xmlConfig.IS_SQLSERVER_DATABASE
$IS_SQLSERVER_PASSWORD = $xmlConfig.IS_SQLSERVER_PASSWORD
$AWIGNOREBACKUP = $xmlConfig.AWIGNOREBACKUP
$AWIDMHOSTNAME = $xmlConfig.AWIDMHOSTNAME
$AWIDMPORT = $xmlConfig.AWIDMPORT
$AWIDMEXTERNALHOSTNAME = $xmlConfig.AWIDMEXTERNALHOSTNAME
$AWIDMEXTERNALPORT = $xmlConfig.AWIDMEXTERNALPORT
$AWIDMUSECERT = $xmlConfig.AWIDMUSECERT
$AWIDMCERT = $xmlConfig.AWIDMCERT
$AWIDMCERTPASSWORD = $xmlConfig.AWIDMCERTPASSWORD
$AWIDMUSEPROXY =  $xmlConfig.AWIDMUSEPROXY
$AWIDMHTTPSPROXY =  $xmlConfig.AWIDMHTTPSPROXY
$AWIDMHTTPSPROXYPORT =  $xmlConfig.AWIDMHTTPSPROXYPORT
$AWIDMNONPROXYHOSTS =  $xmlConfig.AWIDMNONPROXYHOSTS
$AWIDMCLUSTERJOIN =  $xmlConfig.AWIDMCLUSTERJOIN
$AWIDMCLUSTERZIP =  $xmlConfig.AWIDMCLUSTERZIP
$AWIDMPASSWORD = $xmlConfig.AWIDMPASSWORD
$IDM_SQLSERVER_SERVER = $xmlConfig.IDM_SQLSERVER_SERVER
$IDM_SQLSERVER_DATABASE = $xmlConfig.IDM_SQLSERVER_DATABASE
$IDM_SQLSERVER_AUTHENTICATION = $xmlConfig.IDM_SQLSERVER_AUTHENTICATION
$IDM_SQLSERVER_USERNAME = $xmlConfig.IDM_SQLSERVER_USERNAME
$IDM_SQLSERVER_PASSWORD = $xmlConfig.IDM_SQLSERVER_PASSWORD
$AWIDMLITE =  $xmlConfig.AWIDMLITE
$AWIDMSERVICEACCOUNT =  $xmlConfig.AWIDMSERVICEACCOUNT
$IS_NET_API_LOGON_USERNAME =  $xmlConfig.IS_NET_API_LOGON_USERNAME
$IS_NET_API_LOGON_PASSWORD = $xmlConfig.IS_NET_API_LOGON_PASSWORD



# VIDM Config File
$vidmConfig = @"
<?xml version="1.0" encoding="utf-8"?>
<properties>
	<property name="ADDLOCAL" value="$ADDLOCAL" />
	<property name="IS_SQLSERVER_SERVER" value="$IS_SQLSERVER_SERVER" />
	<property name="IS_SQLSERVER_AUTHENTICATION" value="$IS_SQLSERVER_AUTHENTICATION" />
	<property name="IS_SQLSERVER_USERNAME" value="$IS_SQLSERVER_USERNAME" />
	<property name="IS_SQLSERVER_DATABASE" value="$IS_SQLSERVER_DATABASE" />
	<property name="IS_SQLSERVER_PASSWORD" value="$IS_SQLSERVER_PASSWORD" />
	<property name="AWIGNOREBACKUP" value="$AWIGNOREBACKUP" />
	<property name="AWIDMHOSTNAME" value="$AWIDMHOSTNAME"/>
	<property name="AWIDMPORT" value="$AWIDMPORT"/>
	<property name="AWIDMEXTERNALHOSTNAME" value="$AWIDMEXTERNALHOSTNAME"/>
	<property name="AWIDMEXTERNALPORT" value="$AWIDMEXTERNALPORT"/>
	<property name="AWIDMUSECERT" value="$AWIDMUSECERT"/>
	<property name="AWIDMCERT" value="$AWIDMCERT"/>
	<property name="AWIDMCERTPASSWORD" value="$AWIDMCERTPASSWORD"/>
	<property name="AWIDMUSEPROXY" value="$AWIDMUSEPROXY"/>
	<property name="AWIDMHTTPSPROXY" value="$AWIDMHTTPSPROXY"/>
	<property name="AWIDMHTTPSPROXYPORT" value="$AWIDMHTTPSPROXYPORT"/>
	<property name="AWIDMNONPROXYHOSTS" value="$AWIDMNONPROXYHOSTS"/>
	<property name="AWIDMCLUSTERJOIN" value="$AWIDMCLUSTERJOIN"/>
	<property name="AWIDMCLUSTERZIP" value="$AWIDMCLUSTERZIP"/>
	<property name="AWIDMPASSWORD" value="$AWIDMPASSWORD"/>
	<property name="IDM_SQLSERVER_SERVER" value="$IDM_SQLSERVER_SERVER"/>
	<property name="IDM_SQLSERVER_DATABASE" value="$IDM_SQLSERVER_DATABASE"/>
	<property name="IDM_SQLSERVER_AUTHENTICATION" value="$IDM_SQLSERVER_AUTHENTICATION"/>
	<property name="IDM_SQLSERVER_USERNAME" value="$IDM_SQLSERVER_USERNAME"/>
	<property name="IDM_SQLSERVER_PASSWORD" value="$IDM_SQLSERVER_PASSWORD"/>
	<property name="AWIDMLITE" value="$AWIDMLITE"/>
	<property name="AWIDMSERVICEACCOUNT" value="$AWIDMSERVICEACCOUNT"/>
	<property name="IS_NET_API_LOGON_USERNAME" value="$IS_NET_API_LOGON_USERNAME" />
	<property name="IS_NET_API_LOGON_PASSWORD" value="$IS_NET_API_LOGON_PASSWORD" />
</properties>
"@


Set-Content -Value $vidmConfig -Path $vidmConfigXmlPath -Confirm:$false


# Add the application directory to the JSON file. 
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJsonPath



Write-Host "Script Completed" -ForegroundColor Green `n


﻿# Ensure the build fails if there is a problem.
# The build will fail if there are any errors on the remote machine too!
$ErrorActionPreference = 'Stop'
 
# Create a PSCredential Object using the "User" and "Password" parameters that you passed to the job
#Set-Item WSMan:\localhost\Client\TrustedHosts *
 
$SecurePassword = $env:SSH_USER_PASSWORD | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $env:SSH_USER_NAME, $SecurePassword
 
Write-Output "Invoke a command on the remote machine."
# It depends on the type of job you are executing on the remote machine as to if you want to use "-ErrorAction Stop" on your Invoke-Command.
Invoke-Command -ComputerName $env:VA_IP -Credential $cred -argumentlist $env:INSTALLER_ZIP_URL,$env:SSH_USER_NAME,$env:AWIDMHOSTNAME,$env:AWIDMEXTERNALHOSTNAME,$env:IDM_SQLSERVER_SERVER,$env:AWIDMCLUSTERJOIN,$env:AWIDMCLUSTERZIP,$env:AWIDMPASSWORD,$env:ADDLOCAL,$env:AWIDMSERVICEACCOUNT,$env:IS_NET_API_LOGON_USERNAME,$env:IS_NET_API_LOGON_PASSWORD -ScriptBlock {
$zipFileUrlString=$args[0]
$sshUserName=$args[1]
 
Write-Output "Zip file is $zipFileUrlString"
 
$filename = $zipFileUrlString.Substring($zipFileUrlString.LastIndexOf("/") + 1)
 
if( "$zipFileUrlString" -match "bamboo.air-watch.com")
{
$filename1 = $zipFileUrlString.Substring($zipFileUrlString.LastIndexOf("/") + 1)
$filename=$filename1.Substring(0,$filename1.LastIndexOf("?"))
Write-Output "bamboo file   $filename"
}
 
Write-Output "$filename"
$zipFile = New-Object System.IO.FileInfo($filename)
#$zipFile
$zipFileBaseName=$zipFile.BaseName
 
Write-Output "Remove old data complete."
Remove-Item C:\$zipFileBaseName* -Force -recurse
 
Write-Output "$zipFile"
$zipFileOutputPath = "C:\Installer\$zipFile"
Write-Output "zipFileOutputPath is $zipFileOutputPath."
$start_time = Get-Date
 
#$uri=[System.Uri]$zipFileUrlString
Write-Output "uri is $uri"
 
Write-Output "Start downloading file $zipFileUrlString"
$wc = New-Object System.Net.WebClient
$wc.Headers["User-Agent"] = "Chrome"
$wc.DownloadFile($zipFileUrlString, $zipFileOutputPath)
 
Write-Output "Download complete."
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
$Installer="C:\Installer"
if("$zipFileUrlString" -match "zip")
{
Write-Output "Unzip a file"
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Write-Output "Unzipping to C:\"
Unzip "$zipFileOutputPath" "C:\"
$Installer="C:\Installer"
}
#PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command #"C:\temp\AddAccountToLogonAsService.ps1" "$sshUserName"
 
#$Installer="C:\Installer"
$installlog="C:\installs\Install.log"
 
$path = 'C:\installs\vidm-installer-config.xml'
$AWIDMHOSTNAME = $args[2]
$AWIDMEXTERNALHOSTNAME = $args[3]
$IDM_SQLSERVER_SERVER = $args[4]
$AWIDMCLUSTERJOIN = $args[5]
$AWIDMCLUSTERZIP = $args[6]
$AWIDMPASSWORD=$args[7]
$ADDLOCAL=$args[8]
$AWIDMSERVICEACCOUNT=$args[9]
$IS_NET_API_LOGON_USERNAME=$args[10]
$IS_NET_API_LOGON_PASSWORD=$args[11]
 
 
Write-Output "$AWIDMHOSTNAME"
$xml = (Get-Content $path) -as [Xml]
 
#$xml.properties.property."name" = "AWIDMHOSTNAME"
#$xml.Save('winbat-sva-001.hs.trcint.com')
 
$node = $xml.properties.property | ? { $_.name -eq "ADDLOCAL" }
Write-Output "$node"
$node.value = $ADDLOCAL
 
$node = $xml.properties.property | ? { $_.name -eq "AWIDMHOSTNAME" }
Write-Output "$node"
$node.value = $AWIDMHOSTNAME
 
$node = $xml.properties.property | ? { $_.name -eq "AWIDMEXTERNALHOSTNAME" }
Write-Output "$node"
$node.value = $AWIDMEXTERNALHOSTNAME
 
$node = $xml.properties.property | ? { $_.name -eq "IDM_SQLSERVER_SERVER" }
Write-Output "$node"
$node.value = $IDM_SQLSERVER_SERVER
 
if (-not ([string]::IsNullOrEmpty($AWIDMCLUSTERJOIN)))
{
$node = $xml.properties.property | ? { $_.name -eq "AWIDMCLUSTERJOIN" }
Write-Output "$node"
$node.value = $AWIDMCLUSTERJOIN
}
if (-not ([string]::IsNullOrEmpty($AWIDMCLUSTERZIP)))
{
$node = $xml.properties.property | ? { $_.name -eq "AWIDMCLUSTERZIP" }
Write-Output "$node"
$node.value = $AWIDMCLUSTERZIP
}
 
if (-not ([string]::IsNullOrEmpty($AWIDMPASSWORD)))
{
$node = $xml.properties.property | ? { $_.name -eq "AWIDMPASSWORD" }
Write-Output "$node"
$node.value = $AWIDMPASSWORD
}
 
if (-not ([string]::IsNullOrEmpty($AWIDMSERVICEACCOUNT)))
{
$node = $xml.properties.property | ? { $_.name -eq "AWIDMSERVICEACCOUNT" }
Write-Output "$node"
$node.value = $AWIDMSERVICEACCOUNT
}
 
if (-not ([string]::IsNullOrEmpty($IS_NET_API_LOGON_USERNAME)))
{
$node = $xml.properties.property | ? { $_.name -eq "IS_NET_API_LOGON_USERNAME" }
Write-Output "$node"
$node.value = $IS_NET_API_LOGON_USERNAME
}
 
if (-not ([string]::IsNullOrEmpty($IS_NET_API_LOGON_PASSWORD)))
{
$node = $xml.properties.property | ? { $_.name -eq "IS_NET_API_LOGON_PASSWORD" }
Write-Output "$node"
$node.value = $IS_NET_API_LOGON_PASSWORD
}
 
Set-Content  $path -Value $xml.InnerXml -Force
$xml.Save($path)  
 
 
$FileName=dir -Path $Installer -Filter *.exe -Recurse | %{$_.FullName}
Write-Output "Installation file : $FileName"
 
Write-Output "Running install command, $installBatFile"
 
Start-Process -FilePath $FileName -ArgumentList "/s /V`"/qn /lie C:\installs\Install.log TARGETDIR=C:\VMware INSTALLDIR=C:\VMware AWSETUPCONFIGFILE=C:\installs\vidm-installer-config.xml” -Wait
 
Write-Output "Install output:"
Get-Content "$installlog"
 
#Write-Output "Exit code is $exitcode and Service Exit code $serviceexitcode"
Write-Output "Install complete."
 
Get-Date
}
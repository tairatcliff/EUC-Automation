# Displays all properties and sub properties of an object in a GUI
# JSON; Home; Properties; Object; GUI
# Requires -version 3
# JSONViewer Download Available At: https://jsonviewer.codeplex.com/
 
Function Show-AllProperties
{
    param
    (
        [Parameter(ValueFromPipeline=$true)] $InputObject,
        [Parameter(ValueFromPipeline=$false)]
        [ValidateRange(1,9)]
        [int]$Depth=4
    )
 
    # This specifies how many layers deep the JSON will go.  It seems that anything more than 5 can result in a signifigant performance penalty
    $JSONViewPath = "c:\bin\jsonview\jsonview.exe"
 
    if(!(Test-Path $JSONViewPath)) { Write-Error "$JSONViewPath is not found.  This is required."; break}
 
    # Need to use reserved PowerShell variable $input to bypass the "one object at a time" default processing of pipelined input
    $InputObject = $input
    $Date = Get-Date
 
    # Set the file path to the temporary file we'll save the exported JSON to so it can be loaded into jsonview.exe
    # We will constantly overwrite/reuse this file since we have no way of safely deleting it without knowing for sure it's not being used by the jsonview.exe
    $TempFilePath = $env:TEMP+"\PSjsonviewtemp.json"
 
    # Create a status bar so if the conversion takes a long time to run, the user has some kind of visual feedback it's still running  
    $Params = @{
            "Activity" = "[$Date] Converting object to JSON.  This process may take several minutes to complete." 
            "Status" = "(Note: Increasing the -depth paramater beyond 4 may result in substantially longer processing times)"
            "Id" = 1
        }
    
    Write-Progress @Params
 
    # Convert the object to JSON.  Need to use the Compress option to get around a bug when processing some Powershell Objects
    try { $JSON = $InputObject | ConvertTo-Json -Compress -Depth $Depth }
    catch { Write-Warning "This object cannot be converted to JSON. Try selecting a sub property and trying again.`n$_"; break }
    Write-Progress "Completed converting the object to JSON..." -id 1 -Completed 
    
    # Write the JSON to the temporary file and then open it with jsonview.exe
    $JSON | Out-File $TempFilePath -Force
 
    # Call the external JSON view application and pass it the file to display
    Start-Process -FilePath $JSONViewPath -ArgumentList $TempFilePath
}
 
# Set a three letter alias to make it faster to run
Set-Alias sap Show-AllProperties
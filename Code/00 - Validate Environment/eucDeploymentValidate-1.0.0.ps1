<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     eucDeploymentValidate.ps1
 Example:      eucDeploymentValidate.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$Global:eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$Global:eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json

# Import configuration details from JSON and validate.
$deploymentSourceDirectory = Get-Item -path $globalConfig.deploymentSourceDirectory
$deploymentDestinationDirectory = If($eucConfig.globalConfig.deploymentDestinationDirectory){$eucConfig.globalConfig.deploymentDestinationDirectory} Else {Write-Host "Deployment Destination Directory not set" -foreground Red}
$horizonServiceAccountUserName = If($eucConfig.globalConfig.horizonServiceAccount.Username){$eucConfig.globalConfig.horizonServiceAccount.Username} Else {Write-Host "Management vCenter Username not set" -foreground Red}
$horizonServiceAccountPassword = If($eucConfig.globalConfig.horizonServiceAccount.Password){$eucConfig.globalConfig.horizonServiceAccount.Password} Else {Write-Host "Management vCenter Password not set" -foreground Red}
$mgmtvCenterServer = If($eucConfig.mgmtvCenterConfig.vCenter){$eucConfig.mgmtConfig.vCenter} Else {Write-Host "Management vCenter Server not set" -foreground Red }
$mgmtDatacenterName = If($eucConfig.mgmtConfig.DatacenterName){$eucConfig.mgmtConfig.DatacenterName} Else {Write-Host "Management datacenter name not set" -foreground Red}


$horizonInstallBinary = If($eucConfig.horizonInstallBinary){$eucConfig.horizonInstallBinary} Else {Write-Host "Horizon install binary location not set" -foreground Red }
If (! Test-Path ($binaryPath = Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinary)) Out-Null){Write-Host "Unable to find the horizon install binary in $binaryPath" -ForegroundColor Red}



ForEach($mgmtNsxConfig in $eucConfig.mgmtNsxConfig){
    If($mgmtNsxConfig.deployNsxLdr){
        Write-host "Checking Management NSX LDR Configuration....." `n -ForegroundColor
        

    }

}


If($eucConfig.deployNSX){
        $NsxManagerServer = If($eucConfig.nsxConfig.nsxManagerServer){$eucConfig.nsxConfig.nsxManagerServer} Else {Write-Host "NSX Manager Server not set" -foreground Red}
        $NsxManagerPassword = If($eucConfig.nsxConfig.nsxManagerPassword){$eucConfig.nsxConfig.nsxManagerPassword} Else {Write-Host "NSX Manager Password not set" -foreground Red}
        $MgmtClusterName = If($eucConfig.horizonConfig.connectionServers.mgmtCluster){$eucConfig.horizonConfig.connectionServers.mgmtCluster} Else {Write-Host "Management vCenter cluster name not set" -foreground Red}
        $MgmtVdsName = If($eucConfig.horizonConfig.connectionServers.mgmtVDS){$eucConfig.horizonConfig.connectionServers.mgmtVDS} Else {Write-Host "Management VDS name not set" -foreground Red}
        $mgmtEdgeClusterName = If($eucConfig.nsxConfig.mgmtEdgeClusterName){$eucConfig.nsxConfig.mgmtEdgeClusterName} Else {Write-Host "Management edge cluster name not set" -foreground Red}
        $mgmtEdgeDatastoreName = If($eucConfig.nsxConfig.mgmtEdgeDatastoreName){$eucConfig.nsxConfig.mgmtEdgeDatastoreName} Else {Write-Host "Management edge datastore name not set" -foreground Red}
        $mgmtNsxUplinkPortGroup01Name = If($eucConfig.nsxConfig.mgmtNsxUplinkPortGroup01Name){$eucConfig.nsxConfig.mgmtNsxUplinkPortGroup01Name} Else {Write-Host "Management NSX uplink 01 port group name not set" -foreground Red}
        $mgmtNsxUplinkPortGroup02Name = If($eucConfig.nsxConfig.mgmtNsxUplinkPortGroup02Name){$eucConfig.nsxConfig.mgmtNsxUplinkPortGroup02Name} Else {Write-Host "Management NSX uplink 02 port group name not set" -foreground Red}
        $mgmtNsxFolderName = If($eucConfig.nsxConfig.mgmtNsxFolderName){$eucConfig.nsxConfig.mgmtNsxFolderName} Else {Write-Host "Management NSX folder name not set" -foreground Red}
        $mgmtDatacenterName = If($eucConfig.mgmtConfig.mgmtDatacenterName){$eucConfig.mgmtConfig.mgmtDatacenterName} Else {Write-Host "Management datacenter name not set" -foreground Red}
        $tor01UplinkProtocolAddress = If($eucConfig.nsxConfig.mgmtTor01UplinkProtocolAddress){$eucConfig.nsxConfig.mgmtTor01UplinkProtocolAddress} Else {Write-Host "Management Top of Rack uplink 01 address not set" -foreground Red}
        $tor02UplinkProtocolAddress = If($eucConfig.nsxConfig.mgmtTor02UplinkProtocolAddress){$eucConfig.nsxConfig.mgmtTor02UplinkProtocolAddress} Else {Write-Host "Management Top of Rack uplink 02 address not set" -foreground Red}
        $Edge01Uplink01PrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge01Uplink01PrimaryAddress){$eucConfig.nsxConfig.mgmtEdge01Uplink01PrimaryAddress} Else {Write-Host "Management edge 01 uplink 01 address not set" -foreground Red}
        $Edge01Uplink02PrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge01Uplink02PrimaryAddress){$eucConfig.nsxConfig.mgmtEdge01Uplink02PrimaryAddress} Else {Write-Host "Management edge 01 uplink 02 address not set" -foreground Red}
        $Edge02Uplink01PrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge02Uplink01PrimaryAddress){$eucConfig.nsxConfig.mgmtEdge02Uplink01PrimaryAddress} Else {Write-Host "Management edge 02 uplink 01 address not set" -foreground Red}
        $Edge02Uplink02PrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge02Uplink02PrimaryAddress){$eucConfig.nsxConfig.mgmtEdge02Uplink02PrimaryAddress} else {Write-Host "Management edge 02 uplink 02 address not set" -foreground Red}
        $bgpPassword = $eucConfig.nsxConfig.bgpPassword
        $uplinkASN01 = If($eucConfig.nsxConfig.mgmtUplinkASN01){$eucConfig.nsxConfig.mgmtUplinkASN01} Else {Write-Host "Management uplink ASN 01 not set" -foreground Red}
        $uplinkASN02 = If($eucConfig.nsxConfig.mgmtUplinkASN02){$eucConfig.nsxConfig.mgmtUplinkASN02} Else {Write-Host "Management uplink ASN 02 not set" -foreground Red}
        $LocalASN = If($eucConfig.nsxConfig.mgmtLocalASN){$eucConfig.nsxConfig.mgmtLocalASN} Else {Write-Host "Management NSX local ASN not set" -foreground Red}
        $AppliancePassword = If($eucConfig.nsxConfig.mgmtEdgePassword){$eucConfig.nsxConfig.mgmtEdgePassword} Else {Write-Host "Management NSX edge applicance password not set" -foreground Red}
        $mgmtTransitLsName = If($eucConfig.nsxConfig.mgmtTransitLsName){$eucConfig.nsxConfig.mgmtTransitLsName} Else {Write-Host "Management NSX transit logical switch name not set" -foreground Red}
        $mgmtEdgeHAPortGroupName = If($eucConfig.nsxConfig.mgmtEdgeHAPortGroupName){$eucConfig.nsxConfig.mgmtEdgeHAPortGroupName} Else {Write-Host "Management edge HA port group not set" -foreground Red}
        $mgmtEUC_MGMT_Network = If($eucConfig.nsxConfig.mgmtEUC_MGMT_Network){$eucConfig.nsxConfig.mgmtEUC_MGMT_Network} Else {Write-Host "EUC management network network name not set" -foreground Red}
        $mgmtEdge01Name = If($eucConfig.nsxConfig.mgmtEdge01Name){$eucConfig.nsxConfig.mgmtEdge01Name} Else {Write-Host "Management edge 01 name not set" -foreground Red}
        $mgmtEdge02Name = If($eucConfig.nsxConfig.mgmtEdge02Name){$eucConfig.nsxConfig.mgmtEdge02Name} Else {Write-Host "Management edge 02 name not set" -foreground Red}
        $mgmtLdrName = If($eucConfig.nsxConfig.mgmtLdrName){$eucConfig.nsxConfig.mgmtLdrName} Else {Write-Host "Management distributed logical router name not set" -foreground Red}
        $mgmtEUCTransportZoneName = If($eucConfig.nsxConfig.mgmtEUCTransportZoneName){$eucConfig.nsxConfig.mgmtEUCTransportZoneName} Else {Write-Host "Management EUC transport zone name not set" -foreground Red}
        $mgmtEdge01InternalPrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge01InternalPrimaryAddress){$eucConfig.nsxConfig.mgmtEdge01InternalPrimaryAddress} Else {Write-Host "Management edge 01 internal primary IP address not set" -foreground Red}
        $mgmtEdge02InternalPrimaryAddress = If($eucConfig.nsxConfig.mgmtEdge02InternalPrimaryAddress){$eucConfig.nsxConfig.mgmtEdge02InternalPrimaryAddress} Else {Write-Host "Management edge 02 internal primary IP address not set" -foreground Red}
        $mgmtLdrUplinkPrimaryAddress = If($eucConfig.nsxConfig.mgmtLdrUplinkPrimaryAddress){$eucConfig.nsxConfig.mgmtLdrUplinkPrimaryAddress} Else {Write-Host "Management distributed logical router uplink primary IP address not set" -foreground Red}
        $mgmtLdrUplinkProtocolAddress = If($eucConfig.nsxConfig.mgmtLdrUplinkProtocolAddress){$eucConfig.nsxConfig.mgmtLdrUplinkProtocolAddress} Else {Write-Host "Management distributed logical router uplink protocol IP address not set" -foreground Red}
        $mgmtLdrEUCMGMTPrimaryAddress = If($eucConfig.nsxConfig.mgmtLdrEUCMGMTPrimaryAddress){$eucConfig.nsxConfig.mgmtLdrEUCMGMTPrimaryAddress} Else {Write-Host "EUC management distributed locical router primary IP address not set" -foreground Red}
        $mgmtDefaultSubnetBits = If($eucConfig.nsxConfig.mgmtDefaultSubnetBits){$eucConfig.nsxConfig.mgmtDefaultSubnetBits} Else {Write-Host "Default management subnet not set" -foreground Red}
        $mgmtDlrHaDatastoreName = If($eucConfig.nsxConfig.mgmtDlrHaDatastoreName){$eucConfig.nsxConfig.mgmtDlrHaDatastoreName} Else {Write-Host "Management distributed logical router HA datastore name not set" -foreground Red}
        $csServers = If($eucConfig.horizonConfig.ConnectionServers.horizonCS){$eucConfig.horizonConfig.ConnectionServers.horizonCS} Else {Write-Host "Connection Servers are not set" -foreground Red}
        Write-Host "`nFinished validating the NSX logical networking configuration `n" -foreground Green

    ## Only validate these settings if deploying NSX Load Balancers
    If($eucConfig.nsxConfig.deployLoadBalancer){
        $horizonLbEdgeName = $eucConfig.nsxConfig.horizonhorizonLbEdgeName
        $horizonLbPrimaryIPAddress = $eucConfig.nsxConfig.horizonhorizonLbPrimaryIPAddress
        $horizonVipIp = $eucConfig.nsxConfig.horizonVipIp
        $horizonLbAlgorith = $eucConfig.nsxConfig.horizonhorizonLbAlgorithrith
        $horizonLbPoolName = $eucConfig.nsxConfig.horizonLbPoolName
        $horizonVipName = $eucConfig.nsxConfig.horizonVipName
        $horizonAppProfileName = $eucConfig.nsxConfig.horizonAppProfileName
        $horizonVipProtocol = $eucConfig.nsxConfig.horizonhorizonVipProtocol
        $horizonHttpsPort = $eucConfig.nsxConfig.horizonhorizonHttpsPort
        $horizonLBMonitorName = $eucConfig.nsxConfig.horizonhorizonLBMonitorName
        Write-Host "`nFinished validating the NSX load balancer configuration `n" -foreground Green
    }

    ## Only validate these settings if configuring NSX micro segmentation
    If($eucConfig.nsxConfig.deployMicroSeg){
        $horizonSgName = $eucConfig.nsxConfig.horizonSgName
        $horizonSgDescription = $eucConfig.nsxConfig.horizonSgDescription
        $horizonStName = $eucConfig.nsxConfig.horizonStName
        $horizonVIP_IpSet_Name = $eucConfig.nsxConfig.horizonVIP_IpSet_Name
        $horizonInternalESG_IpSet_Name = $eucConfig.nsxConfig.horizonhorizonInternalESG_IpSet_Name
        $horizonFirewallSectionName = $eucConfig.nsxConfig.horizonhorizonFirewallSectionName
        Write-Host "`nFinished validating the NSX micro segmentation configuration `n" -foreground Green
    }
    Write-Host "`nFinished validating the NSX configuration `n" -foreground Green
}

If($eucConfig.cloneHorizonVMs){
    $horizonServiceAccount = If($eucConfig.horizonServiceAccount.Username){$eucConfig.horizonServiceAccount.Username} Else { Write-Host -foreground Red "Horizon service account username not set"}
    $horizonServiceAccountPassword = If($eucConfig.horizonServiceAccount.Password){$eucConfig.horizonServiceAccount.Password} Else {Write-Host -foreground Red "Horizon service account password not set"}

    $csServerArray = If($eucConfig.horizonConfig.connectionServers.horizonCS){$eucConfig.horizonConfig.connectionServers.horizonCS} Else {Write-Host -foreground Red "Connection Servers are not set"}

    $folderName = If($eucConfig.horizonConfig.connectionServers.mgmtFolder){$eucConfig.horizonConfig.connectionServers.mgmtFolder} Else {Write-Host -foreground Red "EUC management folder not set"}
    $datacenterName = If($eucConfig.horizonConfig.connectionServers.mgmtDatacenterName){$eucConfig.horizonConfig.connectionServers.mgmtDatacenterName} Else {Write-Host -foreground Red "EUC management datacenter name not set"}

    $subnetMask = If($eucConfig.horizonConfig.connectionServers.subnetMask){$eucConfig.horizonConfig.connectionServers.subnetMask} Else {Write-Host -foreground Red "Horizon Connection Server subnet mast not set"}
    $gateway = If($eucConfig.horizonConfig.connectionServers.gateway){$eucConfig.horizonConfig.connectionServers.gateway} Else {Write-Host -foreground Red "Horizon Connection Server gateway not set"}
    $dnsServer = If($eucConfig.horizonConfig.connectionServers.dnsServerIP){$eucConfig.horizonConfig.connectionServers.dnsServerIP} Else {Write-Host -foreground Red "Horizon Connection Server DNS not set"}
    $orgName = If($eucConfig.horizonConfig.connectionServers.orgName){$eucConfig.horizonConfig.connectionServers.orgName} Else {Write-Host -foreground Red "Horizon Connection Server guest optimization organization name not set"}
    $domainName = If($eucConfig.horizonConfig.connectionServers.domainName){$eucConfig.horizonConfig.connectionServers.domainName} Else {Write-Host -foreground Red "Horizon Connection Server guest optimization domain name not set"}
    $timeZone = If($eucConfig.horizonConfig.connectionServers.timeZone){$eucConfig.horizonConfig.connectionServers.timeZone } Else {Write-Host -foreground Red "Horizon Connection Server guest optimization time zone not set"}
    $domainJoinUser = If($eucConfig.horizonConfig.connectionServers.domainJoinUser){$eucConfig.horizonConfig.connectionServers.domainJoinUser} Else {Write-Host -foreground Red "Horizon Connection Server guest optimization domain join user not set"}
    $domainJoinPass = If($eucConfig.horizonConfig.connectionServers.domainJoinPass){$eucConfig.horizonConfig.connectionServers.domainJoinPass} Else {Write-Host -foreground Red "Horizon Connection Server guest opimization domain join user password not set"}
    $productKey = If($eucConfig.horizonConfig.connectionServers.windowsLicenceKey){$eucConfig.horizonConfig.connectionServers.windowsLicenceKey} Else {Write-Host -foreground Red "Horizon Connection Server guest optimization windows product key not set"}
    $mgmtDatastore = If($eucConfig.horizonConfig.connectionServers.mgmtDatastore){$eucConfig.horizonConfig.connectionServers.mgmtDatastore} Else {Write-Host -foreground Red "Management datastore not set"}
    $hznReferenceVM = If($eucConfig.horizonConfig.connectionServers.hznReferenceVM){$eucConfig.horizonConfig.connectionServers.hznReferenceVM} Else {Write-Host -foreground Red "Horizon reference VM template not set"}
    $diskFormat = If($eucConfig.horizonConfig.connectionServers.diskFormat){$eucConfig.horizonConfig.connectionServers.diskFormat} Else {Write-Host -foreground Red "Horizon Connection Server disk format not set"}
    $mgmtCluster = If($eucConfig.horizonConfig.connectionServers.mgmtCluster){$eucConfig.horizonConfig.connectionServers.mgmtCluster} Else {Write-Host -foreground Red "Management cluster not set"}
    $mgmtPortGroup = If($eucConfig.horizonConfig.connectionServers.mgmtPortGroup){$eucConfig.horizonConfig.connectionServers.mgmtPortGroup} Else {Write-Host -foreground Red "Horizon Connection Server port group not set"}
    $affinityRuleName = If($eucConfig.horizonConfig.connectionServers.affinityRuleName){$eucConfig.horizonConfig.connectionServers.affinityRuleName} Else {Write-Host -foreground Red "Horizon Connection Server anti-affinity rule name is not set"}
    If($eucConfig.horizonConfig.certificateConfig.requestCASignedCertificate){
        $deploymentSourceDirectory = Get-Item -path $eucConfig.deploymentSourceDirectory
        $deploymentDestinationDirectory = If($eucConfig.deploymentDestinationDirectory){$eucConfig.deploymentDestinationDirectory} Else {Write-Host -foreground Red "EUC deployment desitation directory not set"}
        $requestCASignedCertificate = If($eucConfig.horizonConfig.certificateConfig.requestCASignedCertificate){$eucConfig.horizonConfig.certificateConfig.requestCASignedCertificate} Else {Write-Host -foreground Red "The option to request CA Signed Certificates is not set to either 'true' or 'false'"}
        $caName = If($eucConfig.horizonConfig.certificateConfig.caName){$eucConfig.horizonConfig.certificateConfig.caName} Else {Write-Host -foreground Red "CA certificate authority name not set"}
        $country = If($eucConfig.horizonConfig.certificateConfig.country){$eucConfig.horizonConfig.certificateConfig.country} Else {Write-Host -foreground Red "CA certificate country not set"}
        $state = If($eucConfig.horizonConfig.certificateConfig.state){$eucConfig.horizonConfig.certificateConfig.state} Else {Write-Host -foreground Red "CA certificate state not set"}
        $city = If($eucConfig.horizonConfig.certificateConfig.city){$eucConfig.horizonConfig.certificateConfig.city} Else {Write-Host -foreground Red "CA certificate city not set"}
        $organisation = If($eucConfig.horizonConfig.certificateConfig.organisation){$eucConfig.horizonConfig.certificateConfig.organisation} Else {Write-Host -foreground Red "CA certificate organisation not set"}
        $organisationOU = If($eucConfig.horizonConfig.certificateConfig.organisationOU){$eucConfig.horizonConfig.certificateConfig.organisationOU} Else {Write-Host -foreground Red "CA certificate organisation OU not set"}
        $templateName = If($eucConfig.horizonConfig.certificateConfig.templateName){$eucConfig.horizonConfig.certificateConfig.templateName} Else {Write-Host -foreground Red "CA certificate template name not set"}
        $friendlyName = If($eucConfig.horizonConfig.certificateConfig.friendlyName -eq "vdm"){$eucConfig.horizonConfig.certificateConfig.friendlyName} Else {Write-Host -foreground Red "CA certificate friendly name not set to 'vdm'"}
        $commonName = If($eucConfig.horizonConfig.certificateConfig.commonName){$eucConfig.horizonConfig.certificateConfig.commonName} Else {Write-Host -foreground Red "CA certificate common name not set"}
    }
    Write-Host "`nFinished validating the configuration to clone Horizon VMs `n" -foreground Green
}

If($eucConfig.installConnectionServers){
    $mgmtvCenterServer = If($eucConfig.mgmtConfig.mgmtvCenter){$eucConfig.mgmtConfig.mgmtvCenter} Else {Write-Host -foreground Red "Management vCenter Server not set"}
    $horizonServiceAccount = If($eucConfig.horizonServiceAccount.Username){$eucConfig.horizonServiceAccount.Username} Else { Write-Host -foreground Red "Horizon service account username not set"}
    $horizonServiceAccountPassword = If($eucConfig.horizonServiceAccount.Password){$eucConfig.horizonServiceAccount.Password} Else {Write-Host -foreground Red "Horizon service account password not set"}
        #{$horizonBinary = Get-Content -Path $horizonInstallBinary} Else {Write-Host -foreground Red "$horizonInstallBinary does not exist or can not be found"}
    $deploymentDestinationDirectory = If($eucConfig.deploymentDestinationDirectory){$eucConfig.deploymentDestinationDirectory} Else {Write-Host -foreground Red "EUC deployment destination directory not set"}
    $hznAdminSID = If($eucConfig.horizonServiceAccount.horizonLocalAdminSID){$eucConfig.horizonServiceAccount.horizonLocalAdminSID} Else {Write-Host -foreground Red "Horizon local administrator SID not set"}
    $hznRecoveryPassword = If($eucConfig.horizonConfig.connectionServers.horizonRecoveryPassword){$eucConfig.horizonConfig.connectionServers.horizonRecoveryPassword} Else {Write-Host -foreground Red "Horizon recovery password not set"}
    $hznRecoveryPasswordHint = If($eucConfig.horizonConfig.connectionServers.horizonRecoveryPasswordHint){$eucConfig.horizonConfig.connectionServers.horizonRecoveryPasswordHint} Else {Write-Host -foreground Red "Horizon recovery password hint not set"}
    $horizonDestinationBinary = "$deploymentDestinationDirectory\$($horizonBinary.name)" -replace "(?!^\\)\\{2,}","\"
    $csServers = If($eucConfig.horizonConfig.connectionServers.horizonCS){$eucConfig.horizonConfig.connectionServers.horizonCS} Else {Write-Host -foreground Red "Connection Servers are not set"}
    $horizonConnectionServerURL = If($eucConfig.horizonConfig.connectionServers.horizonConnectionServerURL){$eucConfig.horizonConfig.connectionServers.horizonConnectionServerURL} Else {throw "Horizon connection server global URL not set"}
    $domainName = $eucConfig.horizonConfig.connectionServers.domainName
    $horizonLiceseKey = If($eucConfig.horizonConfig.connectionServers.horizonLicensekey){$eucConfig.horizonConfig.connectionServers.horizonLicensekey} Else {Write-Host "Horizon license key not set. This will apply a trail license" -ForegroundColor Red}
    $blockvCenters = If($eucConfig.horizonConfig.blockvcenters.vcName){$eucConfig.horizonConfig.blockvcenters.vcName} Else {Write-Host -foreground Red "Horizon block vCenter servers are not been set"}

    if($eucConfig.horizonConfig.connectionServers.eventDB.configureEventDB){
        $eventDbServer = If($eucConfig.horizonConfig.connectionServers.eventDB.servername){$eucConfig.horizonConfig.connectionServers.eventDB.servername} Else {Write-Host -foreground Red "Event DB server is not set"}
        $eventDbName = If($eucConfig.horizonConfig.connectionServers.eventDB.databasename){$eucConfig.horizonConfig.connectionServers.eventDB.databasename} Else {Write-Host -foreground Red "Event DB database name is not set"}
        $eventDbUser = If($eucConfig.horizonConfig.connectionServers.eventDB.eventDbUser){$eucConfig.horizonConfig.connectionServers.eventDB.eventDbUser} Else {Write-Host -foreground Red "Event DB username is not set"}
        $eventDbPassword= If($eucConfig.horizonConfig.connectionServers.eventDB.eventDbPassword){$eucConfig.horizonConfig.connectionServers.eventDB.eventDbPassword} Else {Write-Host -foreground Red "Event DB user password is not set"}
        $eventDbType =  If($eucConfig.horizonConfig.connectionServers.eventDB.eventDbType){$eucConfig.horizonConfig.connectionServers.eventDB.eventDbType} Else {Write-Host -foreground Red "Event DB type is not set"}
        $eventDbTablePrefix=  If($eucConfig.horizonConfig.connectionServers.eventDB.eventDbTablePrefix){$eucConfig.horizonConfig.connectionServers.eventDB.eventDbTablePrefix} Else {Write-Host -foreground Red "Event DB table prefix is not set"}
        $eventDbPort = If([int]$eucConfig.horizonConfig.connectionServers.eventDB.eventDbPort){[int]$eucConfig.horizonConfig.connectionServers.eventDB.eventDbPort} Else {Write-Host -foreground Red "Event DB port is not set"}
        $classifyEventsAsNewForDays = If([int]$eucConfig.horizonConfig.connectionServers.eventDB.classifyEventsAsNewForDays){[int]$eucConfig.horizonConfig.connectionServers.eventDB.classifyEventsAsNewForDays} Else {Write-Host -foreground Red "Event DB classify events as new for days is not set"}
        $showEventsForTime = If($eucConfig.horizonConfig.connectionServers.eventDB.showEventsForTime){$eucConfig.horizonConfig.connectionServers.eventDB.showEventsForTime} Else {Write-Host -foreground Red "Event DB show events for time is not set"}
    }
    If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.enabled){
        $syslogUNCPath = If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncPath){$eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncPath} Else {Write-Host -foreground Red "Syslog UNC path is not set"}
        $syslogUNCUserName = If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncUserName){$eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncUserName} Else {Write-Host -foreground Red "Syslog UNC username is not set"}
        $sysloguncPassword = If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncPassword){$eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncPassword} Else {Write-Host -foreground Red "Syslog UNC password is not set"}
        $sysloguncDomain = If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncDomain){$eucConfig.horizonConfig.connectionServers.syslogserver.SyslogFileData.uncDomain} Else {Write-Host -foreground Red "Syslog UNC domain is not set"}
    }
    If($eucConfig.horizonConfig.connectionServers.syslogserver.SyslogUDPData.enabled){
        $syslogUDPNetworks = $eucConfig.horizonConfig.connectionServers.syslogserver.SyslogUDPData.networkAddresses
    }
    If($eucConfig.horizonConfig.InstantCloneDomainAdministrator.useInstantClones){
        $icadminuser = If($eucConfig.horizonConfig.InstantCloneDomainAdministrator.userName){$eucConfig.horizonConfig.InstantCloneDomainAdministrator.userName} Else {Write-Host -foreground Red "Instant Clone domain admin username is not set"}
        $icadminpw = If($eucConfig.horizonConfig.InstantCloneDomainAdministrator.password){$eucConfig.horizonConfig.InstantCloneDomainAdministrator.password} Else {Write-Host -foreground Red "Instant CLone domain admin password is not set"}
        $icadmindomain = If($eucConfig.horizonConfig.InstantCloneDomainAdministrator.domain){$eucConfig.horizonConfig.InstantCloneDomainAdministrator.domain} Else {Write-Host -foreground Red "Instant Clone domain is not set"}
    }
    Write-Host "`nFinished validating the configuration Horizon Connection Server install `n" -foreground Green
}

If($eucConfig.buildNSXDesktopNetworks){
    $DesktopNsxManagerServer = If($eucConfig.nsxConfig.desktopNsxManagerServer){$eucConfig.nsxConfig.desktopNsxManagerServer} Else {Write-Host -foreground Red "Desktop NSX Manager is not set"}
    $DesktopNsxAdminPassword = If($eucConfig.nsxConfig.desktopNsxAdminPassword){$eucConfig.nsxConfig.desktopNsxAdminPassword} Else {Write-Host -foreground Red "Desktop NSX Manager password is not set"}
    $DesktopvCenterUserName = If($eucConfig.nsxConfig.desktopvCenterUserName){$eucConfig.nsxConfig.desktopvCenterUserName} Else {Write-Host -foreground Red "Desktop vCenter username is not set"}
    $DesktopvCenterPassword = If($eucConfig.nsxConfig.desktopvCenterPassword){$eucConfig.nsxConfig.desktopvCenterPassword} Else {Write-Host -foreground Red "Desktop vCenter password is not set"}
    $DesktopLsName = If($eucConfig.nsxConfig.desktopLsName){$eucConfig.nsxConfig.desktopLsName} Else {Write-Host -foreground Red "Desktop logical switch name is not set"}
    $RDSLsName = If($eucConfig.nsxConfig.RdsLsName){$eucConfig.nsxConfig.RdsLsName} Else {Write-Host -foreground Red "RDS logical switch name is not set"}
    $DesktopLdrName = If($eucConfig.nsxConfig.desktopLdrName){$eucConfig.nsxConfig.desktopLdrName} Else {Write-Host -foreground Red "Desktop logical router name is not set"}
    $DesktopTransportZoneName = If($eucConfig.nsxConfig.desktopTransportZoneName){$eucConfig.nsxConfig.desktopTransportZoneName} Else {Write-Host -foreground Red "Desktop transport zone name is not set"}
    $dnsServer = If($eucConfig.horizonConfig.connectionServers.dnsServerIP){$eucConfig.horizonConfig.connectionServers.dnsServerIP} Else {Write-Host -foreground Red "DNS server not set"}
    $DesktopNetwork = If($eucConfig.nsxConfig.desktopNetwork){$eucConfig.nsxConfig.desktopNetwork} Else {Write-Host -foreground Red "Desktop network is not set"}
    $DesktopNetworkPrimaryAddress = If($eucConfig.nsxConfig.desktopNetworkPrimaryAddress){$eucConfig.nsxConfig.desktopNetworkPrimaryAddress} Else {Write-Host -foreground Red "Desktop network primary address is not set"}
    $desktopSubnetMask = If($eucConfig.nsxConfig.desktopSubnetMask){$eucConfig.nsxConfig.desktopSubnetMask} Else {Write-Host -foreground Red "Desktop subnet mask name is not set"}
    $RdsNetwork = If($eucConfig.nsxConfig.desktopRdsNetwork){$eucConfig.nsxConfig.desktopRdsNetwork} Else {Write-Host -foreground Red "RDS network is not set"}
    $RdsNetworkPrimaryAddress = If($eucConfig.nsxConfig.desktopRdsNetworkPrimaryAddress){$eucConfig.nsxConfig.desktopRdsNetworkPrimaryAddress} Else {Write-Host -foreground Red "RDS network primary address is not set"}
    $RdsSubnetMask = If($eucConfig.nsxConfig.desktopRdsSubnetMask){$eucConfig.nsxConfig.desktopRdsSubnetMask} Else {Write-Host -foreground Red "RDS subnet mask is not set"}
    $desktopEdge01Name = If($eucConfig.nsxConfig.desktopEdge01Name){$eucConfig.nsxConfig.desktopEdge01Name} Else {Write-Host -foreground Red "Desktop edge name is not set"}
    $desktopEdge01TransitIP = If($eucConfig.nsxConfig.desktopEdge01TransitIP){$eucConfig.nsxConfig.desktopEdge01TransitIP} Else {Write-Host -foreground Red "Desktop edge transip IP is not set"}
    If(! $eucConfig.nsxConfig.useEdgeDHCPServer){
        $dhcpServerAddress = If($eucConfig.nsxConfig.desktopDhcpServerAddress){$eucConfig.nsxConfig.desktopDhcpServerAddress} Else {Write-Host -foreground Red "DHCP server is not set"}
    }
    If($eucConfig.nsxConfig.deployMicroSeg){
        ## Security Groups
        $desktopSgName = If($eucConfig.nsxConfig.desktopSgName){$eucConfig.nsxConfig.desktopSgName} Else {Write-Host -foreground Red "Desktop security group is not set"}
        $desktopSgDescription = If($eucConfig.nsxConfig.desktopSgDescription){$eucConfig.nsxConfig.desktopSgDescription} Else {Write-Host -foreground Red "Desktop security group description is not set"}
        ## Security Tags
        $desktopStName = If($eucConfig.nsxConfig.desktopStName){$eucConfig.nsxConfig.desktopStName} Else {Write-Host -foreground Red "Desktop security tag is not set"}
        ##DFW
        $desktopFirewallSectionName = If($eucConfig.nsxConfig.desktopFirewallSectionName){$eucConfig.nsxConfig.desktopFirewallSectionName} Else {Write-Host -foreground Red "Desktop firewall section name is not set"}
    }
    Write-Host "`nFinished validating the configuration to build NSX desktop networks `n" -foreground Green
}

If($eucConfig.buildDesktopPools){
    foreach ($newPool in $eucConfig.horizonConfig.pool.desktopPool) {
        $poolType = If([string]$newPool.PoolType.toupper()){[string]$newPool.PoolType.toupper()} Else {Write-Host -foreground Red "Pool type not set"}
        $folderName = If($newPool.VmFolder){$newPool.VmFolder} Else {Write-Host -foreground Red "Pool folder name not set"}
        $masterVM = If($newPool.ParentVM){$newPool.ParentVM} Else {Write-Host -foreground Red "Pool master VM not set"}
        Write-Host "Validating a new $poolType pool named $($newPool.PoolName)"
        If($poolType -eq "INSTANTCLONE"){
            $PoolName = $newPool.PoolName  
            $PoolDisplayName = $newPool.PoolDisplayName  
            $Description = $newPool.Description  
            $UserAssignment = $newPool.UserAssignment  
            $ParentVM = $newPool.ParentVM  
            $SnapshotVM = $newPool.SnapshotVM  
            $VmFolder = $newPool.VmFolder  
            $HostOrCluster = $newPool.HostOrCluster  
            $ResourcePool = $newPool.ResourcePool  
            $NamingMethod = $newPool.NamingMethod  
            $Datastores = $newPool.Datastores   
            $NamingPattern = $newPool.NamingPattern  
            $NetBiosName = $newPool.NetBiosName  
            $DomainAdmin = $newPool.DomainAdmin  
            $vCenter = $newPool.vCenter  
            $MinimumCount = $newPool.MinimumCount  
            $MaximumCount = $newPool.MaximumCount 
        }
        If($poolType -eq "FULLCLONE"){ 
            #runlog -functionIn $MyInvocation.MyCommand -runMessage $message
            $PoolName = $newPool.PoolName   
            $PoolDisplayName = $newPool.PoolDisplayName   
            $Description = $newPool.Description  
            $UserAssignment = $newPool.UserAssignment  
            $VmFolder = $newPool.VmFolder  
            $HostOrCluster = $newPool.HostOrCluster  
            $ResourcePool = $newPool.ResourcePool  
            $NamingMethod = $newPool.NamingMethod  
            $Datastores = $newPool.Datastores   
            $NamingPattern = $newPool.NamingPattern  
            $NetBiosName = $newPool.NetBiosName  
            $vCenter = $newPool.vCenter   
            $Template = $newPool.Template 
            $SysPrepName = $newPool.SysPrepName  
            $CustType = $newPool.CustType
        }
    }	
    Write-Host "`nFinished validating the configuration of the desktop pools `n" -foreground Green
}


﻿<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     installConnectionServers.ps1
 Example:      installConnectionServers.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$error.Clear()

$globalConfig = $eucConfig.globalConfig
$mgmtNsxConfig = $eucConfig.mgmtNsxConfig
$horizonConfig = $eucConfig.horizonConfig
$connectionServerConfig = $horizonConfig.connectionServerConfig
$blockvCenters = $horizonConfig.blockvcenters
$certificateConfig = $horizonConfig.certificateConfig
$mgmtvCenterConfig = $eucConfig.mgmtvCenterConfig


$mgmtvCenterServer = If($mgmtvCenterConfig.vCenter){$mgmtvCenterConfig.vCenter} Else {throw "Management vCenter Server not set"}
$horizonServiceAccount = If($globalConfig.horizonServiceAccount.Username){$globalConfig.horizonServiceAccount.Username} Else { Write-Error "Horizon service account username not set"}
$horizonServiceAccountPassword = If($globalConfig.horizonServiceAccount.Password){$globalConfig.horizonServiceAccount.Password} Else {Write-Error "Horizon service account password not set"}
$csServers = If($connectionServerConfig.ConnectionServers){$connectionServerConfig.ConnectionServers} Else {throw "Connection Servers are not set"}

function WaitFor-VMTools{
	param(
		[string]$vmName
	)

	Write-Host "Waiting for VMTools to start on $vmName" -ForegroundColor Yellow
	$i = 0
	While($toolsStatus -ne "guesttoolsrunning" -and $i -lt (5*6)){
		$toolsStatus = (get-vm $vmName).ExtensionData.Guest.ToolsRunningStatus.ToLower()
		$i++
		Start-Sleep 10
	}
	If($toolsStatus -eq "guesttoolsrunning"){
		Write-Host "VMTools is now confirmed running on $vmName" `n -ForegroundColor Green
		return $true
	} Else {
		return $false
	}
}


# Connect to Management vCenter Server
if($global:defaultVIServers.Name -notcontains $mgmtvCenterServer){
	Connect-VIServer -Server $mgmtvCenterServer -User $horizonServiceAccount -Password $horizonServiceAccountPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterServer){
	Write-Host "Successfully connected to $mgmtvCenterServer" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to vCenter Server"
}

for($i = 0; $i -lt $csServers.count; $i++){ 
	#Skip null or empty properties.
	If ([string]::IsNullOrEmpty($csServers[$i].Name)){Continue}
	
	$csName = $csServers[$i].Name

	# Revert all VMs in array to snapshot
	Write-Host "Rolling back $csName to the snapshot 'Installing Horizon Connection Server'" `n -ForegroundColor White -backgroundColor Red
	$VmSnapshot = get-snapshot -VM $csName -Name "Installing Horizon Connection Server"
	Set-VM -VM $csName -Snapshot $VmSnapshot -Confirm:$false | Out-Null

	# Power the VM back on
	Start-VM $csName | Out-Null
}

for($i = 0; $i -lt $csServers.count; $i++){ 
	#Skip null or empty properties.
	If ([string]::IsNullOrEmpty($csServers[$i].Name)){Continue}
	$csName = $csServers[$i].Name
	WaitFor-VMTools -vmName $csName | Out-Null
}

Disconnect-VIServer * -Force -Confirm:$false
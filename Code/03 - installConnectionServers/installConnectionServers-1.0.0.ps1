﻿<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     installConnectionServers.ps1
 Example:      installConnectionServers.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>
If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}
$error.Clear()


# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$appRootDirectory = $deploymentDirectories.appRootDirectory
$deploymentSourceDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$certificateDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.certificateDirectory
$toolsDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.toolsDirectory
$codeDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.codeDirectory
$configFilesDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.configFilesDirectory
$destinationConfigFilesDirectory = Join-Path -Path $deploymentDestinationDirectory -ChildPath $deploymentDirectories.configFilesDirectory
# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
# Service Accounts
$airwatchServiceAccountName = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$horizonServiceAccountName = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$localDomainAdminUser = $serviceAccounts.localDomainAdmin.username
$localDomainAdminPassword = $serviceAccounts.localDomainAdmin.password
$domainJoinUser = $serviceAccounts.domainJoin.username
$domainJoinPassword = $serviceAccounts.domainJoin.password

# Import arrays of VMs to deploy
$installCn = $airwatchConfig.airwatchServers.CN.installCn
$installDs = $airwatchConfig.airwatchServers.DS.installDs
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers
$blockvCenters = $horizonConfig.blockvCenters

$horizonInstallBinaryName = $binaries.horizonBinary
$horizonInstallBinary = Get-Item -Path (Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinaryName)
$hznRecoveryPassword = $horizonConfig.connectionServerConfig.horizonRecoveryPassword
$hznRecoveryPasswordHint = $horizonConfig.connectionServerConfig.horizonRecoveryPasswordHint
$horizonDestinationBinary = Join-Path -Path $deploymentDestinationDirectory -ChildPath $horizonInstallBinaryName
$domainName = $domainConfig.domainName
$netbios = $domainConfig.netbios
$horizonConnectionServerURL = $horizonConfig.connectionServerConfig.horizonConnectionServerURL
$horizonLicenseKey = $licenseKeys.horizon

$eventDbConfig = $horizonConfig.connectionServerConfig.eventDB
if($eventDbConfig.configureEventDB){
	$eventDbServer = $eventDbConfig.servername
	$eventDbName = $eventDbConfig.databasename
	$eventDbUser = $eventDbConfig.eventDbUser
	$eventDbPassword= $eventDbConfig.eventDbPassword
	$eventDbType=  $eventDbConfig.eventDbType
	$eventDbTablePrefix= $eventDbConfig.eventDbTablePrefix
	$eventDbPort = [int]$eventDbConfig.eventDBPort
	$classifyEventsAsNewForDays = [int]$eventDbConfig.classifyEventsAsNewForDays
	$showEventsForTime = $eventDbConfig.showEventsForTime
}

$syslogConfig = $horizonConfig.connectionServerConfig.syslogserver
$sysloguncEnabled = $syslogConfig.SyslogFileData.enabled
$syslogUDPenabled = $syslogConfig.SyslogUDPData.enabled
If($sysloguncEnabled){
	$sysloguncPath = $syslogConfig.SyslogFileData.path
	$sysloguncUserName = $syslogConfig.SyslogFileData.userName
	$sysloguncPassword = $syslogConfig.SyslogFileData.password
	$sysloguncDomain = $syslogConfig.SyslogFileData.domain
}
If($syslogUDPenabled){
	$syslogUDPNetworks = $syslogConfig.SyslogUDPData.networkAddresses
}

If($serviceAccounts.instantCloneDomainAdministrator.username -ne ""){
	$InstantCloneAdminUser= $serviceAccounts.instantCloneDomainAdministrator.username
	$InstantCloneAdminPassword = $serviceAccounts.instantCloneDomainAdministrator.password
}

$GlobalSettings = $horizonConfig.connectionServerConfig.globalSettings
$clientMaxSessionTimePolicy = $GlobalSettings.generalData.clientMaxSessionTimePolicy
$clientMaxSessionTimeMinutes = $GlobalSettings.generalData.clientMaxSessionTimeMinutes
$clientIdleSessionTimeoutPolicy = $GlobalSettings.generalData.clientIdleSessionTimeoutPolicy
$clientIdleSessionTimeoutMinutes = $GlobalSettings.generalData.clientIdleSessionTimeoutMinutes
$clientSessionTimeoutMinutes = $GlobalSettings.generalData.clientSessionTimeoutMinutes
$desktopSSOTimeoutPolicy = $GlobalSettings.generalData.desktopSSOTimeoutPolicy
$desktopSSOTimeoutMinutes = $GlobalSettings.generalData.desktopSSOTimeoutMinutes
$applicationSSOTimeoutPolicy = $GlobalSettings.generalData.applicationSSOTimeoutPolicy
$applicationSSOTimeoutMinutes = $GlobalSettings.generalData.applicationSSOTimeoutMinutes
$viewAPISessionTimeoutMinutes = $GlobalSettings.generalData.viewAPISessionTimeoutMinutes
$preLoginMessage = $GlobalSettings.generalData.preLoginMessage
$displayWarningBeforeForcedLogoff = $GlobalSettings.generalData.displayWarningBeforeForcedLogoff
$forcedLogoffTimeoutMinutes = $GlobalSettings.generalData.forcedLogoffTimeoutMinutes
$forcedLogoffMessage = $GlobalSettings.generalData.forcedLogoffMessage
$enableServerInSingleUserMode = $GlobalSettings.generalData.enableServerInSingleUserMode
$storeCALOnBroker = $GlobalSettings.generalData.storeCALOnBroker
$storeCALOnClient = $GlobalSettings.generalData.storeCALOnClient
$reauthSecureTunnelAfterInterruption = $GlobalSettings.securityData.reauthSecureTunnelAfterInterruption
$messageSecurityMode = $GlobalSettings.securityData.messageSecurityMode
$enableIPSecForSecurityServerPairing = $GlobalSettings.securityData.enableIPSecForSecurityServerPairing

If($error){throw "Failed to validate the required configuration settings"}

start-sleep 60 

# Declare Functions
function Get-MapEntry {
  param(
    [Parameter(Mandatory = $true)]
    $Key,
    [Parameter(Mandatory = $true)]
    $Value
  )

  $update = New-Object VMware.Hv.MapEntry
  $update.key = $key
  $update.value = $value
  return $update
}


function addVcenter{
	param(
        [string]$vcName,
        [string]$vcuser,
	    [string]$vcpw,
        [switch]$ignoreSSL
	)
		
	$vcService = New-Object VMware.Hv.VirtualCenterService
	$certService = New-Object VMware.Hv.CertificateService
	$vcSpecHelper = $vcService.getVirtualCenterSpecHelper()

	$vcPassword = New-Object VMware.Hv.SecureString
	$enc = [system.Text.Encoding]::UTF8
	$vcPassword.Utf8String = $enc.GetBytes($vcpw)

	$serverSpec = $vcSpecHelper.getDataObject().serverSpec
	$serverSpec.serverName = $vcName
	$serverSpec.port = 443
	$serverSpec.useSSL = $true
	$serverSpec.userName = $vcuser
	$serverSpec.password = $vcPassword
	$serverSpec.serverType = $certService.getServerSpecHelper().SERVER_TYPE_VIRTUAL_CENTER
	
    If(!($ignoreSSL)){
		$certData = $certService.Certificate_Validate($HzService, $serverSpec)
		$certificateOverride = New-Object VMware.Hv.CertificateThumbprint
		$certificateOverride.sslCertThumbprint = $certData.thumbprint.sslCertThumbprint
		$certificateOverride.sslCertThumbprintAlgorithm = $certData.thumbprint.sslCertThumbprintAlgorithm
	    $vcSpecHelper.getDataObject().CertificateOverride = $certificateOverride
    }
	
    #setup the storage accelerator
	$storageAccelertorSetup = new-object VMware.Hv.VirtualCenterStorageAcceleratorData
	$storageAccelertorSetup.enabled = $true
	$storageAccelertorSetup.defaultCacheSizeMB = 2048
	
	$vcSpecHelper.getDataObject().storageAcceleratorData =  $storageAccelertorSetup
	
	# Make service call
	$vcId = $vcService.VirtualCenter_Create($HzService, $vcSpecHelper.getDataObject())
}


function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
		$checkVM = $false
		return $checkVM
	}
	
	# Check if VM Tools is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -eq "GUESTTOOLSRUNNING"){
		$checkVM = $true	
		return $checkVM
	} else {
		$checkVM = $false
		return $checkVM
	}
}


function WaitFor-VMTools{
	param(
		[string]$vmName
	)

	Write-Host "Waiting for VMTools to start on $vmName" -ForegroundColor Yellow
	$i = 0
	While($toolsStatus -ne "guesttoolsrunning" -and $i -lt (5*6)){
		$toolsStatus = (get-vm $vmName).ExtensionData.Guest.ToolsRunningStatus.ToLower()
		$i++
		[System.Threading.Thread]::Sleep(1000*10)
	}
	If($toolsStatus -eq "guesttoolsrunning"){
		Write-Host "VMTools is now confirmed running on $vmName" `n -ForegroundColor Green
		return $true
	} Else {
		return $false
	}
}

function addInstantCloneDomainAdmin{
	 param(
		[string]$InstantCloneAdmin,
		[string]$InstantCloneAdminPassword
	)

	$InstCloneAdminService = $HzService.InstantCloneEngineDomainAdministrator	
	$HvSecurePassword = HvSecurePassword -Password $InstantCloneAdminPassword
	$adDomainService = $HzService.ADDomain
	$DomainID = $adDomainService.ADDomain_List().id
	$InstCloneAdminBase = New-Object VMware.Hv.InstantCloneEngineDomainAdministratorBase
	$InstCloneAdminSpec = New-Object VMware.Hv.InstantCloneEngineDomainAdministratorSpec
	$InstCloneAdminBase.userName = $InstantCloneAdmin
	$InstCloneAdminBase.password = $HvSecurePassword
	$InstCloneAdminBase.domain = $DomainID
	$InstCloneAdminSpec.base = $InstCloneAdminBase
	$InstCloneAdminService.InstantCloneEngineDomainAdministrator_Create($InstCloneAdminSpec)
}


function HvSecurePassword{
	param(
		[string]$Password
	)
	$SecurePassword = New-Object VMware.Hv.SecureString
	$Encoding = [system.Text.Encoding]::UTF8
	$SecurePassword.Utf8String = $Encoding.GetBytes($Password)
	return $SecurePassword
}


function addEventDB{
	param(
		[string]$dbServer,
		[string]$dbName,
		[string]$dbType,
		[int]$eventDbPort,
		[string]$dbUserName,
		[string]$dbPassword,
		[string]$tablePrefix,
		[string]$showEventsForTime,
		[int]$classifyEventsAsNewForDays = 2
	)

	$dbSecurePassword = HvSecurePassword $dbPassword
		
	# HSB This is needed to create an array of update maps 
	$updatesMap = New-Object VMware.Hv.MapEntry
	$updatesMap = @()

	#prefixed with database. or settings. and fixed the case of the keynames. They are case sensitive and all start with lowercase 
	$updatesMap += Get-MapEntry –key 'database.server' –value $dbServer
	$updatesMap += Get-MapEntry -Key 'database.type' -Value "SQLSERVER"
	$updatesMap += Get-MapEntry –key 'database.port' –value $eventDbPort
	$updatesMap += Get-MapEntry –key 'database.name' –value $dbName
	$updatesMap += Get-MapEntry –key 'database.userName' –value $dbUserName
	$updatesMap  += Get-MapEntry –key 'database.password' –value $dbSecurePassword
	$updatesMap  += Get-MapEntry –key 'database.tablePrefix' –value $tablePrefix
	$updatesMap += Get-MapEntry -Key 'settings.showEventsForTime' -Value $showEventsForTime	
	$updatesMap += Get-MapEntry -Key 'settings.classifyEventsAsNewForDays' -Value $classifyEventsAsNewForDays

	$EventDatabaseService = $HzService.EventDatabase
	#only need updates map.
		
	$EventDatabaseService.EventDatabase_Update($updatesMap)
}


function addSYSLOGFile{
	 param(
		[string]$uncEnabled = $false,
		[string]$uncPath,
		[string]$uncUserName,
		[string]$uncPassword,
		[string]$uncDomain,
		[string]$UDPenabled,
		[string]$UDPnetworkAddresses
	)

	$SyslogSecurePassword = HvSecurePassword $uncPassword

	# HSB This is needed to create an array of update maps 
	$updatesMap = New-Object VMware.Hv.MapEntry
	$updatesMap = @()
		
	if ([string]::IsNullOrEmpty($uncEnabled) ){
		[bool]$uncEnabledConfig= $false
	} else {
		[bool]$uncEnabledConfig = [bool]$uncEnabled
	}
	
	$updatesMap += Get-MapEntry –key 'fileData.enabled' –value $uncEnabledConfig	
	$updatesMap += Get-MapEntry –key 'fileData.uncPath' –value $uncPath	
	$updatesMap += Get-MapEntry –key 'fileData.uncUserName' –value $uncUserName
	$updatesMap += Get-MapEntry –key 'fileData.uncPassword' –value $SyslogSecurePassword
	$updatesMap += Get-MapEntry –key 'fileData.uncDomain' –value $uncDomain
		
	[string[]]$networkAddressArray = @()

	$networkAddressArray	

	if ([string]::IsNullOrEmpty($UDPenabled)){
		[bool]$UDPenabledConfig = $false
	} else {
		[bool]$UDPenabledConfig = [bool]$UDPenabled
	}

	if ([string]::IsNullOrEmpty($UDPnetworkAddresses)){
		$networkAddressArray +=''
	} else {
		$networkAddressArray = $UDPnetworkAddresses.split(" ") | where-object {$_ -ne " "}

	}	

	$updatesMap += Get-MapEntry –key 'udpData.enabled' –value $UDPenabledConfig
	$updatesMap += Get-MapEntry –key 'udpData.networkAddresses' –value $networkAddressArray

	$SyslogService = $HzService.Syslog

	$SyslogService.Syslog_Update($updatesMap)
}


function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}


function checkHorizonStatus{
	param(
		[string]$csName,
		[string]$horizonServiceAccountName,
		[string]$horizonServiceAccountPassword
	)
	# Check the VM is working
    If(!(checkVM -vmName $csName)){throw "Unable to find $csName. The VM is either not in the inventory or VMTools is not responding"}
	
	# Validate the connection server is installed and running by checking the service is running on the destination VM using Get-Service
	Write-Host "Waiting for the Connection Server service to start on $csName" -ForegroundColor Yellow
	$checkStatusStatusTimeOut = (Get-Date).AddMinutes(1)
	
	While(($serviceStatus -ne "Running") -and ($checkStatusStatusTimeOut -gt (Get-Date))){
		$serviceStatusOutput = Invoke-VMScript -ScriptText '(Get-Service | Where{$_.Name -eq "wsbroker"}).Status' -VM $csName -guestuser $horizonServiceAccountName -guestpassword $horizonServiceAccountPassword -scripttype Powershell -ErrorAction SilentlyContinue
		$serviceStatus = [string]$serviceStatusOutput.Trim()
		[System.Threading.Thread]::Sleep(1000*10)
		$HorizonStatus = $False
	}
	If ($serviceStatus -eq "Running"){
		$HorizonStatus = $True
	}
	return $HorizonStatus
}


function InstallHorizonReplica{
	param(
		[string]$csName,
		[string]$horizonServiceAccountName,
		[string]$horizonServiceAccountPassword,
		[string]$PrimaryCsFqdn
	)
	
	Write-Host -ForegroundColor Red -BackgroundColor White "`nInstalling the replica server role to $csName"
	
	$csInstallCmd = "$horizonDestinationBinary /s /v`"/qn VDM_SERVER_INSTANCE_TYPE=2 ADAM_PRIMARY_NAME=$PrimaryCsFqdn FWCHOICE=1 VDM_IP_PROTOCOL_USAGE=IPv4 VDM_FIPS_ENABLED=0 VDM_INITIAL_ADMIN_SID=$hznAdminSid`""
	# Execute the job to install all replica Connection Servers simultaneously
	Write-Host "`nInstalling Horizon Connection server on" $csName -ForegroundColor Green
	Start-Job -Name "$csName Install" -ScriptBlock $InstallCsScriptBlock -ArgumentList $csInstallCmd, $csName, $horizonServiceAccountName, $horizonServiceAccountPassword, $mgmtvCenterName #| Out-Null
}


function InstallHorizonPrimary{
	param(
		[string]$csName,
		[string]$horizonServiceAccountName,
		[string]$horizonServiceAccountPassword	
	)
	
	Write-Host "`nInstalling the primary server role to $csName" -ForegroundColor Red -BackgroundColor White
	$csInstallCmd = [scriptblock]::Create("$horizonDestinationBinary /s /v`"/qn VDM_SERVER_INSTANCE_TYPE=1 VDM_INITIAL_ADMIN_SID=$hznAdminSid FWCHOICE=1 VDM_IP_PROTOCOL_USAGE=IPv4 VDM_FIPS_ENABLED=0 VDM_SERVER_RECOVERY_PWD=`"$hznRecoveryPassword`" VDM_SERVER_RECOVERY_PWD_REMINDER=`"$hznRecoveryPasswordHint`"`"")
	
	# Execute the install on the primary Connection Server and wait until it finished before moving on to the replicas.
	Start-Job -Name "$csName Install" -ScriptBlock $InstallCsScriptBlock -ArgumentList $csInstallCmd, $csName, $horizonServiceAccountName, $horizonServiceAccountPassword, $mgmtvCenterName #| Out-Null
}


# Install Connection Server - Script Block
$InstallCsScriptBlock = {
	Param(
		$csInstallCmd,
		$csName,
		$horizonServiceAccountName,
		$horizonServiceAccountPassword,
		$mgmtvCenterName
	)


	Get-Module -ListAvailable VM* | Import-Module
	if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
		Connect-VIServer -Server $mgmtvCenterName -User $horizonServiceAccountName -Password $horizonServiceAccountPassword -Force
	}
	if($global:defaultVIServers.Name -contains $mgmtvCenterName){
		Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
	} Else {
		throw "Unable to connect to vCenter Server"
	}
	
	Invoke-VMScript -ScriptText $csInstallCmd -VM $csName -guestuser $horizonServiceAccountName -guestpassword $horizonServiceAccountPassword -scripttype bat
}


# Copy Connection Server Binary to Server - Script Block
$CopyBinaryScriptBlock = {
	Param(
		$horizonInstallBinary,
		$deploymentDestinationDirectory,
		$csName,
		$horizonServiceAccountName,
		$horizonServiceAccountPassword,
		$mgmtvCenterName
	)
	
	Get-Module -ListAvailable VM* | Import-Module
	if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
		Connect-VIServer -Server $mgmtvCenterName -User $horizonServiceAccountName -Password $horizonServiceAccountPassword -Force #| Out-Null
	}
	if($global:defaultVIServers.Name -contains $mgmtvCenterName){
		Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
	} Else {
		throw "Unable to connect to vCenter Server"
	}
	copy-vmguestfile -LocalToGuest -source $horizonInstallBinary -destination $deploymentDestinationDirectory -Force:$true -vm $csName -guestuser $horizonServiceAccountName -guestpassword $horizonServiceAccountPassword  -ErrorAction SilentlyContinue
	
}
	



####################################################################################
# Script Starts Here

# Connect to Management vCenter Server
if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
	Connect-VIServer -Server $mgmtvCenterName -User $horizonServiceAccountName -Password $horizonServiceAccountPassword -Force #| Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterName){
	Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to vCenter Server"
}

# Get the Horizon Admin SID and write it to the JSON file
$getSidCmd = "CMD /C `"wmic useraccount get name,sid`" | Select-String `"\b$horizonServiceAccountName\b`""
$hznAdminSidString = (Invoke-VMScript -ScriptText $getSidCmd -VM $connectionServers[0].Name -guestuser $horizonServiceAccountName -guestpassword $horizonServiceAccountPassword -scripttype Powershell).scriptOutput
$hznAdminSid = ($hznAdminSidString.Split("$horizonServiceAccountName")[-1]).trim()
$horizonConfig.connectionServerConfig.horizonLocalAdminSID = $hznAdminSid
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJsonPath

# Copy the install binary to each of the connection servers (concurrently)
for($i = 0; $i -lt $connectionServers.count; $i++){
	#Skip null or empty properties.
	If ([string]::IsNullOrEmpty($connectionServers[$i].Name)){Continue}
	$csName = $connectionServers[$i].Name

	# Check the VM is working
	If(!(checkVM -vmName $csName)){throw "Unable to find $csName. The VM is either not in the inventory or VMTools is not responding"}
	
	# Check to see if the install binary has already been copied to the destination server
	$BinaryExists = (Invoke-VMScript -ScriptText "Test-Path $horizonDestinationBinary" -VM $csName -guestuser $horizonServiceAccountName -guestpassword $horizonServiceAccountPassword -scripttype Powershell -ErrorAction SilentlyContinue).Trim()
	If($BinaryExists -eq "False"){
		Write-Host "`nCopying the horizon installation files to $csName" -ForegroundColor Yellow
		# Start the Job to copy the Horizon Install Binary to all Connection Servers Simultaneously.
		Start-Job -Name "Copy files to $csName" -ScriptBlock $CopyBinaryScriptBlock -ArgumentList $horizonInstallBinary, $deploymentDestinationDirectory, $csName, $horizonServiceAccountName, $horizonServiceAccountPassword, $mgmtvCenterName #| Out-Null
	} Else {
		Write-Host "`nHorizon Install Binary already exists on $csName" -ForegroundColor Yellow
	}
}
Get-Job | Format-Table
Write-Host -ForegroundColor Yellow "Waiting for Jobs to Complete"
Wait-Job * -Timeout (60*5) #| Out-Null
Remove-Job * #| Out-Null

# Take Snapshots of each of the connection servers for rollback.
for($i = 0; $i -lt $connectionServers.count; $i++){ 
	#Skip null or empty properties.
	If ([string]::IsNullOrEmpty($connectionServers[$i].Name)){Continue}
	$csName = $connectionServers[$i].Name
	
	# Check the VM is working
    If(!(checkVM -vmName $csName)){throw "Unable to find $csName. The VM is either not in the inventory or VMTools is not responding"}
	
	# Take a snapshot of the VM before attempting to install Horizon Connection Servers, if one doesn't already exist
	Write-Host "`nTaking snapshots of each of the Connection Server VMs to provide a rollback" -ForegroundColor Yellow
	$Snapshot = Get-Snapshot $csName | Select-Object Name
	If($Snapshot.Name -ne "Installing Horizon Connection Server"){
		New-Snapshot -VM $csName -Name "Installing Horizon Connection Server" -Confirm:$false -WarningAction silentlycontinue #| Out-Null
	} Else {
		Write-Host "`nA Snapshot for $csName already exists. There is no need to take a new snapshot!" -ForegroundColor Yellow
	}
}

# Install the primary connection server and then validate the installation. Repeat the install 3 times if it failes.
$Retries = 0
$PrimaryCs = $connectionServers[0].Name
$PrimaryCsFqdn = $connectionServers[0].FQDN
$HorizonPrimaryStatus = ""

While(! $HorizonPrimaryStatus -and $Retries -le 3){
	$Retries++
	Write-Host "Primary Connection Server install attempt: $Retries" `n 

	# Before the third attemp to install the Connection Server, first restart the VM
	If(! $HorizonPrimaryStatus -and $Retries -eq 3){
		Write-Host "Restarting the VM bfore attempting the final Install" `n -ForegroundColor Red
		Restart-VM -VM $csName -confirm:$false #| Out-Null
		WaitFor-VMTools -vmName $csName #| Out-Null
		# Event After VMTools is confirmed running, we'll let the OS run for an additional 30 seconds before trying to run in-guest commands. 
		Start-Sleep 30
	}

	Write-Host "`nAttempting to install the primary Horizon Connection Server to $PrimaryCs. Attempt number $Retries " -ForegroundColor Yellow
	
	#InstallHorizonPrimary -csName $PrimaryCs -horizonServiceAccountName $horizonServiceAccountName -horizonServiceAccountPassword $horizonServiceAccountPassword
	
	Get-Job | Format-Table
	Write-Host "Waiting for the primary Connection Server install to complete" `n -ForegroundColor Yellow
	Wait-Job * -Timeout (60*5) #| Out-Null

	# Confirm if the Horizon Server installed successfully by confirming if the service is running
	$HorizonPrimaryStatus = checkHorizonStatus -csName $PrimaryCs -horizonServiceAccountName $horizonServiceAccountName -horizonServiceAccountPassword $horizonServiceAccountPassword
	
} 
If($HorizonPrimaryStatus){
	Write-Host "`nHorizon Connection Server is installed on $PrimaryCs" -ForegroundColor Green
}
If($Retries -gt 3){
	throw "Horizon Connection Server failed to install on $PrimaryCs after $Retries attempts!"
}


# Create a hashtable with all of the replica Connection Servers (Index [0] is the primary, so we start from Index [1]). Set their default status to $false
$InstalledCS = [ordered]@{}
$csArray = $connectionServers.Name | Where{$_}
foreach($cs in $csArray){
	If($cs -eq $PrimaryCs){
		$InstalledCS.add($cs,$true)
	}Else {
		$InstalledCS.add($cs,$false)
	}
}


# Loop through each of the replica servers and validate if the connection servers are already installed.
foreach($csName in $InstalledCS.Keys | Where{$InstalledCS[$_] -eq $false}){
	Write-Host "Validating the Connection Server install on $csName" -ForegroundColor Yellow
	$Status = checkHorizonStatus -csName $csName -horizonServiceAccountName $horizonServiceAccountName -horizonServiceAccountPassword $horizonServiceAccountPassword
	If($Status){
		Write-Host "Horizon Connection Server is installed on $csName" `n -ForegroundColor Green
		# If the Connection Server replica install worked, change its value to $true
		$InstalledCS.$csName = $true
	}
}


# Install the replica Connection Servers and then Validate. Try to re-install if necessary.
$ValidationRetries = 0
While(($InstalledCS.Values -eq $false) -and ($ValidationRetries -le 3)){
	$ValidationRetries++
	Write-Host "Replica Connection Server install attempt: $Retries" `n 

	# Loop through each replica server (skipping index [0] because it's the primary Connection Server) and concurrently installing the replica servers
	foreach($csName in $InstalledCS.Keys | Where{$InstalledCS[$_] -eq $false}){ 
		# Before the third attemp to install the Connection Server, first restart the VM
		If($ValidationRetries -eq 3){
			Write-Host "Restarting the VM bfore attempting the final Install" `n -ForegroundColor Red
			Restart-VM -VM $csName -confirm:$false #| Out-Null
			WaitFor-VMTools -vmName $csName #| Out-Null
			# Event After VMTools is confirmed running, we'll let the OS run for an additional 30 seconds before trying to run in-guest commands. 
			Start-Sleep 30
		}
		
		Write-Host "Attempting to install the replica Horizon Connection Server to $csName" `n -ForegroundColor Yellow
		InstallHorizonReplica -csName $csName -horizonServiceAccountName $horizonServiceAccountName -horizonServiceAccountPassword $horizonServiceAccountPassword -PrimaryCS $PrimaryCsFqdn
	}

	# Wait for each of the Connection Server install jobs to finish.
	Get-Job | Format-Table
	Write-Host -ForegroundColor Yellow "`nWaiting for Jobs to Complete"
	Wait-Job * -Timeout (60*5) #| Out-Null

	# Loop through each of the replica servers and validate the Horizon service is installed and running.
	foreach($csName in $InstalledCS.Keys | Where{$InstalledCS[$_] -eq $false}){
		Write-Host "Validating the Connection Server install on $csName. Attempt number $ValidationRetries " -ForegroundColor Yellow
		$Status = checkHorizonStatus -csName $csName -horizonServiceAccountName $horizonServiceAccountName -horizonServiceAccountPassword $horizonServiceAccountPassword
		If($Status){
			Write-Host "Horizon Connection Server is installed on $csName" `n -ForegroundColor Green
			# If the Connection Server replica install worked, change its value to $true
			$InstalledCS.$csName = $true
		}
	}
}

# Remove all the jobs that have been run
Remove-Job * -Force #| Out-Null

Write-Host "`n`n-------------------- Connection Server Install status --------------------" -ForegroundColor Yellow
$InstalledCS

If($InstalledCS.Values -eq $false){
	throw "Installation of replica Connection Servers failed!"
}


# Connect to the newly provisioned Horizon Servers
If($global:defaultHVServers.Name -notcontains $horizonConnectionServerURL){
	Connect-HVServer -server $horizonConnectionServerURL -user $horizonServiceAccountName -password $horizonServiceAccountPassword -domain $domainName -WarningAction SilentlyContinue
}
If($global:defaultHVServers.Name -contains $horizonConnectionServerURL){
	Write-Host "`nSuccessfully connected to $horizonConnectionServerURL" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to Horizon Connection Server"
}

# Connect to the Horizon API services so that we can configure the connection servers. 
$HzService = $global:DefaultHVServers.extensiondata

# Apply the Horizon License 
If(!([string]::IsNullOrEmpty($horizonLicenseKey))){
	Write-Host "Applying license key to Horizon: $horizonLicenseKey" 
	$HzService.License.license_set($horizonLicenseKey)
} Else {
	Write-Host "Applied trail licnse to Horizon"
}

# Add each of the Block vCenters
for($i = 0; $i -lt $blockvCenters.count; $i++){ 
	#Skip null or empty properties.
	If ([string]::IsNullOrEmpty($blockvCenters[$i].Name)){Continue}
	$vCenter = $blockvCenters[$i].Name
	$ignoreSSL = $blockvCenters[$i].blockConfig.ignoreSSLValidation
	
	Write-Host "Getting ready to add vCenter: $vCenter"
	if($ignoreSSL){
	    addVcenter -vcName $vCenter -vcuser $horizonServiceAccountName -vcpw $horizonServiceAccountPassword -ignoreSSL
    } else {
        addVcenter -vcName $vCenter -vcuser $horizonServiceAccountName -vcpw $horizonServiceAccountPassword
    }
}

# Configure Horizon to connect to the Event DB
if($eventDbConfig.configureEventDB){
	Write-Host "Adding event database: $eventDbName" 
	addEventDB -dbName $eventDbName -eventDbPort $eventDbPort -dbPassword $eventDbPassword -dbServer $eventDbServer -dbType $eventDbType -dbUserName $eventDbUser -tablePrefix $eventDbTablePrefix -classifyEventsAsNewForDays $classifyEventsAsNewForDays -showEventsForTime $showEventsForTime 
}

# Configure Horizon to send logs to Syslog
if ($syslogUDPenabled){
	Write-Host "Adding Syslog Server: $syslogUDPNetworks"
	addSYSLOGFile -uncenabled $sysloguncEnabled -uncUserName $sysloguncUserName -uncPassword $sysloguncPassword -uncDomain $sysloguncDomain -uncPath $sysloguncPath -UDPenabled $syslogUDPenabled -UDPnetworkAddresses $syslogUDPNetworks	
}

# Configure Intant Clone Admin users
If($serviceAccounts.instantCloneDomainAdministrator.username -ne ""){
	Write-Host "Adding Instant Clone Domain Admin account: $InstantCloneAdminUser"
	addInstantCloneDomainAdmin -InstantCloneAdmin $InstantCloneAdminUser -InstantCloneAdminPassword $InstantCloneAdminPassword
}

# Write the updated settings to the JSON file
$horizonConfig.connectionServerConfig.ConnectionServers = $connectionServers
$eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJsonPath

Disconnect-VIServer * -Force -Confirm:$false
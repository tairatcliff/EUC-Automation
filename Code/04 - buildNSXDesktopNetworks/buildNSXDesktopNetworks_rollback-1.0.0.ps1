<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     buildNsxDesktopNetworks.ps1
 Example:      buildNsxDesktopNetworks.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$error.Clear()

#$globalConfig = $eucConfig.globalConfig
#$mgmtNsxConfig = $eucConfig.mgmtNsxConfig
#$horizonConfig = $eucConfig.horizonConfig
#$mgmtvCenterConfig = $eucConfig.mgmtvCenterConfig


#Get Connection required.
try {
    Connect-NsxServer -NsxServer $DesktopNsxManagerServer -Username 'admin' -password $DesktopNsxAdminPassword -VIUsername $DesktopvCenterUserName -VIPassword $DesktopvCenterPassword -ViWarningAction Ignore -DebugLogging | out-null
} catch {
    Throw "Failed connecting.  Check connection details and try again.  $_"
}

# NSX Infrastructure Configuration.  Adjust to suit environment.
$DesktopNsxManagerServer = If($eucConfig.nsxConfig.desktopNsxManagerServer){$eucConfig.nsxConfig.desktopNsxManagerServer} Else {throw "Desktop NSX Manager is not set"}
$DesktopNsxAdminPassword = If($eucConfig.nsxConfig.desktopNsxAdminPassword){$eucConfig.nsxConfig.desktopNsxAdminPassword} Else {throw "Desktop NSX Manager password is not set"}
$DesktopvCenterUserName = If($eucConfig.nsxConfig.desktopvCenterUserName){$eucConfig.nsxConfig.desktopvCenterUserName} Else {throw "Desktop vCenter username is not set"}
$DesktopvCenterPassword = If($eucConfig.nsxConfig.desktopvCenterPassword){$eucConfig.nsxConfig.desktopvCenterPassword} Else {throw "Desktop vCenter password is not set"}

############################################
############################################
# Topology Details.  No need to modify below here

#Names
$DesktopLsName = If($eucConfig.nsxConfig.desktopLsName){$eucConfig.nsxConfig.desktopLsName} Else {throw "Desktop logical switch name is not set"}
$RDSLsName = If($eucConfig.nsxConfig.RdsLsName){$eucConfig.nsxConfig.RdsLsName} Else {throw "RDS logical switch name is not set"}
$DesktopLdrName = If($eucConfig.nsxConfig.desktopLdrName){$eucConfig.nsxConfig.desktopLdrName} Else {throw "Desktop logical router name is not set"}
$DesktopTransportZoneName = If($eucConfig.nsxConfig.desktopTransportZoneName){$eucConfig.nsxConfig.desktopTransportZoneName} Else {throw "Desktop transport zone name is not set"}

# Configuration
$dnsServer = If($eucConfig.horizonConfig.connectionServers.dnsServerIP){$eucConfig.horizonConfig.connectionServers.dnsServerIP} Else {throw "DNS server not set"}
$DesktopNetwork = If($eucConfig.nsxConfig.desktopNetwork){$eucConfig.nsxConfig.desktopNetwork} Else {throw "Desktop network is not set"}
$DesktopNetworkPrimaryAddress = If($eucConfig.nsxConfig.desktopNetworkPrimaryAddress){$eucConfig.nsxConfig.desktopNetworkPrimaryAddress} Else {throw "Desktop network primary address is not set"}
$desktopSubnetMask = If($eucConfig.nsxConfig.desktopSubnetMask){$eucConfig.nsxConfig.desktopSubnetMask} Else {throw "Desktop subnet mask name is not set"}
$RdsNetwork = If($eucConfig.nsxConfig.desktopRdsNetwork){$eucConfig.nsxConfig.desktopRdsNetwork} Else {throw "RDS network is not set"}
$RdsNetworkPrimaryAddress = If($eucConfig.nsxConfig.desktopRdsNetworkPrimaryAddress){$eucConfig.nsxConfig.desktopRdsNetworkPrimaryAddress} Else {throw "RDS network primary address is not set"}
$RdsSubnetMask = If($eucConfig.nsxConfig.desktopRdsSubnetMask){$eucConfig.nsxConfig.desktopRdsSubnetMask} Else {throw "RDS subnet mask is not set"}
$desktopEdge01Name = If($eucConfig.nsxConfig.desktopEdge01Name){$eucConfig.nsxConfig.desktopEdge01Name} Else {throw "Desktop edge name is not set"}
$desktopEdge01TransitIP = If($eucConfig.nsxConfig.desktopEdge01TransitIP){$eucConfig.nsxConfig.desktopEdge01TransitIP} Else {throw "Desktop edge transip IP is not set"}

$useEdgeDHCPServer = $eucConfig.nsxConfig.useEdgeDHCPServer  
If(! $useEdgeDHCPServer){
    $dhcpServerAddress = If($eucConfig.nsxConfig.desktopDhcpServerAddress){$eucConfig.nsxConfig.desktopDhcpServerAddress} Else {Write-Host -foreground Red "DHCP server is not set"}
}
$deployMicroSeg = $eucConfig.nsxConfig.deployMicroSeg   
If($deployMicroSeg){
    ## Security Groups
    $desktopSgName = If($eucConfig.nsxConfig.desktopSgName){$eucConfig.nsxConfig.desktopSgName} Else {throw "Desktop security group is not set"}
    $desktopSgDescription = If($eucConfig.nsxConfig.desktopSgDescription){$eucConfig.nsxConfig.desktopSgDescription} Else {throw "Desktop security group description is not set"}
    ## Security Tags
    $desktopStName = If($eucConfig.nsxConfig.desktopStName){$eucConfig.nsxConfig.desktopStName} Else {throw "Desktop security tag is not set"}
    ##DFW
    $desktopFirewallSectionName = If($eucConfig.nsxConfig.desktopFirewallSectionName){$eucConfig.nsxConfig.desktopFirewallSectionName} Else {throw "Desktop firewall section name is not set"}
}

<#
####################################################################################################
# Remove DHCP Relay on Desktop Network - This is not available via PowerNSX and requires API calls.

# If the DHCP server will be the Edge (rather than an enterprise DHCP Server), set the DHCP Server Address to the Endge Transit IP.
If($useEdgeDHCPServer){
    $dhcpServerAddress = $desktopEdge01TransitIP
}


Write-Host "Removing DHCP Relay" `n -ForegroundColor Yellow
$dhcpModified = $False
$uriDhcpRelayConfig = "/api/4.0/edges/$($DesktopLdr.id)/dhcp/config/relay"
$dhcpRelayResponse = Invoke-NsxRestMethod -method GET -URI $uriDhcpRelayConfig

$xmlDhcpRelayRoot = $dhcpRelayResponse.SelectSingleNode("//relay")
$xmlRelayServerRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayServer")

if (! $xmlRelayServerRoot){
    Write-Host "No DHCP Relays Configured!" -ForegroundColor Red `n
    #Add-XmlElement -xmlRoot $xmlDhcpRelayRoot -xmlElementName "relayServer"
    #$xmlRelayServerRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayServer")
}


if ($dhcpRelayResponse.SelectSingleNode("//relay/relayServer[ipAddress='$($dhcpServerAddress)']")) {
    Write-Host "Flound DHCP Relay Server to remove" `n
    Add-XmlElement -xmlRoot $xmlRelayServerRoot -xmlElementName "ipAddress" -xmlElementText ""
    $dhcpModified = $True         
}

$xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
if ($xmlDhcpRelayAgentsRoot){
    Add-XmlElement -xmlRoot $xmlDhcpRelayRoot -xmlElementName "relayAgents"
    $xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
}

write-host "Removing DHCP Relay Agents if required" `n -ForegroundColor Yellow
foreach ($interface in (Get-NsxLogicalRouter $DesktopLdrName | Get-NsxLogicalRouterInterface | Where-Object {($_.name -eq "$DesktopLsName") -or ($_.name -eq "$RdsLsName")})) {
    if (! ($dhcpRelayResponse.SelectSingleNode("//relay/relayAgents/relayAgent[vnicIndex='$($interface.index)']")) ) {
        [System.XML.XMLDocument]$xmlDoc = New-Object System.XML.XMLDocument
        [System.XML.XMLElement]$xmlRelayAgent = $XMLDoc.CreateElement("relayAgent")
        $xmlDoc.appendChild($xmlRelayAgent) | out-null
        Add-XmlElement -xmlRoot $xmlRelayAgent -xmlElementName "vnicIndex" -xmlElementText $interface.index
        $importedRelayAgent = $dhcpRelayResponse.ImportNode($xmlRelayAgent, $True)
        $xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
        $xmlDhcpRelayAgentsRoot.appendChild($importedRelayAgent) | out-null
        $dhcpModified = $True     
    }
}


if ($dhcpModified) {
    write-host "Updating DHCP Relay configuration" -ForegroundColor Green
    Invoke-NsxRestMethod -method PUT -URI $uriDhcpRelayConfig -body $dhcpRelayResponse.OuterXml    
}

#>


Get-NsxLogicalRouter | Get-NsxLogicalRouterInterface -Name $DesktopLsName | Remove-NsxLogicalRouterInterface -Confirm:$false
Get-NsxLogicalRouter | Get-NsxLogicalRouterInterface -Name $RdsLsName | Remove-NsxLogicalRouterInterface -Confirm:$false

Get-NsxLogicalSwitch | Where-Object{($_.name -eq $DesktopLsName) -or ($_.name -eq $mgmtTransitLsName) -or ($_.Name -eq $mgmtEUC_MGMT_Network)} | Remove-NsxLogicalSwitch -Confirm:$False
Get-NsxLogicalSwitch | Where-Object{($_.name -eq $RDSLsName) -or ($_.name -eq $mgmtTransitLsName) -or ($_.Name -eq $mgmtEUC_MGMT_Network)} | Remove-NsxLogicalSwitch -Confirm:$False


Write-Host "Rolled back NSX configuraiton `n" -ForegroundColor Green

Disconnect-NsxServer * -Confirm:$false
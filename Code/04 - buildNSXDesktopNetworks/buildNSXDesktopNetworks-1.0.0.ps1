<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     buildNsxDesktopNetworks.ps1
 Example:      buildNsxDesktopNetworks.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>
If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}
$error.Clear()


# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig

# Script specific variables from JSON
$appRootDirectory = $deploymentDirectories.appRootDirectory
$deploymentSourceDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$certificateDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.certificateDirectory
$toolsDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.toolsDirectory
$codeDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.codeDirectory
$configFilesDirectory = Join-Path -Path $appRootDirectory -ChildPath $deploymentDirectories.configFilesDirectory
$destinationConfigFilesDirectory = Join-Path -Path $deploymentDestinationDirectory -ChildPath $deploymentDirectories.configFilesDirectory

# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
# Service Accounts
$airwatchServiceAccountName = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$horizonServiceAccountName = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$localDomainAdminUser = $serviceAccounts.localDomainAdmin.username
$localDomainAdminPassword = $serviceAccounts.localDomainAdmin.password
$domainJoinUser = $serviceAccounts.domainJoin.username
$domainJoinPassword = $serviceAccounts.domainJoin.password

# Import arrays of VMs to deploy
$installCn = $airwatchConfig.airwatchServers.CN.installCn
$installDs = $airwatchConfig.airwatchServers.DS.installDs
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$blockvCenters = $horizonConfig.blockvCenters
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers

$horizonInstallBinaryName = $binaries.horizonBinary
$horizonInstallBinary = Get-Item -Path (Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinaryName)
$hznRecoveryPassword = $horizonConfig.connectionServerConfig.horizonRecoveryPassword
$hznRecoveryPasswordHint = $horizonConfig.connectionServerConfig.horizonRecoveryPasswordHint
$horizonDestinationBinary = Join-Path -Path $deploymentDestinationDirectory -ChildPath $horizonInstallBinaryName
$domainName = $domainConfig.domainName
$netbios = $domainConfig.netbios
$dnsServer = $domainConfig.dnsServer
$horizonConnectionServerURL = $horizonConfig.connectionServerConfig.horizonConnectionServerURL
$horizonLicenseKey = $licenseKeys.horizon


# Functions
function IP-toINT64 () {
    param (
        [Parameter (Mandatory=$true, Position=1)]
        [string]$ip
    )
    $octets = $ip.split(".")
    return [int64]([int64]$octets[0]*16777216 +[int64]$octets[1]*65536 +[int64]$octets[2]*256 +[int64]$octets[3])
}


function INT64-toIP() {
    param ([int64]$int)

    return (([math]::truncate($int/16777216)).tostring()+"."+([math]::truncate(($int%16777216)/65536)).tostring()+"."+([math]::truncate(($int%65536)/256)).tostring()+"."+([math]::truncate($int%256)).tostring() )
}


function Convert-IPAddressToBits([string] $IPAddress)
{
  $result = 0; 
  # ensure we have a valid IP address
  [IPAddress] $ip = $IPAddress;
  $octets = $ip.IPAddressToString.Split('.');
  foreach($octet in $octets)
  {
    while(0 -ne $octet) 
    {
      $octet = ($octet -shl 1) -band [byte]::MaxValue
      $result++; 
    }
  }
  return $result;
}


for($i = 0; $i -lt $blockvCenters.count; $i++){

    $blockvCenterServer = $blockvCenters[$i]
    $blockNsxConfig = $blockvCenters[$i].blockConfig.blockNsxConfig

    # NSX Infrastructure Configuration.  Adjust to suit environment.
    $blockvCenterServerName = $blockvCenterServer.Name
    $desktopNsxServer = $blockNsxConfig.nsxServer
    $dhcpConfig = $blockNsxConfig.dhcp
    $poolConfig = $blockvCenterServer.blockConfig.poolConfig

    If($error){throw "Failed to validate the required configuration settings"}

    for($i = 0; $i -lt $poolConfig.count; $i++){
        #Skip null or empty properties.
	    If(! $poolConfig[$i].DeployPool){Break}

        $pool = $poolConfig[$i]
        $poolNsxConfig = $poolConfig[$i].poolNsxConfig

        # Topology Details.  No need to modify below here
        $desktopLsName = $pool.LsName
        $desktopLdrName = $blockNsxConfig.LdrName
        $desktopTransportZoneName = $blockNsxConfig.TransportZoneName

        # Configuration
        $desktopEdgeClusterName = $blockNsxConfig.EdgeClusterName
        $desktopNetwork = $poolNsxConfig.Network
        $desktopNetworkPrimaryAddress = $poolNsxConfig.NetworkPrimaryAddress
        $desktopSubnetMask = $poolNsxConfig.SubnetMask

        $useEdgeDHCPServer = $dhcpConfig.useEdgeDHCPServer
        If($useEdgeDHCPServer){
            $dhcpServerAddress = $dhcpConfig.edge01TransitIP
            $desktopEdge01Name = $dhcpConfig.edge01Name
        } Else {
            $dhcpServerAddress = $dhcpConfig.externalDhcpServerIP
        } 


        #Get Connection required.
        try {
            Connect-NsxServer -vCenterServer $blockvCenterServerName -Username $horizonServiceAccountName -Password $horizonServiceAccountPassword #| Out-Null
        } catch {
            throw "Failed connecting.  Check connection details and try again.  $_"
        }

        # Validate.
        If(! (Get-NsxTransportZone $DesktopTransportZoneName)){Write-Error "Unable to find Desktop Transport Zone Name: $DesktopTransportZoneName"}
        If(! (Get-NsxLogicalRouter $DesktopLdrName)){Write-Error "Unable to find Desktop Logical Router: $DesktopLdrName"}

        write-host -foregroundcolor Green "NSX Horizon Desktop deployment beginning.`n"

        #Logical Switches
        write-host -foregroundcolor "Green" "Creating Logical Switches `n"
        $UniversalLDR = [bool](Get-NsxLogicalRouter $blockNsxConfig.LdrName).isUniversal

        # Creates logical switches for either Universal or Global LDR
        If($UniversalLDR){
            $DesktopLs = Get-NsxTransportZone $DesktopTransportZoneName | New-NsxLogicalSwitch $desktopLsName
        } else { 
            $DesktopLs = Get-NsxTransportZone $DesktopTransportZoneName | New-NsxLogicalSwitch $desktopLsName
        }
        

        If(Get-NSXLogicalSwitch -Name $DesktopLsName){
            Write-Host "Created Logical Switch: $desktopLsName" `n -ForegroundColor Yellow
        } Else {
            Write-Error "Failed to create Logical Switch $desktopLsName"
        }

        # DLR Appliance has the uplink router interface created first.
        write-host -foregroundcolor "Green" "Connecting Logical Switches to DLR `n"
        $DesktopSubnetBits = Convert-IPAddressToBits $desktopSubnetMask
        Get-NsxLogicalRouter $DesktopLdrName | New-NsxLogicalRouterInterface -type Internal -Name $desktopLsName -ConnectedTo $DesktopLs -PrimaryAddress $DesktopNetworkPrimaryAddress -SubnetPrefixLength $DesktopSubnetBits #| Out-Null
        $DesktopLdr = Get-NsxLogicalRouter $DesktopLdrName


        # Write an update the JSON to add the desktop pool network portgroup Name.
        [string]$desktopPortGroup = (Get-VDPortgroup | Where-Object {$_.Name -like "*$($desktopLs.vdnId)-$($desktopLs.Name)"}).Name
        $pool.networkPortGroup = $desktopPortGroup
        $eucConfig | ConvertTo-Json -depth 100 | Set-Content $eucConfigJson
        

        # Enable DHCP Relay on Desktop Network - This is not available via PowerNSX and requires API calls.
        Write-Host "Starting DHCP configuration" `n -ForegroundColor Green

        Write-Host "Configuring DHCP Relay" `n -ForegroundColor Yellow
        $dhcpModified = $False
        $uriDhcpRelayConfig = "/api/4.0/edges/$($DesktopLdr.id)/dhcp/config/relay"
        $dhcpRelayResponse = Invoke-NsxRestMethod -method GET -URI $uriDhcpRelayConfig

        $xmlDhcpRelayRoot = $dhcpRelayResponse.SelectSingleNode("//relay")
        $xmlRelayServerRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayServer")

        if (! $xmlRelayServerRoot){
            Add-XmlElement -xmlRoot $xmlDhcpRelayRoot -xmlElementName "relayServer"
            $xmlRelayServerRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayServer")
        }

        if (! $dhcpRelayResponse.SelectSingleNode("//relay/relayServer[ipAddress='$($dhcpServerAddress)']")) {
            Add-XmlElement -xmlRoot $xmlRelayServerRoot -xmlElementName "ipAddress" -xmlElementText $dhcpServerAddress
            $dhcpModified = $True         
        }

        $xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
        if (! $xmlDhcpRelayAgentsRoot){
            Add-XmlElement -xmlRoot $xmlDhcpRelayRoot -xmlElementName "relayAgents"
            $xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
        }

        write-host "Adding DHCP Relay Agents if required" `n -ForegroundColor Yellow
        foreach ($interface in (Get-NsxLogicalRouter $DesktopLdrName | Get-NsxLogicalRouterInterface | Where-Object {($_.name -eq "$desktopLsName") -or ($_.name -eq "$RdsLsName")})) {
            if (! ($dhcpRelayResponse.SelectSingleNode("//relay/relayAgents/relayAgent[vnicIndex='$($interface.index)']")) ) {
                [System.XML.XMLDocument]$xmlDoc = New-Object System.XML.XMLDocument
                [System.XML.XMLElement]$xmlRelayAgent = $XMLDoc.CreateElement("relayAgent")
                $xmlDoc.appendChild($xmlRelayAgent) #| Out-Null
                Add-XmlElement -xmlRoot $xmlRelayAgent -xmlElementName "vnicIndex" -xmlElementText $interface.index
                $importedRelayAgent = $dhcpRelayResponse.ImportNode($xmlRelayAgent, $True)
                $xmlDhcpRelayAgentsRoot = $dhcpRelayResponse.SelectSingleNode("//relay/relayAgents")
                $xmlDhcpRelayAgentsRoot.appendChild($importedRelayAgent) #| Out-Null
                $dhcpModified = $True     
            }
        }

        if ($dhcpModified) {
            write-host "Updating DHCP Relay configuration" -ForegroundColor Green
            Invoke-NsxRestMethod -method PUT -URI $uriDhcpRelayConfig -body $dhcpRelayResponse.OuterXml    
        }


        # Enable DHCP server on ESG
        if($useEdgeDHCPServer){
            Write-Host "Configuring Edge with DHCP Server" -ForegroundColor Yellow
            $desktopEdge = Get-NsxEdge | Where-Object {$_.Name -eq $desktopEdge01Name}

            $edgeDhcpModified = $False

            $uriEdgeDhcpConfig = "/api/4.0/edges/$($desktopEdge.id)/dhcp/config"
            $edgeDhcpConfig = Invoke-NsxRestMethod -method GET -URI $uriEdgeDhcpConfig

            if (! $edgeDhcpConfig.dhcp.enabled) {
                write-host "Configuring DHCP Server on ESG $($desktopEdge.name) ($($desktopEdge.id))"
                $edgeDhcpConfig.dhcp.enabled = "true"
                $edgeDhcpModified = $True    
            }

            $xmlEdgeDhcpPoolRoot = $edgeDhcpConfig.SelectSingleNode("//dhcp/ipPools")

            # Configure Desktop DHCP Settings
            $desktopNetworkDetails = Get-NetworkRange -Network $DesktopNetwork -SubnetMask $DesktopSubnetMask
            $desktopStartAddress = INT64-toIP((IP-toINT64($DesktopNetwork)) + 3)
            $desktopEndAddress = INT64-toIP((IP-toINT64($desktopNetworkDetails.broadcast)) - 1)
            $desktopDefaultGateway = INT64-toIP((IP-toINT64($DesktopNetwork)) + 1)

            if (-not ($edgeDhcpConfig.SelectSingleNode("//dhcp/ipPools/ipPool[ipRange='$($desktopStartAddress)-$($desktopEndAddress)']"))) {
                [System.XML.XMLDocument]$xmlDoc = New-Object System.XML.XMLDocument
                [System.XML.XMLElement]$xmlIpPool = $XMLDoc.CreateElement("ipPool")
                $xmlDoc.appendChild($xmlIpPool) #| Out-Null
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "autoConfigureDNS" -xmlElementText "false"
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "primaryNameServer" -xmlElementText $dnsServer
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "subnetMask" -xmlElementText $desktopSubnetMask
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "ipRange" -xmlElementText "$($desktopStartAddress)-$($desktopEndAddress)"
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "defaultGateway" -xmlElementText $desktopDefaultGateway
                
                $importedDhcpIpPool = $edgeDhcpConfig.ImportNode($xmlIpPool, $True)
                $xmldDhcpIpPoolsRoot = $edgeDhcpConfig.SelectSingleNode("//dhcp/ipPools")
                $xmldDhcpIpPoolsRoot.appendChild($importedDhcpIpPool) #| Out-Null

                write-host "Updating Edge Desktop DHCP configuration `n" -ForegroundColor Green
                Invoke-NsxRestMethod -method PUT -URI $uriEdgeDhcpConfig -body $edgeDhcpConfig.OuterXml 
            }
            if (-not ($edgeDhcpConfig.SelectSingleNode("//dhcp/ipPools/ipPool[ipRange='$($RdsStartAddress)-$($RdsEndAddress)']"))) {
                [System.XML.XMLDocument]$xmlDoc = New-Object System.XML.XMLDocument
                [System.XML.XMLElement]$xmlIpPool = $XMLDoc.CreateElement("ipPool")
                $xmlDoc.appendChild($xmlIpPool) #| Out-Null
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "autoConfigureDNS" -xmlElementText "false"
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "primaryNameServer" -xmlElementText $dnsServer
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "subnetMask" -xmlElementText $RdsSubnetMask
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "ipRange" -xmlElementText "$($RdsStartAddress)-$($RdsEndAddress)"
                Add-XmlElement -xmlRoot $xmlIpPool -xmlElementName "defaultGateway" -xmlElementText $RdsDefaultGateway
                
                $importedDhcpIpPool = $edgeDhcpConfig.ImportNode($xmlIpPool, $True)
                $xmldDhcpIpPoolsRoot = $edgeDhcpConfig.SelectSingleNode("//dhcp/ipPools")
                $xmldDhcpIpPoolsRoot.appendChild($importedDhcpIpPool) #| Out-Null     
                
                write-host "Updating Edge RDS DHCP configuration `n" -ForegroundColor Green
                Invoke-NsxRestMethod -method PUT -URI $uriEdgeDhcpConfig -body $edgeDhcpConfig.OuterXml 
            }
        }
    }
}


Write-Host "`n The deployment and configuraiton of NSX is complete `n" -ForegroundColor Green
Disconnect-NsxServer * -Confirm:$False

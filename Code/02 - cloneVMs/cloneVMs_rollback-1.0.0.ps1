<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     cloneHorizonVMs.ps1
 Example:      cloneHorizonVMs.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$error.Clear()

# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $version = $globalConfig.version
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig


# Script specific variables from JSON
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password

# Import arrays of VMs to deploy
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN
$airwatchDsServers = $airwatchConfig.airwatchServers.DS



If($error){throw "Failed to validate the required configuration settings"}



function rollbackVMs{
    param(
        [Array]$vmArray,
        [String]$vmFolder
    )

    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
    	If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        
        $csName = $vmArray[$i].Name
        If (Get-VM $csName){
            Stop-VM $csName -Confirm:$false | Out-Null
            Remove-VM $csName -DeletePermanently -Confirm:$false | Out-Null
        }
    }
    If (Get-Folder $vmFolder -ErrorAction SilentlyContinue ){
        Remove-Folder $vmFolder -DeletePermanently -Confirm:$false | Out-Null
    }
    Write-Host "Rolled back VM Clones`n" -ForegroundColor Green
}


#####################################################################################
######################           SCRIPT STARTS HERE            ######################
#####################################################################################

# Connect to Management vCenter Server
if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
	Connect-VIServer -Server $mgmtvCenterName -User $mgmtvCenterAccount -Password $mgmtvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterName){
	Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to vCenter Server"
}

rollbackVMs -vmArray $connectionServers -vmFolder $vmFolders.horizon
rollbackVMs -vmArray $airwatchCnServers -vmFolder $vmFolders.airwatch
rollbackVMs -vmArray $airwatchDsServers -vmFolder $vmFolders.airwatch

Disconnect-VIServer * -Force -Confirm:$false
<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     cloneVMs.ps1
 Example:      cloneVMs.ps1 -eucConfigJson eucConfig.json
========================================================================
#>
<#
param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)


$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
#>
If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    Write-Host "Refreshing JSON config"
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}

$error.Clear()


# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$vidmConfig = $eucConfig.vidmConfig
$uagConfig = $eucConfig.uagConfig


# Script specific variables from JSON
$deploymentSourceDirectory = Get-Item -path $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$mgmtDatacenterName = $mgmtvCenterConfig.datacenter
$mgmtDatastoreName = $mgmtvCenterConfig.datastore
$mgmtClusterName = $mgmtvCenterConfig.cluster
$mgmtNetwork = $networks.mgmtNetwork
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
$dmzDatacenterName = $dmzvCenterConfig.datacenter
$dmzDatastoreName = $dmzvCenterConfig.datastore
$dmzClusterName = $dmzvCenterConfig.cluster
$dmzInternetNetwork = $networks.dmzInternet
# Service Accounts
$airwatchServiceAccountName = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$horizonServiceAccountName = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$vidmServiceAccountName = $serviceAccounts.vidmServiceAccount.username
$vidmServiceAccountPassword = $serviceAccounts.vidmServiceAccount.password
$localDomainAdminUser = $serviceAccounts.localDomainAdmin.username
$localDomainAdminPassword = $serviceAccounts.localDomainAdmin.password
$domainJoinUser = $serviceAccounts.domainJoin.username
$domainJoinPassword = $serviceAccounts.domainJoin.password
# Domain Config
$domainName = $domainConfig.domainName
$netbios = $domainConfig.netbios
$dnsServer = $domainConfig.dnsServer
# Guest Customisation Config
$orgName = $windowsConfig.orgName
$timeZone = $windowsConfig.timeZone
$referenceVmName = $windowsConfig.referenceVM
$referenceSnapshot = $windowsConfig.referenceSnapshot
$diskFormat = $windowsConfig.diskFormat
$fullAdministratorName = $windowsConfig.fullAdministratorName
$windowsLicenseKey = $licenseKeys.windows
$deployLinkedClones = $windowsConfig.deployLinkedClones
# CPU and RAM
$airwatchCnNumCpu = $airwatchConfig.airwatchServers.CN.numCPU
$airwatchCnRamGb = $airwatchConfig.airwatchServers.CN.ramGB
$airwatchDsNumCpu = $airwatchConfig.airwatchServers.CN.numCPU
$airwatchDsRamGb = $airwatchConfig.airwatchServers.CN.ramGB
$vidmNumCpu = $vidmConfig.vidmServers.numCPU
$vidmRamGb = $vidmConfig.vidmServers.ramGB
$connectionServerNumCpu = $horizonConfig.connectionServerConfig.numCPU
$connectionServerRamGb = $horizonConfig.connectionServerConfig.ramGB
# Affinity Rule Names
$horizonAffinityRuleName = $affinityRuleName.horizonCS
$airwatchCnAffinityRuleName = $affinityRuleName.airwatchCN
$airwatchDsAffinityRuleName = $affinityRuleName.airwatchDS
$vidmAffinityRuleName = $affinityRuleName.vidmServers
# Import arrays of VMs to deploy
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers
$vidmServers = $vidmConfig.vidmServers.servers
# Check which products are being deployed
$deployNsxEdges = $nsxConfig.edge.deployNsxEdges
$deployNsxLogicalSwitches = $nsxConfig.logicalSwitches.deployNsxLogicalSwitches
$deployHorizon = $horizonConfig.deployHorizon
$deployAirWatch = $airwatchConfig.deployAirWatch
$deployVidm = $vidmConfig.deployVidm
$deployUAG = $uagConfig.deployUAG
$deployvIDM = $vidmConfig.deployVidm
# CA SSL Certificate config
$requestAirWatchSslCert = $airwatchConfig.certificateConfig.requestCASignedCertificate
$requestHorizonSslCert = $horizonConfig.certificateConfig.requestCASignedCertificate
$caName = $certificateConfig.caName
$country = $certificateConfig.country
$state = $certificateConfig.state
$city = $certificateConfig.city
$organisation = $certificateConfig.organisation
$organisationOU = $certificateConfig.organisationOU
# Horizon Cert Config
$horizonTemplateName = $horizonConfig.certificateConfig.templateName
$horizonFriendlyName = $horizonConfig.certificateConfig.friendlyName
$horizonCommonName = $horizonConfig.certificateConfig.commonName
# AirWatch Cert Config
$airwatchTemplateName = $airwatchConfig.certificateConfig.templateName
$airwatchCnFriendlyName = $airwatchConfig.certificateConfig.cnFriendlyName
$airwatchCnCommonName = $airwatchConfig.certificateConfig.cnCommonName
$airwatchDsCommonName = $airwatchConfig.certificateConfig.dsCommonName

If($error){throw "Failed to validate the required configuration settings"}


function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}


function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
        $checkVM = $false
        Write-Host "Unable to find $vmName in vCenter!" `n -ForegroundColor Red
		return $checkVM
	}
	
	# Check if VM Toosl is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -ne "GUESTTOOLSRUNNING"){
        $checkVM = $false	
        Write-Host "VMTools is not running on $vmName!" `n -ForegroundColor Red
		return $checkVM
	} 
    
    # Check if network is accessible
    if(Test-Connection -ComputerName $vmName -Count 1 -Quiet -ErrorAction SilentlyContinue){
    	$checkVM = $true	
		return $checkVM
	} else {
        $checkVM = $false
        Write-Host "Unable to reach $vmName over the network!" `n -ForegroundColor Red
		return $checkVM
    }
}


function cloneVMs {
    param(
        [Array]$vmArray,
        [String]$vmFolder,
        [Array]$network,
        [String]$clusterName,
        [String]$datastoreName,
        [Int]$ramGB,
        [Int]$numCPU
    )

    $newVmArray = @()

    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        
        # Guest Customization
        $vmName = $vmArray[$i].Name
        $ipAddress = If($vmArray[$i].IP){$vmArray[$i].IP} Else {Write-Error "$vmName IP not set"}
        $osCustomizationSpecName = "$vmName-CustomizationSpec"
        $subnetmask = $network.netmask
        $gateway = $network.gateway
        $networkPortGroup = Get-VDPortgroup -Name $network.name

        Write-Host "Now provisioning $vmName" -BackgroundColor Blue -ForegroundColor Black `n
        
        # Check if VM already exists in vCenter. If it does, skip to the next VM
        If(Get-VM -Name $vmName -ErrorAction Ignore){
            Write-Host "$vmName already exists. Moving on to next VM" -ForegroundColor Yellow `n
            $newVM = $false
            Continue
        } Else {
            $newVM = $true
        }
    
        # If a Guest Customization with the same name already exists then we will remove it. This will make sure that we get the correct settings applied to the VM
        If(Get-OSCustomizationSpec -Name $osCustomizationSpecName -ErrorAction Ignore){
            Remove-OSCustomizationSpec -OSCustomizationSpec $osCustomizationSpecName -Confirm:$false
        }
        
        # Create a new Guest Customization for each VM so that we can configure each OS with the correct details like a static IP    
        New-OSCustomizationSpec -Name $osCustomizationSpecName -Type NonPersistent -OrgName $orgName -OSType Windows -ChangeSid -DnsServer $dnsServer -DnsSuffix $domainName -AdminPassword $localDomainAdminPassword -TimeZone $timeZone -Domain $domainName -DomainUsername $domainJoinUser -DomainPassword $domainJoinPassword -ProductKey $windowsLicenseKey -NamingScheme fixed -NamingPrefix $vmName -LicenseMode Perserver -LicenseMaxConnections 5 -FullName $fullAdministratorName | Out-Null
        Get-OSCustomizationSpec -Name $osCustomizationSpecName | Get-OSCustomizationNicMapping | Set-OSCustomizationNicMapping -IpMode UseStaticIP -IpAddress $ipAddress -SubnetMask $subnetMask -DefaultGateway $gateway -Dns $dnsServer | Out-Null
        
        If(Get-OSCustomizationSpec -Name $osCustomizationSpecName -ErrorAction Ignore){
            Write-Host "$osCustomizationSpecName profile has been created for $vmName" -ForegroundColor Green `n
        } Else {
            Write-Host "$osCustomizationSpecName failed to create for $vmName" -ForegroundColor Red `n
        }
    
        # For testing purposes Linked Clones can be used. For Production full clones must be used. 
        If($deployLinkedClones){
            Write-Host "Deploying $vmName as a Linked Clone VM"
            New-VM -LinkedClone -ReferenceSnapshot $referenceSnapshot -Name $vmName -ResourcePool $clusterName -Location $vmFolder -Datastore $datastoreName -OSCustomizationSpec $osCustomizationSpecName -DiskStorageFormat $diskFormat -VM $referenceVmName -ErrorAction Stop | Out-Null
            If(Get-VM -Name $vmName -ErrorAction Ignore){
                Write-Host "$vmName has been provisioned as a Linked Clone VM" -ForegroundColor Green `n
            }
        } Else {
            Write-Host "Deploying $vmName as a full clone VM" -ForegroundColor Green `n
            New-VM -Name $vmName -Datastore $datastoreName -DiskStorageFormat $diskFormat -OSCustomizationSpec $osCustomizationSpecName -Location $vmFolder -VM $referenceVmName -ResourcePool $clusterName | Out-Null
            If(Get-VM -Name $vmName -ErrorAction Ignore){
                Write-Host "$vmName has been provisioned as a Full Clone VM" -ForegroundColor Green `n
            }
        }
    
        # Adding each VM to an array of VM Objects that can be used for bulk modifications.
        If($newVM){    
            If(Get-VM -Name $vmName){
                $newVmArray += (Get-VM -Name $vmName)
            } Else {
                Write-Error "$vmName failed to be created, it may already exist."
                Continue
            }
        }

        # After the VM is cloned, wait 1 sec (without displaying a sleep timer) before making changes to the VM.
        [System.Threading.Thread]::Sleep(1000)

        #Make sure the new server is provisioned to the correct network/portgroup and set to "Connected"
        Write-Host "Changing network of VM to $($networkPortGroup.Name)"
        $networkAdapter = Get-VM $vmName | Get-NetworkAdapter -Name "Network adapter 1"
        Set-NetworkAdapter -NetworkAdapter $networkAdapter -Portgroup $networkPortGroup -Confirm:$false
        Set-NetworkAdapter -NetworkAdapter $networkAdapter -StartConnected:$true -Confirm:$false

        # Reconfigure the VM with the correct CPU and RAM
        Get-VM -Name $vmName | Set-VM -MemoryGB $ramGB -NumCpu $numCPU -confirm:$false

        # Power on the VMs after they are cloned so that the Guest Customizations can be applied
        Start-VM -VM $vmName -Confirm:$false | Out-Null
        Write-Host "Powering on $vmName VM" -ForegroundColor Yellow `n

        # After the VM has started, confirm the network adapter is connected
        Set-NetworkAdapter -NetworkAdapter $networkAdapter -Connected:$true -Confirm:$false
    }

    # Wait for a while to ensre the OS Guest Customization is compute and VMTools has started
    If($newVmArray){
        Write-Host "`nPausing the script while we wait until the VMs are ready to execute in-guest operations." -ForegroundColor Yellow
        Write-Host "This will take up to 5 minutes for Guest Opimization to complete and VMTools is ready.  " `n -ForegroundColor Yellow
        Start-Sleep -Seconds (60*5)
    } Else {
        Write-Host "No new VMs were created..." `n -ForegroundColor Yellow
    }

    # Check the newly provisioned VMs exist and wait for VMTools to respond
    for($i = 0; $i -lt 5; $i++){
        foreach($VM in $newVmArray){
            If(checkVM -vmName $VM){
                # If the VM is responding them remove it from the array so it doesn't get checked again.
                $newVmArray = $newVmArray | Where-Object {$_ -ne $VM}
            }
        }
        If(! $newVmArray){Break}
        Write-Host "$($newVmArray -join ", ") are not responding. Wait 1 minute and try again...." `n -ForegroundColor Yellow
        Start-Sleep 60
    }
}


function newAffinityRule{
    param(
        [String]$ruleName,
        [String]$cluster,
        [array]$vmArray
    )

    $vmObjectArray = @()
    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        $vmObjectArray += (Get-VM -Name $vmArray[$i].Name)
    }

    if(!(Get-DrsRule -Cluster $cluster -Name $ruleName -ErrorAction Ignore)){
        If($vmArray){ 
            New-DrsRule -Name $ruleName -Cluster $cluster -VM $vmObjectArray -KeepTogether $false -Enabled $true | Out-Null
        }
        if(Get-DrsRule -Cluster $cluster -Name $ruleName -ErrorAction Ignore){
            Write-Host "Created a DRS anti-affinity rule for the Connection Servers: $ruleName" -ForegroundColor Green `n
        } Else {
            Write-Error "Failed to create DRS anti-affinity rule!" `n
        }
    } Else {
        Write-Error "Affinity rule name already exists!"
    }
}


function requestCaCerts{
    param(
        [String]$guestOsAccount,
        [String]$guestOsPassword,
        [string]$commonName,
        [string]$friendlyName,
        [string]$templatename,
        [array]$vmArray
    )

	$createDestinationDirectoryCMD = "If(!(Test-Path -Path $deploymentDestinationDirectory)){New-Item -Path $deploymentDestinationDirectory -ItemType Directory | Out-Null}"

    for($i = 0; $i -lt $vmArray.count; $i++){
         #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
        
        $vmName = $vmArray[$i].Name
        $fqdn = "$vmName.$domainName"

        $certInfSourceFile = "$deploymentSourceDirectory\$vmName.inf" -replace "(?!^\\)\\{2,}","\"
        $certInfDestinationFile = "$deploymentDestinationDirectory\$vmName.inf" -replace "(?!^\\)\\{2,}","\"
        $certReqDestinationFile = "$deploymentDestinationDirectory\$vmName.req" -replace "(?!^\\)\\{2,}","\"
        $certDestinationFile = "$deploymentDestinationDirectory\$vmName.cer" -replace "(?!^\\)\\{2,}","\"

        If( ! (checkVM $vmName)){
            Write-Error "$vmName is not responding!!"
            break
        } Else {
            Write-Host "$vmName is now requesting CA signed certificates." `n -foreground Yellow
        }
        
$certReqInf = @"
[NewRequest]
Subject = "CN=$commonName, OU=$organisationOU, O=$organisation, L=$city, S=$state, C=$country"
MachineKeySet = TRUE
KeyLength = 2048
KeySpec=1
Exportable = TRUE
RequestType = PKCS10
FriendlyName = $friendlyName
SMIME = False
PrivateKeyArchive = FALSE
UserProtected = FALSE
UseExistingKeySet = FALSE
ProviderType = 12
KeyUsage = 0xa0
Hashalgorithm = sha256

[EnhancedKeyUsageExtension]
OID=1.3.6.1.5.5.7.3.1 ; Server Authentication
OID=1.3.6.1.5.5.7.3.2 ; Client authentication

[RequestAttributes]
CertificateTemplate = "$templateName"

[Extensions]
2.5.29.17 = "{text}"
_continue_ = "dns=$fqdn&"
"@      

            Write-Host "Copying certificate configuration scripts to $vmName" -ForegroundColor Yellow `n
            # Check the destination directory exists on the VM and if not, create it.
            Invoke-VMScript -ScriptText $createDestinationDirectoryCMD -VM $vmName -guestuser $guestOsAccount -guestpassword $guestOsPassword
            # Create the certificate .inf file and copy it to the designation VM.
            Set-Content -Path $certInfSourceFile -Value $certReqInf
            Copy-VMGuestfile -LocalToGuest -source "$certInfSourceFile" -destination $deploymentDestinationDirectory -Force:$true  -vm $vmName -guestuser $guestOsAccount -guestpassword $guestOsPassword  -ErrorAction SilentlyContinue
            If(Test-Path "$certInfSourceFile"){Remove-Item -Path "$certInfSourceFile"}

$certReqScript = @"
Invoke-Expression -Command "certreq -new '$certInfDestinationFile' '$certReqDestinationFile'"
Invoke-Expression -Command "certreq -adminforcemachine -submit -config $caName '$certReqDestinationFile' '$certDestinationFile'"
Invoke-Expression -Command "certreq -accept '$certDestinationFile'"
"@
						
            Write-Host "Requesting a new certificate for $vmName" -ForegroundColor Yellow `n
            
            # Use VMTools to execute the certificate request powershell script within the guest OS on the destination server
            
            Invoke-VMScript -ScriptText $certreqScript -VM $vmName -guestuser $guestOsAccount -guestpassword $guestOsPassword
            
            # Use VMTools to clean up all of the deployment files from the destination server, without deleting the deployment folder.
            $removeGuestFiles = "Get-ChildItem -Path '$deploymentDestinationDirectory' -Include * -File -Recurse | foreach {`$_.Delete()}"
            Write-Host "Removing left over deployment files from $vmName" -ForegroundColor Yellow `n
            Write-Host "$removeGuestFiles `n"
            Invoke-VMScript -ScriptText $removeGuestFiles -VM $vmName -guestuser $guestOsAccount -guestpassword $guestOsPassword
    }
}


function createVmFolders{
    param(
        [array]$vmFolders,
        [String]$datacenterName
    )
    
    # Create VM Folders
    Foreach($folder in $vmFolders){
        if(get-Folder -Name $folder -ErrorAction Ignore){
            Write-Host "Found an existing $folder VM Folder in vCenter. This is where the Connection Servers will be deployed." -BackgroundColor Yellow -ForegroundColor Black `n
        } Else {
            Write-Host "The $folder VM folder does not exist, creating a new folder" -BackgroundColor Yellow -ForegroundColor Black `n
            (Get-View (Get-View -viewtype datacenter -filter @{"name"="$datacenterName"}).vmfolder).CreateFolder("$folder") | Out-Null
        }
    }
}


function addLocalAdmin{
    param(
        [String]$guestOsAccount,
        [String]$guestOsPassword,
        [String]$serviceAccount,
        [String]$domainName,
        [array]$vmArray
    )

    for($i = 0; $i -lt $vmArray.count; $i++){
        #Skip null or empty properties.
        If ([string]::IsNullOrEmpty($vmArray[$i].Name)){Continue}
       
        $vmName = $vmArray[$i].Name

$addAdminScriptText = @"
`$localAdminGroup = [ADSI]"WinNT://$vmName/Administrators,group"
`$userName = [ADSI]"WinNT://$domainName/$serviceAccount,user"
`$localAdminGroup.Add(`$userName.Path)
"@
        Write-Host "Adding $serviceAccount to the local Administrator group on $vmName" `n -ForegroundColor Yellow
        $Output = Invoke-VMScript -ScriptText $addAdminScriptText -VM $vmName -guestuser $guestOsAccount -guestpassword $guestOsPassword -ScriptType PowerShell
        $Output | Select-Object -ExpandProperty ScriptOutput
    }
}


#####################################################################################
######################           SCRIPT STARTS HERE            ######################
#####################################################################################
# Connect to vCenter Servers
if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
    Connect-VIServer -Server $mgmtvCenterName -User $mgmtvCenterAccount -Password $mgmtvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterName){
    Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

 # If the Horizon VMs are set to $true, Clone the VMs
 If($deployHorizon){
    Write-Host "Deploying Horizon Connection Server VMs" `n -ForegroundColor Yellow
    # Use Start-Job to deploy VM groups in parallel
    createVmFolders -vmFolders $vmFolders.horizon -datacenterName $mgmtDatacenterName
    cloneVMs -vmArray $connectionServers -vmFolder $vmFolders.horizon -network $mgmtNetwork -clusterName $mgmtClusterName -datastoreName $mgmtDatastoreName -ramGB $connectionServerRamGb -numCPU $connectionServerNumCpu
    newAffinityRule -ruleName $horizonAffinityRuleName -cluster $mgmtClusterName -vmArray $connectionServers
    addLocalAdmin -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword -serviceAccount $horizonServiceAccountName -domainName $domainName -vmArray $connectionServers
    If($requestHorizonSslCert){
        Write-Host "Requesting CA signed SSL Certificates for the Horizon Connection Server VMs" `n -ForegroundColor Yellow
        requestCaCerts -commonName $horizonCommonName -friendlyName $horizonFriendlyName -templatename $horizonTemplateName -vmArray $connectionServers -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword
    }
}

# If the AirWatch VMs are set to $true, Clone the VMs
If($deployAirWatch){
    Write-Host "Deploying AirWatch CN VMs" `n -ForegroundColor Yellow
    # Use Start-Job to deploy VM groups in parallel
    createVmFolders -vmFolders $vmFolders.airwatch -datacenterName $mgmtDatacenterName
    cloneVMs -vmArray $airwatchCnServers -vmFolder $vmFolders.airwatch -network $mgmtNetwork -clusterName $mgmtClusterName -datastoreName $mgmtDatastoreName -ramGB $airwatchCnRamGb -numCPU $airwatchCnNumCpu
    newAffinityRule -ruleName $airwatchCnAffinityRuleName -cluster $mgmtClusterName -vmArray $airwatchCnServers
    addLocalAdmin -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword -serviceAccount $airwatchServiceAccountName -domainName $domainName -vmArray $airwatchCnServers
    If($requestAirWatchSslCert){
        Write-Host "Requesting CA signed SSL Certificates for the AirWatch CS VMs" `n -ForegroundColor Yellow
        requestCaCerts -commonName $airwatchCnCommonName -friendlyName $airwatchCnFriendlyName -templatename $airwatchTemplateName -vmArray $airwatchCnServers -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword
    }
}

Disconnect-VIServer * -Force -Confirm:$false

# Connect to DMZ vCenter Servers
if($global:defaultVIServers.Name -notcontains $dmzvCenterName){
    Connect-VIServer -Server $dmzvCenterName -User $dmzvCenterAccount -Password $dmzvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $dmzvCenterName){
    Write-Host "Successfully connected to $dmzvCenterName" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

# If the AirWatch VMs are set to $true, Clone the VMs
If($deployAirWatch){
    Write-Host "Deploying AirWatch DS VMs" `n -ForegroundColor Yellow
    # Use Start-Job to deploy VM groups in parallel
    createVmFolders -vmFolders $vmFolders.airwatch -datacenterName $dmzDatacenterName
    cloneVMs -vmArray $airwatchDsServers -vmFolder $vmFolders.airwatch -network $dmzInternetNetwork -clusterName $dmzClusterName -datastoreName $dmzDatastoreName -ramGB $airwatchDsRamGb -numCPU $airwatchDsNumCpu
    newAffinityRule -ruleName $airwatchDsAffinityRuleName -cluster $dmzClusterName -vmArray $airwatchDsServers
    # If a local admin account is not configured (because the DMZ VM isn't on a domain) this will still run anyway. 

    addLocalAdmin -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword -serviceAccount $airwatchServiceAccountName -domainName $domainName -vmArray $airwatchDsServers
}

If($deployVidm){
    Write-Host "Deploying VMware Identity Manager" `n -ForegroundColor Yellow
    # Use Start-Job to deploy VM groups in parallel
    createVmFolders -vmFolders $vmFolders.vidm -datacenterName $dmzDatacenterName
    cloneVMs -vmArray $vidmServers -vmFolder $vmFolders.vidm -network $dmzInternetNetwork -clusterName $dmzClusterName -datastoreName $dmzDatastoreName -ramGB $vidmRamGb -numCPU $vidmNumCpu
    newAffinityRule -ruleName $vidmAffinityRuleName -cluster $dmzClusterName -vmArray $vidmServers
    # If a local admin account is not configured (because the DMZ VM isn't on a domain) this will still run anyway. 

    addLocalAdmin -guestOsAccount $localDomainAdminUser -guestOsPassword $localDomainAdminPassword -serviceAccount $vidmServiceAccountName -domainName $domainName -vmArray $vidmServers
}

Disconnect-VIServer * -Force -Confirm:$false


Write-Host "Script Completed" -ForegroundColor Green `n
﻿<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     installUAG.ps1
 Example:      installUAG.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>
If(! $eucConfig){
    Write-Host "Required global variables have not be set. The Install_VMware_EUC.ps1 script configures the environment and sets the global variables that are required to run this script." `n -ForegroundColor Red
    throw "EUC Install Script needs to be executed first."
} Else {
    # Refresh the euc Config information from the JSON
    $eucConfigJsonPath = Join-Path -Path $eucConfig.globalConfig.deploymentDirectories.configFilesDirectory -ChildPath $eucConfig.globalConfig.configFileNames.jsonFile
    $eucConfig = Get-Content -Path $eucConfigJsonPath | ConvertFrom-Json
}
$error.Clear()


# Global variables from JSON
$globalConfig = $eucConfig.globalConfig
    $binaries = $globalConfig.binaries
    $licenseKeys = $globalConfig.licenseKeys
    $deploymentDirectories = $globalConfig.deploymentDirectories
    $configFileNames = $globalConfig.configFileNames
    $serviceAccounts = $globalConfig.serviceAccounts
    $windowsConfig = $globalConfig.windowsConfig
    $domainConfig = $globalConfig.domainConfig
    $certificateConfig = $globalConfig.certificateConfig
    $mgmtvCenterConfig = $globalConfig.mgmtvCenterConfig
    $dmzvCenterConfig = $globalConfig.dmzvCenterConfig
    $networks = $globalConfig.networks
    $affinityRuleName = $globalConfig.affinityRuleName
    $vmFolders = $globalConfig.vmFolders
$nsxConfig = $eucConfig.nsxConfig
$horizonConfig = $eucConfig.horizonConfig
$airwatchConfig = $eucConfig.airwatchConfig
$uagConfig = $eucConfig.uagConfig


$mgmtvCenterServer = $mgmtvCenterConfig.vCenter
$horizonServiceAccount = $serviceAccounts.horizonServiceAccount.username
$horizonServiceAccountPassword = $serviceAccounts.horizonServiceAccount.password
$vidmServiceAccount = $serviceAccounts.vidmServiceAccount.username
$vidmServiceAccountPassword = $serviceAccounts.vidmServiceAccount.password
$airwatchServiceAccount = $serviceAccounts.airwatchServiceAccount.username
$airwatchServiceAccountPassword = $serviceAccounts.airwatchServiceAccount.password
$deploymentSourceDirectory = Get-Item -path $deploymentDirectories.sourceDirectory
$deploymentDestinationDirectory = $deploymentDirectories.destinationDirectory
$horizonInstallBinaryName = $binaries.horizonBinary
$horizonInstallBinary = Get-Item -Path (Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinaryName)
$horizonDestinationBinary = Join-Path -Path $deploymentDestinationDirectory -ChildPath $horizonInstallBinary.Name
$deployScriptRoot = $deploymentDirectories.appRootDirectory
$certificateDirectory = Join-Path -Path $deployScriptRoot -ChildPath $deploymentDirectories.certificateDirectory

# Import arrays of VMs to deploy
$connectionServers = $horizonConfig.connectionServerConfig.ConnectionServers
$horizonUagServers = $horizonConfig.connectionServerConfig.uagConfig.uagServers
$airwatchCnServers = $airwatchConfig.airwatchServers.CN.servers
$airwatchCnUagServers = $airwatchConfig.airwatchServers.CN.uagConfig.uagServer
$airwatchDsServers = $airwatchConfig.airwatchServers.DS.servers
$airwatchDsUagServers = $airwatchConfig.airwatchServers.DS.uagConfig.uagServer
$vidmServers = $vidmConfig.vidmServers.servers
$vidmUagServers = $vidmConfig.vidmServers.uagConfig.uagServers
$blockvCenters = $horizonConfig.blockvCenters

# Check which products are being deployed
$deployHorizon = $horizonConfig.deployHorizon
$deployAirWatch = $airwatchConfig.deployAirWatch
$deployVidm = $vidmConfig.deployVidm
$installCn = $airwatchConfig.airwatchServers.CN.installCn
$installDs = $airwatchConfig.airwatchServers.DS.installDs
$deployHorizonUag = $horizonConfig.connectionServerConfig.uagConfig.deployUag
$deployVidmUag = $vidmConfig.vidmServers.uagConfig.deployUag
$deployAirWatchUag = $airwatchConfig.airwatchServers.CN.uagConfig.deployUag

# Other config
$domainName = $domainConfig.domainName
$netbios = $domainConfig.netbios
$horizonConnectionServerURL = $horizonConfig.connectionServerConfig.horizonConnectionServerURL
$horizonLicenseKey = $licenseKeys.horizon
$uagType = $uagConfig.type.toupper()

# vCenter Config
$mgmtvCenterName = $mgmtvCenterConfig.vCenter
$mgmtvCenterAccount = $serviceAccounts.mgmtvCenterAccount.username
$mgmtvCenterPassword = $serviceAccounts.mgmtvCenterAccount.password
$dmzvCenterName = $dmzvCenterConfig.vCenter
$dmzvCenterAccount = $serviceAccounts.dmzvCenterAccount.username
$dmzvCenterPassword = $serviceAccounts.dmzvCenterAccount.password
$dmzDomain = $domainConfig.dmzDomainName
$folderName = $vmFolders.uag
$dmzDatacenter = $dmzvCenterConfig.datacenter
$dmzHostOrCluster = $dmzvCenterConfig.cluster

# Import the root and admin UAG password
$uagRootPassword = $serviceAccounts.uagRootAccount.password
$uagAdminPassword = $serviceAccounts.uagAdminAccount.password

# Define the static UAG configuration defaults that are the same for each UAG
$uagDefaults = @()
$uagDefaults += "[General]"
$uagDefaults += "source=$($uagConfig.source)"
$uagDefaults += "target=$($uagConfig.target)"
$uagDefaults += "ds=$($uagConfig.ds)"
$uagDefaults += "diskMode=$($uagConfig.diskMode)"
$uagDefaults += "dns=$($uagConfig.dns)"
$uagDefaults += "syslogUrl=$($uagConfig.syslogUrl)"
$uagDefaults += "sessionTimeout=$($uagConfig.sessionTimeout)"
$uagDefaults += "honorCipherOrder=$($uagConfig.honorCipherOrder)"
$uagDefaults += "netInternet=$($networks.dmzInternet.Name)"
$uagDefaults += "netManagementNetwork=$($networks.dmzManagement.Name)"
$uagDefaults += "netBackendNetwork=$($networks.dmzBackend.Name)"
$uagDefaults += "defaultGateway=$($networks.dmzInternet.gateway)"
$uagConfig.target = "vi://$($dmzvCenterAccount)@$($dmzDomain):$($dmzvCenterPassword)@$($dmzvCenterName)/$($dmzDatacenter)/host/$($dmzHostOrCluster)"

function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
		$checkVM = $false
		return $checkVM
	}
	
	# Check if VM Tools is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -eq "GUESTTOOLSRUNNING"){
		$checkVM = $true	
		return $checkVM
	} else {
		$checkVM = $false
		return $checkVM
	}
}


function WaitFor-VMTools{
	param(
		[string]$vmName
	)

	Write-Host "Waiting for VMTools to start on $vmName" -ForegroundColor Yellow
	$i = 0
	While($toolsStatus -ne "guesttoolsrunning" -and $i -lt (5*6)){
		$toolsStatus = (get-vm $vmName).ExtensionData.Guest.ToolsRunningStatus.ToLower()
		$i++
		[System.Threading.Thread]::Sleep(1000*10)
	}
	If($toolsStatus -eq "guesttoolsrunning"){
		Write-Host "VMTools is now confirmed running on $vmName" `n -ForegroundColor Green
		return $true
	} Else {
		return $false
	}
}


# Install UAG - Script Block
$InstallUagScriptBlock = {
	& "$using:deployScriptRoot\Code\10 - Deploy UAG\uagdeploy.ps1" -iniFile "$using:uagConfigFile" -rootPwd $using:uagRootPassword -adminPwd $using:uagAdminPassword -ceipEnabled:$false
}


# Get the horizon certificate thumbprints Command/Script Block
$horizonThumbprintCmd = @"
`$thumbprint = Get-ChildItem -Path cert:\LocalMachine\My | Where{`$_.Subject -like "CN=$($horizonConfig.certificateConfig.commonName)*"} | Select Thumbprint
return `$thumbprint.Thumbprint
"@


# Get the vIDM certificate thumbprints Command/Script Block
$vidmThumbprintCmd = @"
`$thumbprint = Get-ChildItem -Path cert:\LocalMachine\My | Where{`$_.Subject -like "CN=$($vidmCertificateConfig.commonName)*"} | Select Thumbprint
return `$thumbprint.Thumbprint
"@


function getThumbprints{
	param(
		$uagConnectTo
	)

	# Define thumprint array
	$thumbprintArray = @()
	$uagConnectTo = $uagConnectTo.toupper()

	switch ($uagConnectTo) {
		"HORIZON" {
			for($i = 0; $i -lt $connectionServers.count; $i++){
				#Skip null or empty properties.
				If ([string]::IsNullOrEmpty($connectionServers[$i].Name)){Continue}
				$vmName = $connectionServers[$i].Name

				$output = Invoke-VMScript -ScriptText $horizonThumbprintCmd -VM $vmName -guestuser $horizonServiceAccount -guestpassword $horizonServiceAccountPassword -ErrorAction Stop
				$thumbprint = $output.ScriptOutput.Trim()
				# Use a regex to add spaces in between every 2 characters
				$thumbprint = $thumbprint -replace '(..(?!$))','$1 '
				$thumbprintArray += "sha1=$thumbprint"
			}
			# Convert the thumbprint array to a comma separated string
			$thumbprintString = $thumbprintArray -join ","

			# Update the JSON configuration setting with the new thumbprints
			$horizonConfig.connectionServerConfig.uagConfig.proxyDestinationUrlThumbprints = $thumbprintString
		}
		"VIDM" {
			for($i = 0; $i -lt $vidmServers.count; $i++){
				#Skip null or empty properties.
				If ([string]::IsNullOrEmpty($vidmServers[$i].Name)){Continue}
				$vmName = $vidmServers[$i].Name

				$output = Invoke-VMScript -ScriptText $horizonThumbprintCmd -VM $vmName -guestuser $vidmServiceAccount -guestpassword $vidmServiceAccountPassword -ErrorAction Stop
				$thumbprint = $output.ScriptOutput.Trim()
				# Use a regex to add spaces in between every 2 characters
				$thumbprint = $thumbprint -replace '(..(?!$))','$1 '
				$thumbprintArray += "sha1=$thumbprint"
			}
			# Convert the thumbprint array to a comma separated string
			$thumbprintString = $thumbprintArray -join ","

			# Update the JSON configuration setting with the new thumbprints
			$vidmConfig.vidmServers.uagConfig.proxyDestinationUrlThumbprints = $thumbprintString
		}
		Default {
			Write-Error "UAG connectTo must be defined as 'Horizon', 'AirWatch', or 'vIDM'. Please review the configuration JSON before continuing." 
		}

	}
	return $thumbprintString
}


function deployUAG{
	param(
		$uagConnectTo
	)

	Write-Host "UAG is connecting to: $uagConnectTo" `n -ForegroundColor Yellow
	$uagSslCerts = @()
	$uagConnectTo = $uagConnectTo.toupper()

	switch($uagConnectTo){
		"HORIZON" {
			$uagServiceConfig = $horizonConfig.connectionServerConfig.uagConfig
		}
		"VIDM" {
			$uagServiceConfig = $vidmConfig.vidmServers.uagConfig
		}
		"AIRWATCH" {
			$uagServiceConfig = $airwatchConfig.airwatchServers.CN.uagConfig
		}
	}
	
	# Update SSL Certificate config
	$uagSslCerts += "[SSLCert]"
	If($uagServiceConfig.SSLCert.pfxCerts){$uagSslCerts += "pfxCerts=$certificateDirectory\$($uagServiceConfig.SSLCert.pfxCerts)"}
	If($uagServiceConfig.SSLCert.pemCerts){$uagSslCerts += "pemCerts=$certificateDirectory\$($uagServiceConfig.SSLCert.pemCerts)"}
	If($uagServiceConfig.SSLCert.pemPrivKey){$uagSslCerts += "pemPrivKey=$certificateDirectory\$($uagServiceConfig.SSLCert.pemPrivKey)"}
	$uagSslCerts += "[SSLCertAdmin]"
	If($uagServiceConfig.SSLCertAdmin.pfxCerts){$uagSslCerts += "pfxCerts=$certificateDirectory\$($uagServiceConfig.SSLCertAdmin.pfxCerts)"}
	If($uagServiceConfig.SSLCertAdmin.pemCerts){$uagSslCerts += "pemCerts=$certificateDirectory\$($uagServiceConfig.SSLCertAdmin.pemCerts)"}
	If($uagServiceConfig.SSLCertAdmin.pemPrivKey){$uagSslCerts += "pemPrivKey=$certificateDirectory\$($uagServiceConfig.SSLCertAdmin.pemPrivKey)"}

	
	# Create the UAG configuration INI file from the JSON settings
	$uag = $uagServiceConfig.uagServers
	for($i = 0; $i -lt $uag.name.count; $i++){
		Write-Host "Creating the UAG configuration Ini File for $($uag[$i].Name)" `n -ForegroundColor Yellow
		
		# Create a new array for all of the UAG configuration details. Start with a new Ini file for each UAG
		$uagIni = @()

		# Add the UAG default settings to the empty uag Ini
		$uagIni += $uagDefaults

		Write-Host "UAG type is: $uagType" `n -ForegroundColor Yellow
		switch ($uagType) {
			"ONENIC" {
				$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
				$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
				$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
			}
			"TWONIC" {
				$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
				$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
				$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
				$uagIni += "ip1=$($uag[$i].twoNic.ip1)"
				$uagIni += "netmask1=$($uag[$i].twoNic.netmask1)"
				$uagIni += "routes1=$($uag[$i].twoNic.routes1)"
			}
			"THREENIC" {
				$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
				$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
				$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
				$uagIni += "ip1=$($uag[$i].twoNic.ip1)"
				$uagIni += "netmask1=$($uag[$i].twoNic.netmask1)"
				$uagIni += "routes1=$($uag[$i].ontwoNiceNic.routes1)"
				$uagIni += "ip2=$($uag[$i].threeNic.ip2)"
				$uagIni += "netmask2=$($uag[$i].threeNic.netmask2)"
				$uagIni += "routes2=$($uag[$i].threeNic.routes2)"
			}
			Default { 
				Write-Error "UAG type must be defined as 'oneNic', 'twoNic', or 'threeNic'. Please review the configuration JSON before continuing." 
			}
		}

		switch ($uagConnectTo) {
			"HORIZON" { 
				$uagIni += "name=$($uag[$i].Name)"
				$uagIni += $uagSslCerts
				$uagIni += "[Horizon]"
				$uagIni += "proxyDestinationUrl=$($uagServiceConfig.proxyDestinationUrl)"
				$uagIni += "proxyDestinationUrlThumbprints=$($uagServiceConfig.proxyDestinationUrlThumbprints)"
				$uagIni += "tunnelExternalUrl=$($uagServiceConfig.tunnelExternalUrl)"
				$uagIni += "blastExternalUrl=$($uagServiceConfig.blastExternalUrl)"
				$uagIni += "pcoipExternalUrl=$($uagServiceConfig.pcoipExternalUrl)"
			}
			"VIDM" {
				$uagIni += "name=$($uag[$i].Name)"
				$uagIni += $uagSslCerts
				$uagIni += "[WebReverseProxy]"
				$uagIni += "instanceId=$($uagServiceConfig.instanceId)"
				$uagIni += "proxyDestinationURL=$($uagServiceConfig.proxyDestinationURL)"
				$uagIni += "proxyDestinationUrlThumbprints=$($uagServiceConfig.proxyDestinationUrlThumbprints)"
				$uagIni += "proxyPattern=$($uagServiceConfig.proxyPattern)"
				$uagIni += "unSecurePattern=$($uagServiceConfig.unSecurePattern)"
				$uagIni += "loginRedirectURL=$($uagServiceConfig.loginRedirectURL)"
				$uagIni += "authCookie=$($uagServiceConfig.authCookie)"
			}

			"AIRWATCH" {
				$uagIni += "name=$($uag[$i].Name)"
				$uagIni += "[Airwatch]"
				$uagIni += "tunnelGatewayEnabled=$($uagServiceConfig.tunnelGatewayEnabled)"
				$uagIni += "tunnelProxyEnabled=$($uagServiceConfig.tunnelProxyEnabled)"
				$uagIni += "segEnabled=$($uagServiceConfig.segEnabled)"
				$uagIni += "apiServerUrl=$($uagServiceConfig.apiServerUrl)"
				$uagIni += "apiServerUsername=$($uagServiceConfig.apiServerUsername)"
				$uagIni += "organizationGroupCode=$($uagServiceConfig.organizationGroupCode)"
				$uagIni += "airwatchServerHostname=$($uagServiceConfig.airwatchServerHostname)"
				$uagIni += "memConfigId=$($uagServiceConfig.memConfigId)"
				$uagIni += "airwatchOutboundProxy=$($uagServiceConfig.airwatchOutboundProxy)"
				$uagIni += "outboundProxyPort=$($uagServiceConfig.outboundProxyPort)"
				$uagIni += "outboundProxyHost=$($uagServiceConfig.outboundProxyHost)"
				$uagIni += "outboundProxyUsername=$($uagServiceConfig.outboundProxyUsername)"
				$uagIni += "ntlmAuthentication=$($uagServiceConfig.ntlmAuthentication)"
			}
			Default {
				Write-Error "UAG connectTo must be defined as 'Horizon', 'AirWatch', or 'vIDM'. Please review the configuration JSON before continuing." 
			}
		}

		# Write the UAG configuration to the Ini file
		Set-Content -Path "$PsScriptRoot\$($uag[$i].Name)_config.ini" -Value $uagIni
		$uagConfigFile = Get-Item -Path "$PsScriptRoot\$($uag[$i].Name)_config.ini"

		# Execute the UAG deploy ps1 script as a new job and pass it the config file we've just created
		#& "$PsScriptRoot\uagdeploy.ps1" -iniFile "$PsScriptRoot\uag$($i + 1)_config.ini"
		Start-Job -Name "$($uag[$i].Name) Deploy" -ScriptBlock $InstallUagScriptBlock #| Out-Null

	}

	Write-Host "Waiting for UAG Install jobs to finish." -ForegroundColor Yellow
	Get-Job
	Wait-Job * | Out-Null
	Receive-Job *
	Remove-Job * -Force | Out-Null

	# Move the newly created UAG VMs to the correct VM folder
	for($i = 0; $i -lt $uag.name.count; $i++){
		Write-Host "Moving $($uag[$i].Name) to the $folderName VM Folder" -ForegroundColor Yellow
		Move-VM -VM "$($uag[$i].Name)" -Destination $folderName -ErrorAction SilentlyContinue
	}
}


#############################################################################################################################
# Script Starts Here
#############################################################################################################################

# Connect to the Management vCenters to retrieve thumbprints of VMs
if($global:defaultVIServers.Name -notcontains $mgmtvCenterName){
    Connect-VIServer -Server $mgmtvCenterName -User $mgmtvCenterAccount -Password $mgmtvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterName){
    Write-Host "Successfully connected to $mgmtvCenterName" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

If($deployHorizonUag){
	Write-Host "Getting the Horizon Connection Server certificate thumbprints" -ForegroundColor Yellow
	getThumbprints -uagConnectTo "HORIZON"
}

Disconnect-VIServer * -Force -Confirm:$false


# Connect to DMZ vCenter Servers
if($global:defaultVIServers.Name -notcontains $dmzvCenterName){
    Connect-VIServer -Server $dmzvCenterName -User $dmzvCenterAccount -Password $dmzvCenterPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $dmzvCenterName){
    Write-Host "Successfully connected to $dmzvCenterName" -ForegroundColor Green `n
} Else {
    Write-Error "Unable to connect to Management vCenter Server"
}

# Create a folder for the EUC Management VMs
if(get-Folder -Name $folderName -ErrorAction Ignore){
    Write-Host "Found an existing $foldername VM Folder in vCenter. This is where the UAG Servers will be deployed." -BackgroundColor Yellow -ForegroundColor Black `n
} Else {
    Write-Host "The $foldername VM folder does not exist, creating a new folder" -BackgroundColor Yellow -ForegroundColor Black `n
    (Get-View (Get-View -viewtype datacenter -filter @{"name"="$datacenterName"}).vmfolder).CreateFolder("$folderName") #| Out-Null
}

If($deployHorizonUag){
	Write-Host "Deploying Horizon UAGs in the DMZ" -ForegroundColor Yellow
	deployUAG -uagConnectTo "HORIZON"
}
If($deployVidmUag){
	Write-Host "Deploying vIDM UAGs in the DMZ" -ForegroundColor Yellow
	deployUAG -uagConnectTo "VIDM"
}
If($deployAirWatchUag){
	Write-Host "Deploying AirWatch UAGs in the DMZ" -ForegroundColor Yellow
	deployUAG -uagConnectTo "AIRWATCH"
}


Write-Host "Finished deploying "
# Write the updated settings to the JSON file
$horizonConfig.connectionServerConfig.ConnectionServers = $connectionServers
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJson

Disconnect-VIServer * -Force -Confirm:$false

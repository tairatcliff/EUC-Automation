﻿<#
========================================================================
 Created on:   05/25/2018
 Created by:   Tai Ratcliff
 Organization: VMware	 
 Filename:     installUAG.ps1
 Example:      installUAG.ps1 -eucConfigJson eucConfigXML.json
========================================================================
#>

param(
    [ValidateScript({Test-Path -Path $_})]
    [String]$eucConfigJson = "$PsScriptRoot\..\..\eucConfig.json"
)

$eucConfig = Get-Content -Path $eucConfigJson | ConvertFrom-Json
$error.Clear()

$globalConfig = $eucConfig.globalConfig
$mgmtNsxConfig = $eucConfig.mgmtNsxConfig
$horizonConfig = $eucConfig.horizonConfig
$connectionServerConfig = $horizonConfig.connectionServerConfig
$blockvCenters = $horizonConfig.blockvcenters
$certificateConfig = $horizonConfig.certificateConfig
$mgmtvCenterConfig = $eucConfig.mgmtvCenterConfig
$uagConfig = $eucConfig.uagConfig
$uagHorizon = $uagConfig.horizon
$uagAirWatch = $uagConfig.AirWatch
$uagSSLCertAdmin = $uagConfig.sslCertAdmin


$mgmtvCenterServer = If($mgmtvCenterConfig.vCenter){$mgmtvCenterConfig.vCenter} Else {throw "Management vCenter Server not set"}
$horizonServiceAccount = If($globalConfig.horizonServiceAccount.Username){$globalConfig.horizonServiceAccount.Username} Else { Write-Error "Horizon service account username not set"}
$horizonServiceAccountPassword = If($globalConfig.horizonServiceAccount.Password){$globalConfig.horizonServiceAccount.Password} Else {Write-Error "Horizon service account password not set"}
$deploymentSourceDirectory = Get-Item -path $globalConfig.deploymentSourceDirectory
$deploymentDestinationDirectory = If($globalConfig.deploymentDestinationDirectory){$globalConfig.deploymentDestinationDirectory} Else {Write-Error "EUC deployment destination directory not set"}
$horizonInstallBinaryName = If($connectionServerConfig.horizonInstallBinary){$connectionServerConfig.horizonInstallBinary} Else {Write-Error "Horizon install binary name not set"}
$horizonInstallBinary = Get-Item -Path (Join-Path -Path $deploymentSourceDirectory -ChildPath $horizonInstallBinaryName)
$hznAdminSID = If($connectionServerConfig.horizonLocalAdminSID){$connectionServerConfig.horizonLocalAdminSID} Else {Write-Error "Horizon local administrator SID not set"}
$hznRecoveryPassword = If($connectionServerConfig.horizonRecoveryPassword){$connectionServerConfig.horizonRecoveryPassword} Else {Write-Error "Horizon recovery password not set"}
$hznRecoveryPasswordHint = If($connectionServerConfig.horizonRecoveryPasswordHint){$connectionServerConfig.horizonRecoveryPasswordHint} Else {Write-Error "Horizon recovery password hint not set"}
$horizonDestinationBinary = Join-Path -Path $deploymentDestinationDirectory -ChildPath $horizonInstallBinary.Name
$csServers = If($connectionServerConfig.ConnectionServers){$connectionServerConfig.ConnectionServers} Else {Write-Error "Connection Servers are not set"}
$domainName = $connectionServerConfig.domainName
$horizonConnectionServerURL = If($connectionServerConfig.horizonConnectionServerURL){$connectionServerConfig.horizonConnectionServerURL} Else {Write-Error "Horizon connection server global URL not set"}
$horizonLicenseKey = If($connectionServerConfig.horizonLicensekey){$connectionServerConfig.horizonLicensekey} Else {Write-Host "Horizon license key not set. This will apply a trail license" -ForegroundColor Red}




function checkVM{
	param(
		[string]$vmName
	)
	$VM = get-vm $vmName -ErrorAction 'silentlycontinue'

	# Confirm the VM exists in vCenter
	If (! $VM){
		$checkVM = $false
		return $checkVM
	}
	
	# Check if VM Tools is running
	if ($VM.ExtensionData.Guest.ToolsRunningStatus.toupper() -eq "GUESTTOOLSRUNNING"){
		$checkVM = $true	
		return $checkVM
	} else {
		$checkVM = $false
		return $checkVM
	}
}


function WaitFor-VMTools{
	param(
		[string]$vmName
	)

	Write-Host "Waiting for VMTools to start on $vmName" -ForegroundColor Yellow
	$i = 0
	While($toolsStatus -ne "guesttoolsrunning" -and $i -lt (5*6)){
		$toolsStatus = (get-vm $vmName).ExtensionData.Guest.ToolsRunningStatus.ToLower()
		$i++
		[System.Threading.Thread]::Sleep(1000*10)
	}
	If($toolsStatus -eq "guesttoolsrunning"){
		Write-Host "VMTools is now confirmed running on $vmName" `n -ForegroundColor Green
		return $true
	} Else {
		return $false
	}
}


# Connect to Management vCenter Server
if($global:defaultVIServers.Name -notcontains $mgmtvCenterServer){
	Connect-VIServer -Server $mgmtvCenterServer -User $horizonServiceAccount -Password $horizonServiceAccountPassword -Force | Out-Null
}
if($global:defaultVIServers.Name -contains $mgmtvCenterServer){
	Write-Host "Successfully connected to $mgmtvCenterServer" -ForegroundColor Green `n
} Else {
	throw "Unable to connect to vCenter Server"
}















# Define the static UAG configuration defaults that are the same for each UAG
$uagDefaults = @()

$uagDefaults += "[General]"
$uagDefaults += "source=$($uagConfig.source)"
$uagDefaults += "target=$($uagConfig.target)"
$uagDefaults += "ds=$($uagConfig.ds)"
$uagDefaults += "diskMode=$($uagConfig.diskMode)"
$uagDefaults += "dns=$($uagConfig.dns)"
$uagDefaults += "syslogUrl=$($uagConfig.syslogUrl)"
$uagDefaults += "sessionTimeout=$($uagConfig.sessionTimeout)"
$uagDefaults += "honorCipherOrder=$($uagConfig.honorCipherOrder)"
$uagDefaults += "netInternet=$($uagConfig.netInternet)"
$uagDefaults += "netManagementNetwork=$($uagConfig.netManagementNetwork)"
$uagDefaults += "netBackendNetwork=$($uagConfig.netBackendNetwork)"
$uagDefaults += "defaultGateway=$($uagConfig.defaultGateway)"

$uagSslCerts = @()
$uagSslCerts += "[SSLCert]"
If($uagSSLCert.pfxCerts){$uagSslCerts += "pfxCerts=$($uagSSLCert.pfxCerts)"}
If($uagSSLCert.pemCerts){$uagSslCerts += "pemCerts=$($uagSSLCert.pemCerts)"}
If($uagSSLCert.pemPrivKey){$uagSslCerts += "pemPrivKey=$($uagSSLCert.pemPrivKey)"}
$uagSslCerts += "[SSLCertAdmin]"
If($uagSSLCertAdmin.pfxCerts){$uagSslCerts += "pfxCerts=$($uagSSLCertAdmin.pfxCerts)"}
If($uagSSLCertAdmin.pemCerts){$uagSslCerts += "pemCerts=$($uagSSLCertAdmin.pemCerts)"}
If($uagSSLCertAdmin.pemPrivKey){$uagSslCerts += "pemPrivKey=$($uagSSLCertAdmin.pemPrivKey)"}


# Create the UAG configuration INI file from the JSON settings
$uag = $uagConfig.uag
for($i = 0; $i -lt $uag.name.count; $i++){
	Write-Host "Creating the UAG configuration Ini File for $($uag[$i].Name)" `n -ForegroundColor Yellow
	
	# Create a new array for all of the UAG configuration details. Start with a new Ini file for each UAG
	$uagIni = @()

	# Add the UAG default settings to the empty uag Ini
	$uagIni += $uagDefaults

	Write-Host "UAG type is: $uagType" `n -ForegroundColor Yellow
	switch ($uagType) {
		"ONENIC" {
			$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
			$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
			$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
		}
		"TWONIC" {
			$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
			$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
			$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
			$uagIni += "ip1=$($uag[$i].twoNic.ip1)"
			$uagIni += "netmask1=$($uag[$i].twoNic.netmask1)"
			$uagIni += "routes1=$($uag[$i].twoNic.routes1)"
		}
		"THREENIC" {
			$uagIni += "ip0=$($uag[$i].oneNic.ip0)"
			$uagIni += "netmask0=$($uag[$i].oneNic.netmask0)"
			$uagIni += "routes0=$($uag[$i].oneNic.routes0)"
			$uagIni += "ip1=$($uag[$i].twoNic.ip1)"
			$uagIni += "netmask1=$($uag[$i].twoNic.netmask1)"
			$uagIni += "routes1=$($uag[$i].ontwoNiceNic.routes1)"
			$uagIni += "ip2=$($uag[$i].threeNic.ip2)"
			$uagIni += "netmask2=$($uag[$i].threeNic.netmask2)"
			$uagIni += "routes2=$($uag[$i].threeNic.routes2)"
		}
		Default { 
			Write-Error "UAG type must be defined as 'oneNic', 'twoNic', or 'threeNic'. Please review the configuration JSON before continuing." 
		}
	}

	Write-Host "UAG is connecting to: $uagConnectTo" `n -ForegroundColor Yellow
	switch ($uagConnectTo) {
		"HORIZON" { 
			$uagIni += "name=$($uag[$i].Name)"
			$uagIni += $uagSslCerts
			$uagIni += "[Horizon]"
			$uagIni += "proxyDestinationUrl=$($uagHorizon.proxyDestinationUrl)"
			$uagIni += "proxyDestinationUrlThumbprints=$($uagHorizon.proxyDestinationUrlThumbprints)"
			$uagIni += "tunnelExternalUrl=$($uagHorizon.tunnelExternalUrl)"
			$uagIni += "blastExternalUrl=$($uagHorizon.blastExternalUrl)"
			$uagIni += "pcoipExternalUrl=$($uagHorizon.pcoipExternalUrl)"
		}
		"VIDM" {
			$uagIni += "name=$($uag[$i].Name)"
			$uagIni += $uagSslCerts
			$uagIni += "[WebReverseProxy]"
			$uagIni += "instanceId=$($uagAirWatch.instanceId)"
			$uagIni += "proxyDestinationURL=$($uagAirWatch.proxyDestinationURL)"
			$uagIni += "proxyDestinationUrlThumbprints=$($uagAirWatch.proxyDestinationUrlThumbprints)"
			$uagIni += "proxyPattern=$($uagAirWatch.proxyPattern)"
			$uagIni += "unSecurePattern=$($uagAirWatch.unSecurePattern)"
			$uagIni += "loginRedirectURL=$($uagAirWatch.loginRedirectURL)"
			$uagIni += "authCookie=$($uagAirWatch.authCookie)"
		}

		"AIRWATCH" {
			$uagIni += "name=$($uag[$i].Name)"
			$uagIni += "[Airwatch]"
			$uagIni += "tunnelGatewayEnabled=$($uagAirWatch.tunnelGatewayEnabled)"
			$uagIni += "tunnelProxyEnabled=$($uagAirWatch.tunnelProxyEnabled)"
			$uagIni += "segEnabled=$($uagAirWatch.segEnabled)"
			$uagIni += "apiServerUrl=$($uagAirWatch.apiServerUrl)"
			$uagIni += "apiServerUsername=$($uagAirWatch.apiServerUsername)"
			$uagIni += "organizationGroupCode=$($uagAirWatch.organizationGroupCode)"
			$uagIni += "airwatchServerHostname=$($uagAirWatch.airwatchServerHostname)"
			$uagIni += "memConfigId=$($uagAirWatch.memConfigId)"
			$uagIni += "airwatchOutboundProxy=$($uagAirWatch.airwatchOutboundProxy)"
			$uagIni += "outboundProxyPort=$($uagAirWatch.outboundProxyPort)"
			$uagIni += "outboundProxyHost=$($uagAirWatch.outboundProxyHost)"
			$uagIni += "outboundProxyUsername=$($uagAirWatch.outboundProxyUsername)"
			$uagIni += "ntlmAuthentication=$($uagAirWatch.ntlmAuthentication)"
		}
		Default {
			Write-Error "UAG connectTo must be defined as 'Horizon', 'AirWatch', or 'vIDM'. Please review the configuration JSON before continuing." 
		}
	}

	# Write the UAG configuration to the Ini file
	Set-Content -Path "$PsScriptRoot\uag$($i + 1)_config.ini" -Value $uagIni

	# Execute the UAG deploy ps1 script as a new job and pass it the config file we've just created
	#& "$PsScriptRoot\uagdeploy.ps1" -iniFile "$PsScriptRoot\uag$($i + 1)_config.ini"
	Start-Job -Name "UAG$($i + 1) Deploy" -ScriptBlock $InstallUagScriptBlock -ArgumentList ($i + 1), $PsScriptRoot, $uagRootPassword, $uagAdminPassword | Out-Null
}

Write-Host "Waiting for UAG Install jobs to finish." -ForegroundColor Yellow
Get-Job
Wait-Job * | Out-Null
Remove-Job * -Force | Out-null

Write-Host "Finished deploying "
# Write the updated settings to the JSON file
$connectionServerConfig.ConnectionServers = $csServers
$eucConfig | ConvertTo-Json -Depth 100 | Set-Content $eucConfigJson

Disconnect-VIServer * -Force -Confirm:$false